		// Объект настройки
		var FileAPI = {
			debug: false, // режим отладки
			staticPath: '/js/' // путь до флешек
		};

		var setupUploader = function (serviceName){ 
            var thisElem = $(".FOTO[name='"+serviceName+"']").find("input[type=file]")
              , uplBut = thisElem.next().next()  
              , resize = 0

            // Upload
            var _onUploudFile = function( evt ){

                var uplInt = $(this)
                  , endInput = uplInt.prev()  
                  , thEl     = uplInt.prev().prev()
                  , progress = uplInt.next().children()
                  , files    = FileAPI.getFiles(thEl)
                console.log("FILEEEEEE"+files)

         console.log(resize);

            if( files != '' ){
                FileAPI.upload({
                    url: '../uploderFiles/upload.php'
                    , files: files
                    , imageTransform: resize
                , upload: function (){
                    uplInt.attr("disabled", true)
                   // document.getElementById('uploading').innerHTML = '(uploading&hellip;)';
                }
                , progress: function (evt){
                    progress.attr("style","width : " + (evt.loaded/evt.total * 100).toFixed(2) +'%');
                }
                , complete: function (err, xhr){
                    if( err ){
                        alert('Oops, server error.');
                    } else {
                        infApp.services.onPhotoUploadEnd(serviceName, xhr.response);
                        progress.attr("style","width : 100%");
                        endInput.val(xhr.response)
                        uplInt.attr("disabled", false)
                    }
                }
                });
            }
            }

			// Функция создающая preview для изображения
			function _createPreview(file, preview){

				FileAPI.getInfo(file, function (err,info){

                    var bigVal
                      , smallVal

                    if ( info.width > info.height ){
                        bigVal   = info.width
                    } else {
                        bigVal   = info.height
                    }

                    if( bigVal < 800 ){
                        resize = 0 
                    }else{
                        prosHeight = info.height * (( 800 * 100 / bigVal ) / 100);
                        prosWidth  = info.width * (( 800 * 100 / bigVal ) / 100); 

                        resize = { width : prosWidth, height : prosHeight }
                    }
				});

				FileAPI.Image(file)
					.preview(197, 180)
					.get(function (err, image){

						// Если не было ошибок, то вставляем изображение
						if( !err ){
							// Отчищаем контейнер от текущего изображения
							preview.html('');

							// Вставляем новое
							preview.append(image);
						}
					})
				;
			};

			// Функция, которая будет срабатывать при выборе файла
			var _onSelectFile = function (evt){

				// Получаем выбранный файл
				var file = FileAPI.getFiles(evt)[0]
                  , prew = $(this).parent().prev()

				if( file ){
					// Строим preview для изображений
					_createPreview(file,prew);

				}
			};

			thisElem.on( "change", _onSelectFile);
            uplBut.on( "click", _onUploudFile);
		};
