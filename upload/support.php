<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>О нас</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">


  <!-- Stylesheets -->
  <link href="style/bootstrap.css" rel="stylesheet">
  <!-- Font awesome icon -->
  <link rel="stylesheet" href="style/font-awesome.css"> 
  <!-- jQuery UI -->
  <link rel="stylesheet" href="style/jquery-ui.css"> 
  <!-- Calendar -->
  <link rel="stylesheet" href="style/fullcalendar.css">
  <!-- prettyPhoto -->
  <link rel="stylesheet" href="style/prettyPhoto.css">   
  <!-- Star rating -->
  <link rel="stylesheet" href="style/rateit.css">
  <!-- Date picker -->
  <link rel="stylesheet" href="style/bootstrap-datetimepicker.min.css">
  <!-- CLEditor -->
  <link rel="stylesheet" href="style/jquery.cleditor.css"> 
  <!-- Uniform -->
  <link rel="stylesheet" href="style/uniform.default.css"> 
  <!-- Bootstrap toggle -->
  <link rel="stylesheet" href="style/bootstrap-toggle-buttons.css">
  <!-- Main stylesheet -->
  <link href="style/style.css" rel="stylesheet">
  <!-- Widgets stylesheet -->
  <link href="style/widgets.css" rel="stylesheet">   
  <!-- Responsive style (from Bootstrap) -->
  <link href="style/bootstrap-responsive.css" rel="stylesheet">
 
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon/favicon.png">
   <link href="style/bootstrap-select.css" rel="stylesheet">
  <link href="style/bootstrap-select.min.css" rel="stylesheet">
</head>

<body>

<?php include("header.php"); ?>
<!-- Header ends -->
<!-- Main content starts -->
<div class="content">
  	<!-- Sidebar -->
  	<div class="sidebar">
		<?php include("sidebar.php"); ?>
 	</div>
	<!-- Sidebar ends -->
	<!-- Main bar -->
  	  	<div class="mainbar">

      <!-- Page heading -->
      <div class="page-head">
        <h2 class="pull-left"><i class="icon-file-alt"></i> Обратная связь</h2>


        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->

	    <!-- Matter -->

	    <div class="matter">
        <div class="container-fluid">

          <div class="row-fluid">

            <div class="span8">

              <!-- Widget -->
              <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                  <div class="pull-left">Напишите нам</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>  
                  <div class="clearfix"></div>
                </div>
                 <!-- Widget content -->
				<div class="row-fluid">
					<div class="span3">
						<center><p>
						 <strong>Тема письма</strong>
						</p></center>
					</div>
					<div class="span5">
						<center><p>
							<select class="selectpicker"  data-width="350px" >
								<option>Деловое предложение</option>
								<option>Я программист, хочу работать с вами</option>
								<option>Я спонсор, хочу работать с вами</option>
								<option>Хочу стать представителем</option>
								<option>Ошибка на сайте</option>
								<option>Другая</option>
							</select>
						</p></center>
						
					</div>
                </div>
                   <hr />

				 <form class="form-horizontal uni">
					<center><div class="textarea" >
                        <!-- Add the "cleditor" to textarea to add CLeditor -->
                        <textarea class="cleditor" name="input"></textarea>

                      </div></center>
					 </form>
                    <hr />
				 <form class="form-horizontal uni">
				 <p>Capcha
				 </p>
										<a href="img/photos/s1.jpg" class="prettyPhoto[pp_gal]"><img src="img/photos/t1.jpg" alt=""></a>

				 </form>
 					<center>
					<button type="submit" class="btn">Отправить</button>
					</center>
					<p>
					</p>
					
					 <!-- Widget footer -->
                  <div class="widget-foot">
                    <p>Мы учитываем каждое Ваше замечание, с уважением администрация сайта.</p>
                  </div>
                </div>

              </div> 

            <div class="span4">

              <!-- Widget -->
              <div class="widget">
                <!-- Widget title -->
                <div class="widget-head">
                  <div class="pull-left">Контакты</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <!-- Widget content -->
                  <div class="padd">
                                               <!-- Contact box -->
                             <div class="support-contact">
                                <!-- Phone, email and address with font awesome icon -->
                                <p>Свяжитесь с нами, если Вам не достточно представленной информации, или если Ваши желания превышают возможности нашего сайта</p>
                                <hr />
                                <p href="support@infokain.com" ><i class="icon-envelope-alt"></i> Email<strong>:</strong><a class="btn btn-link">support@infokain.com</a></p>
                                <hr />
                                <p><i class="icon-skype"></i> Skype<strong>:</strong> Infokain</p>
                                 <!-- Button -->
                            </div>
                  </div>
                </div>

              </div> 

            </div>

          </div>         

        </div>
		  </div>

		<!-- Matter ends -->

    </div>
    </div>

	<!-- Footer starts -->
<script src="js/bootstrap-select.js"></script> <!-- bootstrap-select -->
<script src="js/bootstrap-select.min.js"></script> <!-- bootstrap-select -->
<script src="js/bootstrap-tooltip.js"></script> <!-- bootstrap-tooltip -->
<script>
	$('.selectpicker').selectpicker();
	$('.chosen').chosen({no_results_text: "Bratan takogo netu"});
</script>

</body>
</html>