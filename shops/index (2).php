<?php
	
	if ( isset($_GET['lang']) )
	{
		$lang = $_GET['lang'];
		SetCookie("lang", $lang, time()+7776000);
	}
	else
	{
		if ( empty( $_COOKIE['lang'] ) ) 
		{ 
			$languages = array( 'tm','ru','en','ar','bg','hu','vi','ht','el','da','he','id','es','it','ca','lv','lt','hm','de','nl','no','fa','pl','pt','ro','sk','sl','tr','uk','fi','fr','cs','sv','et','zh','th','hi','ja');		

			if ( in_array( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2), $languages ) )
			{ 
				$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
			} 
			else
			{
				$lang = "en";
			}
			SetCookie("lang", $lang, time()+7776000);
		}
		else
		{
			$lang = $_COOKIE['lang'];
		}
	}
	
	$l = parse_ini_file("../lang/restoran/" . $lang . ".ini");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>INFOKAIN.COM - Информация под рукой</title>
   <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen" />   
	<link rel="stylesheet" href="../css/jquery-ui1.css" type="text/css" media="screen" />	
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="screen" />
    
 <link rel="stylesheet" href="../css/bootstrap-select.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" href="../css/chosen.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/custom-icons.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/jasny-bootstrap-responsive.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/jasny-bootstrap-responsive.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/jasny-bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/jasny-bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/bootstrap-formhelpers.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" href="../css/bootstrap-editable.css" type="text/css" media="screen" /> 
    <link rel="stylesheet" href="../css/ftxt.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/carusel.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../css/slider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" href="../css/jquery.dataTables.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/jquery.dataTables_themeroller.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" media="screen" />
	
	<script src="../js/jquery-1.9.1.js"></script>
    <script src="../js/tooltip.js"></script>
	<script src="../js/jquery-ui.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	
        
    <script src="../js/tooltip.js"></script>
    <script src="../js/chosen.jquery.min.js"></script>
    	<script src="../js/jasny-bootstrap.min.js"></script>
	<script src="../js/jasny-bootstrap.js"></script>
	<script src="../js/bootstrap-formhelpers-datepicker.js"></script>
	<script src="../js/bootstrap-formhelpers-datepicker.ru_RU.js"></script> 
	<script src="../js/bootstrap-popover.js"></script>
    	<script src="../js/bootstrap-editable.min.js"></script>
	<script src="../js/bootstrap-editable.js"></script>
	<script src="../js/bootstrap-typeahead.js"></script> 
    <script src="../js/jquery.dataTables.js"></script>
    <script src="../js/ftxt.js"></script>
	<!--photo-->
	<script src="../js/upload/vendor/jquery.ui.widget.js"></script>
	<script src="../js/upload/load-image.min.js"></script>
	<script src="../js/upload/canvas-to-blob.min.js"></script>
	<script src="../js/upload/jquery.fileupload.js"></script>
	<script src="../js/upload/jquery.iframe-transport.js"></script>
   	<script src="../js/upload/jquery.fileupload-process.js"></script> 
	<script src="../js/upload/jquery.fileupload-image.js"></script>
	<!--photo-->
    <script src="../js/carusel.js"></script>
	<script src="../js/bootstrap-slider.js"></script>
	<script src="../js/jquery.carouFredSel-6.1.0-packed.js"></script>
	<script src="../js/bootstrap-select.js"></script> 
 	<script src="../js/photo.js"></script>
	<script src="../js/query.js"></script>
	
	<script>
        function showAndHide( data, panel ){
            if ( data == "show" ){ $("#" + panel ).show("slide"); }
            else { $("#" + panel).hide("slide"); }
        }
		
 	</script>
<!--<script type="text/javascript">

function followLink(event, link)
{
    var nameLink = link.innerHTML;
    uploadContent(link.href);
    history.pushState({title:nameLink, href:link.href}, null, link.href);
    updateTitle(nameLink);
    event.preventDefault();
}

function updateTitle(title)
{
    var elm = document.getElementsByTagName('title')[0];
    elm.innerHTML = title;
}

function uploadContent(link)
{
    //тут реализуем загрузку части страницы с помощью AJAX
}

window.addEventListener("popstate", function(e) {
    uploadContent(e.state.href);									 
    updateTitle(e.state.title);
}, false );


  </script>
-->
	<style>

.btn-est { background-color: hsl(0, 0%, 16%) !important; background-repeat: repeat-x; filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#5b5b5b", endColorstr="#282828"); background-image: -khtml-gradient(linear, left top, left bottom, from(#5b5b5b), to(#282828)); background-image: -moz-linear-gradient(top, #5b5b5b, #282828); background-image: -ms-linear-gradient(top, #5b5b5b, #282828); background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #5b5b5b), color-stop(100%, #282828)); background-image: -webkit-linear-gradient(top, #5b5b5b, #282828); background-image: -o-linear-gradient(top, #5b5b5b, #282828); background-image: linear-gradient(#5b5b5b, #282828); border-color: #282828 #282828 hsl(0, 0%, 11%); color: #fff !important; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33); -webkit-font-smoothing: antialiased; }

.btn-car { background-color: hsl(24, 100%, 35%) !important; background-repeat: repeat-x; filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#fe7b23", endColorstr="#b24700"); background-image: -khtml-gradient(linear, left top, left bottom, from(#fe7b23), to(#b24700)); background-image: -moz-linear-gradient(top, #fe7b23, #b24700); background-image: -ms-linear-gradient(top, #fe7b23, #b24700); background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fe7b23), color-stop(100%, #b24700)); background-image: -webkit-linear-gradient(top, #fe7b23, #b24700); background-image: -o-linear-gradient(top, #fe7b23, #b24700); background-image: linear-gradient(#fe7b23, #b24700); border-color: #b24700 #b24700 hsl(24, 100%, 29.5%); color: #fff !important; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.36); -webkit-font-smoothing: antialiased; }
.btn-ser { background-color: hsl(110, 56%, 16%) !important; background-repeat: repeat-x; filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#398f28", endColorstr="#193f11"); background-image: -khtml-gradient(linear, left top, left bottom, from(#398f28), to(#193f11)); background-image: -moz-linear-gradient(top, #398f28, #193f11); background-image: -ms-linear-gradient(top, #398f28, #193f11); background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #398f28), color-stop(100%, #193f11)); background-image: -webkit-linear-gradient(top, #398f28, #193f11); background-image: -o-linear-gradient(top, #398f28, #193f11); background-image: linear-gradient(#398f28, #193f11); border-color: #193f11 #193f11 hsl(110, 56%, 11%); color: #fff !important; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33); -webkit-font-smoothing: antialiased; }
.btn-res { background-color: hsl(201, 100%, 30%) !important; background-repeat: repeat-x; filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#00a5ff", endColorstr="#006399"); background-image: -khtml-gradient(linear, left top, left bottom, from(#00a5ff), to(#006399)); background-image: -moz-linear-gradient(top, #00a5ff, #006399); background-image: -ms-linear-gradient(top, #00a5ff, #006399); background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #00a5ff), color-stop(100%, #006399)); background-image: -webkit-linear-gradient(top, #00a5ff, #006399); background-image: -o-linear-gradient(top, #00a5ff, #006399); background-image: linear-gradient(#00a5ff, #006399); border-color: #006399 #006399 hsl(201, 100%, 25%); color: #fff !important; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33); -webkit-font-smoothing: antialiased; }
.btn-job {
  background-color: hsl(0, 89%, 34%) !important;
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#f22121", endColorstr="#a30909");
  background-image: -khtml-gradient(linear, left top, left bottom, from(#f22121), to(#a30909));
  background-image: -moz-linear-gradient(top, #f22121, #a30909);
  background-image: -ms-linear-gradient(top, #f22121, #a30909);
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f22121), color-stop(100%, #a30909));
  background-image: -webkit-linear-gradient(top, #f22121, #a30909);
  background-image: -o-linear-gradient(top, #f22121, #a30909);
  background-image: linear-gradient(#f22121, #a30909);
  border-color: #a30909 #a30909 hsl(0, 89%, 29%);
  color: #fff !important;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33);
  -webkit-font-smoothing: antialiased;
}
.btn-shop {
  background-color: hsl(0, 89%, 34%) !important;
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#da70d6", endColorstr="#660066");
  background-image: -khtml-gradient(linear, left top, left bottom, from(#da70d6), to(#660066));
  background-image: -moz-linear-gradient(top, #da70d6, #660066);
  background-image: -ms-linear-gradient(top, #da70d6, #660066);
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #da70d6), color-stop(100%, #660066));
  background-image: -webkit-linear-gradient(top, #da70d6, #660066);
  background-image: -o-linear-gradient(top, #da70d6, #660066);
  background-image: linear-gradient(#da70d6, #660066);
  border-color: #660066 #660066 hsl(0, 89%, 29%);
  color: #fff !important;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.33);
  -webkit-font-smoothing: antialiased;
}
.dialog_form th {
text-align: left;
}

.dialog_form textarea, .dialog_form input[type=text] {
width: 320px;
}

#dialog_window_minimized_container {
position: fixed;
		  bottom: 0px;
		  left: 0px;
}

.dialog_window_minimized {
	float: left;
	padding: 5px 10px;
	font-size: 12px;
	cursor: pointer;
	margin-right: 2px;
	z-index:10000;
	display:none;
}

.dialog_window_minimized .ui-icon {
	display: inline-block !important;
	position: relative;
	top: 3px;
	cursor: pointer;
}

.ui-dialog .ui-dialog-titlebar-minimize {
	height: 18px;
	width: 19px;
	padding: 1px;
	position: absolute;
	right: 23px;
	top: 9px;
}

.ui-dialog .ui-dialog-titlebar-minimize .ui-icon {
	display: block;
	margin: 1px;
}
.ui-dialog .ui-dialog-titlebar-minimize {
	height: 18px;
	width: 19px;
	padding: 1px;
	position: absolute;
	right: 23px;
	top: 9px;
}

.ui-dialog .ui-dialog-titlebar-minimize .ui-icon {
	display: block;
	margin: 1px;
}
.ui-dialog .ui-dialog-titlebar-minimize:hover, .ui-dialog .ui-dialog-titlebar-minimize:focus {
	padding: 0;
}
 .ui-dialog .ui-dialog-titlebar-close {
	height: 18px;
	width: 19px;
	padding: 1px;
	position: absolute;
	right: 0px;
	top: 15px;
}

.ui-dialog .ui-dialog-titlebar-close .ui-icon {
	display: block;
	margin: 1px;
}
.ui-dialog .ui-dialog-titlebar-close {
	height: 18px;
	width: 19px;
	padding: 1px;
	position: absolute;
	right: 0px;
	top: 20px;
}

.ui-dialog .ui-dialog-titlebar-close .ui-icon {
	display: block;
	margin: -8px;
}
.ui-dialog .ui-dialog-titlebar-close:hover, .ui-dialog .ui-dialog-titlebar-minimize:focus {
	padding: 0;
}                
	</style>
</head>
<body>
<center><h2><p class="text-info"><?php echo $l['RES']; ?></p></h2></center>
<p><div class="part_border_1 res_color"></div></p>
	<div class="container">
		<?php include('button.php'); ?>
	</div>
<p><p><div class="part_border_1 res_color"></div></p></p>
	<?php include('../accordion.php'); ?>
<p><p><div class="part_border_1 res_color"></div></p></p>
<div id="trans_search_div">
<div class="container">
	<div class="accordion" id="accordionTableParam">
		<div class="accordion-group">
			<div id="collapseParamOpen" class="accordion-body collapse in">
				<div class="accordion-inner">
					<a type="button" class="btn btn-link" class="accordion-toggle" data-toggle="collapse" data-parent="#accordionTableParam" href="#collapseParamClose"><i class="icon-chevron-up"></i></a>
					<div class="row">
						<div class="span6">
							<div class="part_border_1 car_color"></div>
								<div class="alert alert-error">
									<a class="close" data-dismiss="alert" href="#">&times;</a>
										В этой области храняться столбцы с заданным параметром</br><strong>Внимание! Изъять столбец из таблицы можно только с одним-сохраненным параметром!</strong> 
								</div>
								<div class="alert alert-error" id="aft">
									Выбранные параметры
								</div>
						<div class="part_border_1 car_color"></div>
						</div>
						<div class="span6">
						<div class="part_border_1 car_color"></div>
							<div class="alert alert-error">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
								В этой области хранятся столбцы которые Вы можете добавить в таблицу</br><strong>Внимание! Вы можете добавить все параметры в таблицу, но она может оказаться нечитаемой!</strong>
							</div>
							<div class="alert alert-error" id="aft2">
								Столбцы с параметрами которые можно добавить
							</div>
								
							<div class="part_border_1 car_color"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accordion-group">
			<div id="collapseParamClose" class="accordion-body collapse">
				<div class="accordion-inner">
					<a type="button" class="btn btn-link" class="accordion-toggle" data-toggle="collapse" data-parent="#accordionTableParam" href="#collapseParamOpen"><i class="icon-chevron-down"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div>

<!-- POPOVER STRING -->
<div id="search_pop"  class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title">A Title</h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; margin-left:20px; right:10px; top:6px">Закрыть</a><p><div class="spax">
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i>Сохранить параметр и изъять столбец</a><br><br>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i>Изъять столбец без параметра</a><br><br>
<input type="text" class="search_column" style="margin-left:20px;" placeholder="Начните вводить" data-provide="typeahead"></div>
</div>
</div>
</div>
<!-- POPOVER NUMERIC -->
<div id="search_pop2" style="display:none" class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title">A Title</h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; right:10px; top:6px">Закрыть</a><p><div style="float:left; width:300px; height:150px">
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i>Сохранить параметр и изъять столбец</a><br><br>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i>Изъять столбец без параметра</a><br><br>
<center>

</center>
</div>
</div>
</div>
</div>
<!-- END POPOVER -->

<div id="posY">
<div style="position:relative; z-index:10; bottom:-55px;" class="span2">
<label>Показано <select data-size="4" style="width:60px; float:left" id="ad_number"  class="selectpicker span1"  size="1" name="transport_search_table_length" aria-controls="transport_search_table">
<option value="10" selected="selected">10</option>
<option value="25">25</option><option value="50">50</option>
<option value="100">100</option>
</select></label></div>
<div style="position:relative; float:left; z-index:10; bottom:-55px;" class="span2">
<button class="btn" type="button" data-toggle="button-checkbox"  style="position:relative;" onclick="showPhotos()" >Показать фотографии</button>

</div>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="transport_search_table">
    
    <thead>
    
        <tr>
        	<th width="4px">
           
            </th>
			<th>
            <a col_id="1" title="Страна" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="2" title="Город" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="3" title="Валюта" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="4" title="Действие" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="5" title="Тип" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="6" title="Кузов" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="7" title="Марка" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
           <a col_id="8" title="Модель" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
           <a col_id="9" title="Двери(шт)" div="trans_search_div" onclick="showPopoverNum(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="10" title="Привод" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="11" title="Топливо" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="12" title="Цена" div="trans_search_div" onclick="showPopoverNum(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="13" title="Дата" div="trans_search_div"><i class="icon-search"></i>
            </th>
             <th >
            <a col_id="14" title="ID" div="trans_search_div"  onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
		</tr>
        <tr role="row">
		<th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-sort aria-label><div class="DataTables_sort_wrapper"></div></th>
		<th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-sort="ascending" aria-label="Страна: activate to sort column descending"><div class="DataTables_sort_wrapper">Страна</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Город: activate to sort column ascending"><div class="DataTables_sort_wrapper">Город</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Валюта: activate to sort column ascending"><div class="DataTables_sort_wrapper">Валюта</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Действие: activate to sort column ascending"><div class="DataTables_sort_wrapper">Действие</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Тип: activate to sort column ascending"><div class="DataTables_sort_wrapper">Тип</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Кузов: activate to sort column ascending"><div class="DataTables_sort_wrapper">Кузов</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Марка: activate to sort column ascending"><div class="DataTables_sort_wrapper">Марка</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Модель: activate to sort column ascending"><div class="DataTables_sort_wrapper">Модель</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Двери(шт): activate to sort column ascending"><div class="DataTables_sort_wrapper">Двери(шт)</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Привод: activate to sort column ascending"><div class="DataTables_sort_wrapper">Привод</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Топливо: activate to sort column ascending"><div class="DataTables_sort_wrapper">Топливо</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Цена: activate to sort column ascending"><div class="DataTables_sort_wrapper">Цена</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">Дата</div></th>
		<th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">ID</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Цена: activate to sort column ascending"><div class="DataTables_sort_wrapper">Фото</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">VIP</div></th>
        </tr>
	</thead>
        
	<tbody>
		
	</tbody>
	<tfoot>
		<tr>
			<th ></th>
			<th >Страна</th>
			<th >Город</th>
			<th >Валюта</th>
			<th >Действие</th>
			<th >Тип</th>
			<th >Кузов</th>
			<th >Марка</th>
			<th >Модель</th>
            <th >Двери(шт)</th>
            <th >Привод</th>
            <th >Топливо</th>
            <th >Цена</th>
            <th >Дата</th>
			<th >ID</th>
		</tr>
	</tfoot>
    </table> 
    </div>   
</div>
</div>
</body>
</html>