<!-- POPOVER STRING -->
<div id="search_pop" style="display:none" class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title">A Title</h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; margin-left:20px; right:10px; top:6px">Закрыть</a><p><div class="spax">
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i>Сохранить параметр и изъять столбец</a><br><br>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i>Изъять столбец без параметра</a><br><br>
<input type="text" class="search_column" style="margin-left:20px;" placeholder="Начните вводить" data-provide="typeahead"></div>
</div>
</div>
</div>
<!-- POPOVER NUMERIC -->
<div id="search_pop2" style="display:none" class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title">A Title</h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; right:10px; top:6px">Закрыть</a><p><div style="float:left; width:300px; height:150px">
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i>Сохранить параметр и изъять столбец</a><br><br>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i>Изъять столбец без параметра</a><br><br>
<center>

</center>
</div>
</div>
</div>
</div>
<!-- END POPOVER -->

<div id="posY">
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="transport_search_table">
    
    <thead>
    
        <tr>
        	<th >
            <a col_id="0" title="Страна" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="1" title="Город" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="2" title="Валюта" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="3" title="Действие" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="4" title="Тип" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="5" title="Кузов" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="6" title="Марка" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
           <a col_id="7" title="Модель" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
           <a col_id="8" title="Двери(шт)" div="search_div" onclick="showPopoverNum(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="9" title="Привод" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="10" title="Топливо" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="11" title="Цена" div="search_div" onclick="showPopoverNum(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="12" title="Дата" div="search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
		</tr>
        <tr role="row"><th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-sort="ascending" aria-label="Страна: activate to sort column descending"><div class="DataTables_sort_wrapper">Страна</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Город: activate to sort column ascending"><div class="DataTables_sort_wrapper">Город</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Валюта: activate to sort column ascending"><div class="DataTables_sort_wrapper">Валюта</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Действие: activate to sort column ascending"><div class="DataTables_sort_wrapper">Действие</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Тип: activate to sort column ascending"><div class="DataTables_sort_wrapper">Тип</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Кузов: activate to sort column ascending"><div class="DataTables_sort_wrapper">Кузов</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Марка: activate to sort column ascending"><div class="DataTables_sort_wrapper">Марка</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Модель: activate to sort column ascending"><div class="DataTables_sort_wrapper">Модель</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Двери(шт): activate to sort column ascending"><div class="DataTables_sort_wrapper">Двери(шт)</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Привод: activate to sort column ascending"><div class="DataTables_sort_wrapper">Привод</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Топливо: activate to sort column ascending"><div class="DataTables_sort_wrapper">Топливо</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Цена: activate to sort column ascending"><div class="DataTables_sort_wrapper">Цена</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">Дата</div></th>
        </tr>
	</thead>
        
	<tbody>
		
	</tbody>
	<tfoot>
		<tr>
			<th >Страна</th>
			<th >Город</th>
			<th >Валюта</th>
			<th >Действие</th>
			<th >Тип</th>
			<th >Кузов</th>
			<th >Марка</th>
			<th >Модель</th>
            <th >Двери(шт)</th>
            <th >Привод</th>
            <th >Топливо</th>
            <th >Цена</th>
            <th >Дата</th>
		</tr>
	</tfoot>
    </table> 
    </div>   
</div>
</div>
