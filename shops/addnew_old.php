<?php
	
	if ( isset($_GET['lang']) )
	{
		$lang = $_GET['lang'];
		SetCookie("lang", $lang, time()+7776000);
	}
	else
	{
		if ( empty( $_COOKIE['lang'] ) ) 
		{ 
			$languages = array( 'tm','ru','en','ar','bg','hu','vi','ht','el','da','he','id','es','it','ca','lv','lt','hm','de','nl','no','fa','pl','pt','ro','sk','sl','tr','uk','fi','fr','cs','sv','et','zh','th','hi','ja');		

			if ( in_array( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2), $languages ) )
			{ 
				$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
			} 
			else
			{
				$lang = "en";
			}
			SetCookie("lang", $lang, time()+7776000);
		}
		else
		{
			$lang = $_COOKIE['lang'];
		}
	}
	
	$l = parse_ini_file("../lang/" . $lang . ".ini");
	
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>INFOKAIN.COM - Информация под рукой</title>

    <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen" />   
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="screen" />
    
	<link rel="stylesheet" href="../css/bootstrap-select.css" type="text/css" media="screen" /> 
	<link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" media="screen" />
	
	<script src="../js/jquery-1.9.1.js"></script>
    <script src="../js/tooltip.js"></script>
	<script src="../js/jquery-ui.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	
        
	<!--photo-->
	<script src="../js/upload/vendor/jquery.ui.widget.js"></script>
	<script src="../js/upload/load-image.min.js"></script>
	<script src="../js/upload/canvas-to-blob.min.js"></script>
	<script src="../js/upload/jquery.fileupload.js"></script>
	<script src="../js/upload/jquery.iframe-transport.js"></script>
   	<script src="../js/upload/jquery.fileupload-process.js"></script> 
	<script src="../js/upload/jquery.fileupload-image.js"></script>
	<!--photo-->
    <script src="../js/carusel.js"></script>
	<script src="../js/bootstrap-slider.js"></script>
	<script src="../js/jquery.carouFredSel-6.1.0-packed.js"></script>
	<script src="../js/bootstrap-select.js"></script> 
 	<script src="../js/photo.js"></script>
	<script src="../js/query.js"></script>
</head>
<body>
<center><h2><p class="text-info"><?php echo $l['SHOP']; ?></p></h2></center>
<p><div class="part_border_1 res_color"></div></p>
	<div class="container">
		<?php include('button.php'); ?>
	</div>
<p><p><div class="part_border_1 res_color"></div></p></p>
<div class="container">
	<div class="alert alert-info">
		<center><strong><?php echo $l['PREVIU']; ?></strong></center>
	</div>
</div>
<p><p><div class="part_border_1 res_color"></div></p></p>
<div class="container">
<div class="accordion" id="accordionServise">
	<div class="accordion-group">
		<div id="collapseMain" class="accordion-body collapse in">
			<div class="accordion-inner">
				<div class="alert alert-info">
					<center><strong><?php echo $l['AFTER_SELECT']; ?> <button class="btn btn-link" type="button" class="accordion-toggle" data-toggle="collapse" data-parent="#accordionServise" href="#collapseKotacts"><?php echo $l['CONTACT_INFO']; ?></button></strong></center>
				</div>
				<div class="row">
					<div class="span4">
						<div class="alert alert-info">
							<strong>Категория магазина</strong>
						</div>
					</div>
					<div class="span7">
						<div class="alert alert-info">
							<strong>Виды товаров</strong>
						</div>
					</div>
					<div class="span1"  rel="tooltip"  title="Отмечайте галочкой продоваемые товары" placement="bottom">
						<div class="alert alert-info">
							<strong><i class="icon-ok"></i></strong>
						</div>
					</div>
				</div>
			<table class="table table-hover">
				<tr>
					<td>
						<input type="text" class="span4" placeholder="Категория"  data-provide="typeahead" data-source='[&quot;Business&quot;,&quot;Animals&quot;,&quot;Publishing house&quot;,&quot;Medicine&quot;,&quot;Activity&quot;,&quot;Education&quot;,&quot;Software&quot;,&quot;Advertising&quot;,&quot;Repair&quot;,&quot;Beauty Salo&quot;,&quot;Construction&quot;,&quot;Transportation&quot;,]' rel="tooltip"  title="Категория магазина">
					</td>
					<td>
						<input type="text" class="span7" placeholder="Услуга"  data-provide="typeahead" data-source='[...]' rel="tooltip"  title="Выберите вид товаров, которые можно купить в Вашем магазине">
					</td>
					<td>
					<td>
					<div class="span1">
					</div>
					</td>
				</tr>
				<tr>
					<td>Теле-видео-аудио-фото
					</td>
					<td>Видеокамеры 
					</td>
					<td>
						<input type="checkbox" value="">
					</td>
				</tr>
			</table>
			</div>
		</div>
	</div>
	<div class="accordion-group">
		<div id="collapseKotacts" class="accordion-body collapse">
			<div class="accordion-inner">
				<div class="alert alert-info">
					<strong><?php echo $l['ORGANISATION_TYPE']; ?></strong>
				</div>
				<div class="row">
					<div class="span2">
						<label><h6><p class="text-info" ><?php echo $l['OWNERSHIP_FORM']; ?></p></h6></label>
							<select class="selectpicker span2" data-style="btn-primary" data-container="body">
								<option  value="STATE"><?php echo $l['STATE']; ?></option>
								<option  value="PRIVATE_ENTERPRISE"><?php echo $l['PRIVATE_ENTERPRISE']; ?></option>
								<option  value="INDIVIDUAL_FACE"><?php echo $l['INDIVIDUAL_FACE']; ?></option>
							</select>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['THE_NUMBER_OF']; ?> <i class="icon-exclamation-sign" rel="tooltip"  title="<?php echo $l['HOW_MANY']; ?>"></i></h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="5"></input>
								</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['MODE_OF_OPERATION']; ?> <i class="icon-exclamation-sign" rel="tooltip"  title="<?php echo $l['AROUND_THE_CLOCK']; ?>"></i></p></h6></label>
								<div class="btn-group">
									<select class="selectpicker" data-style="btn-primary" data-width="67px" data-container="body">
										<option>0:00</option>
										<option>0:30</option>
										<option>1:00</option>
										<option>1:30</option>
										<option>2:00</option>
										<option>2:30</option>
										<option>3:00</option>
										<option>3:30</option>
										<option>4:00</option>
										<option>4:00</option>
										<option>4:30</option>
										<option>4:30</option>
										<option>5:00</option>
										<option>5:30</option>
										<option>6:00</option>
										<option>6:30</option>
										<option>7:00</option>
										<option>7:30</option>
										<option>8:00</option>
										<option>8:30</option>
										<option>9:00</option>
										<option>9:30</option>
										<option>10:00</option>
										<option>10:30</option>
										<option>11:00</option>
										<option>11:30</option>
										<option>12:00</option>
										<option>12:30</option>
										<option>13:00</option>
										<option>13:30</option>
										<option>14:00</option>
										<option>14:30</option>
										<option>15:00</option>
										<option>15:30</option>
										<option>16:00</option>
										<option>16:30</option>
										<option>17:00</option>
										<option>17:30</option>
										<option>18:00</option>
										<option>18:30</option>
										<option>19:00</option>
										<option>19:30</option>
										<option>20:00</option>
										<option>20:30</option>
										<option>21:00</option>
										<option>21:30</option>
										<option>22:00</option>
										<option>22:30</option>
										<option>23:00</option>
										<option>23:30</option>
										<option>24:00</option>
											</select>
									<select class="selectpicker" data-style="btn-primary" data-width="75px" data-container="body">
										<option>24:00</option>
										<option>23:30</option>
										<option>23:00</option>
										<option>22:30</option>
										<option>22:00</option>
										<option>21:30</option>
										<option>21:00</option>
										<option>20:30</option>
										<option>20:00</option>
										<option>19:30</option>
										<option>19:00</option>
										<option>18:30</option>
										<option>18:00</option>
										<option>17:30</option>
										<option>17:00</option>
										<option>16:30</option>
										<option>16:00</option>
										<option>15:30</option>
										<option>15:00</option>
										<option>14:30</option>
										<option>14:00</option>
										<option>13:30</option>
										<option>13:00</option>
										<option>12:30</option>
										<option>12:00</option>
										<option>11:30</option>
										<option>11:00</option>
										<option>10:30</option>
										<option>10:00</option>
										<option>9:30</option>
										<option>9:00</option>
										<option>8:30</option>
										<option>8:00</option>
										<option>7:30</option>
										<option>7:00</option>
										<option>6:30</option>
										<option>6:00</option>
										<option>5:30</option>
										<option>5:00</option>
										<option>4:30</option>
										<option>4:30</option>
										<option>4:00</option>
										<option>4:00</option>
										<option>3:30</option>
										<option>3:00</option>
										<option>2:30</option>
										<option>2:00</option>
										<option>1:30</option>
										<option>1:00</option>
										<option>0:30</option>
										<option>0:00</option>
									</select>
								</div>
						</div>
					</div>
					<div class="span2">
					<label><h6><p class="text-info"><?php echo $l['DAYS_OFF']; ?></p></h6></label>
						<select class="selectpicker span2" multiple data-style="btn-primary">
							<option  value="NO"><?php echo $l['N_O']; ?></option>
							<option  value="MONDAY"><?php echo $l['MONDAY']; ?></option>
							<option  value="TUESDAY"><?php echo $l['TUESDAY']; ?></option>
							<option  value="WEDNESDAY"><?php echo $l['WEDNESDAY']; ?></option>
							<option  value="THURSDAY"><?php echo $l['THURSDAY']; ?></option>
							<option  value="FRIDAY"><?php echo $l['FRIDAY']; ?></option>
							<option  value="SATURDAY"><?php echo $l['SATURDAY']; ?></option>
							<option  value="SUNDAY"><?php echo $l['SUNDAY']; ?></option>
						</select>
					</div>
					<div class="span2">
					<label><h6><p class="text-info"><?php echo $l['PAYMENT_METHODS']; ?></p></h6></label>
						<select class="selectpicker span2" multiple data-style="btn-primary">
							<optgroup label="<?php echo $l['CASHLESS_PAYMENTS']; ?>">
								<option  value="AMERICAN_EXPRESS"><?php echo $l['AMERICAN_EXPRESS']; ?></option>
								<option  value="CIRRUS"><?php echo $l['CIRRUS']; ?></option>
								<option  value="DINERS_CLUB"><?php echo $l['DINERS_CLUB']; ?></option>
								<option  value="EUROCARD"><?php echo $l['EUROCARD']; ?></option>
								<option  value="JCB"><?php echo $l['JCB']; ?></option>
								<option  value="MAESTRO"><?php echo $l['MAESTRO']; ?></option>
								<option  value="MASTERCARD"><?php echo $l['MASTERCARD']; ?></option>
								<option  value="UNION"><?php echo $l['UNION']; ?></option>
								<option  value="VISA"><?php echo $l['VISA']; ?></option>
								<option  value="VISA_ELECTRON"><?php echo $l['VISA_ELECTRON']; ?></option>
							</optgroup>
							<optgroup label="<?php echo $l['CASH']; ?>">
									<option value="USD" data-subtext="<?php echo $l['USD']; ?>">USD</option>
									<option value="EUR" data-subtext="<?php echo $l['EUR']; ?>">EUR</option>
									<option value="CNY" data-subtext="<?php echo $l['CNY']; ?>">CNY</option>
									<option value="JPY" data-subtext="<?php echo $l['JPY']; ?>">JPY</option>
									<option value="INR" data-subtext="<?php echo $l['INR']; ?>">INR</option>
									<option value="BRL" data-subtext="<?php echo $l['BRL']; ?>">BRL</option>
									<option value="GBP" data-subtext="<?php echo $l['GBP']; ?>">GBP</option>
									<option value="RUB" data-subtext="<?php echo $l['RUB']; ?>">RUB</option>
									<option value="AED" data-subtext="<?php echo $l['AED']; ?>">AED</option>
									<option value="ARS" data-subtext="<?php echo $l['ARS']; ?>">ARS</option>
									<option value="AUD" data-subtext="<?php echo $l['AUD']; ?>">AUD</option>
									<option value="BGN" data-subtext="<?php echo $l['BGN']; ?>">BGN</option>
									<option value="BHD" data-subtext="<?php echo $l['BHD']; ?>">BHD</option>
									<option value="CAD" data-subtext="<?php echo $l['CAD']; ?>">CAD</option>
									<option value="CHF" data-subtext="<?php echo $l['CHF']; ?>">CHF</option>
									<option value="CLP" data-subtext="<?php echo $l['CLP']; ?>">CLP</option>
									<option value="COP" data-subtext="<?php echo $l['COP']; ?>">COP</option>
									<option value="CRC" data-subtext="<?php echo $l['CRC']; ?>">CRC</option>
									<option value="CZK" data-subtext="<?php echo $l['CZK']; ?>">CZK</option>
									<option value="DOP" data-subtext="<?php echo $l['DOP']; ?>">DOP</option>
									<option value="DZD" data-subtext="<?php echo $l['DZD']; ?>">DZD</option>
									<option value="EEK" data-subtext="<?php echo $l['EEK']; ?>">EEK</option>
									<option value="EGP" data-subtext="<?php echo $l['EGP']; ?>">EGP</option>
									<option value="HKD" data-subtext="<?php echo $l['HKD']; ?>">HKD</option>
									<option value="HRK" data-subtext="<?php echo $l['HRK']; ?>">HRK</option>
									<option value="HUF" data-subtext="<?php echo $l['HUF']; ?>">HUF</option>
									<option value="ILS" data-subtext="<?php echo $l['ILS']; ?>">ILS</option>
									<option value="ISK" data-subtext="<?php echo $l['ISK']; ?>">ISK</option>
									<option value="JOD" data-subtext="<?php echo $l['JOD']; ?>">JOD</option>
									<option value="KES" data-subtext="<?php echo $l['KES']; ?>">KES</option>
									<option value="KRW" data-subtext="<?php echo $l['KRW']; ?>">KRW</option>
									<option value="KWD" data-subtext="<?php echo $l['KWD']; ?>">KWD</option>
									<option value="LBP" data-subtext="<?php echo $l['LBP']; ?>">LBP</option>
									<option value="LKR" data-subtext="<?php echo $l['LKR']; ?>">LKR</option>
									<option value="MAD" data-subtext="<?php echo $l['MAD']; ?>">MAD</option>
									<option value="MUR" data-subtext="<?php echo $l['MUR']; ?>">MUR</option>
									<option value="MXN" data-subtext="<?php echo $l['MXN']; ?>">MXN</option>
									<option value="MYR" data-subtext="<?php echo $l['MYR']; ?>">MYR</option>
									<option value="NOK" data-subtext="<?php echo $l['NOK']; ?>">NOK</option>
									<option value="NZD" data-subtext="<?php echo $l['NZD']; ?>">NZD</option>
									<option value="OMR" data-subtext="<?php echo $l['OMR']; ?>">OMR</option>
									<option value="PEN" data-subtext="<?php echo $l['PEN']; ?>">PEN</option>
									<option value="PHP" data-subtext="<?php echo $l['PHP']; ?>">PHP</option>
									<option value="PKR" data-subtext="<?php echo $l['PKR']; ?>">PKR</option>
									<option value="PLN" data-subtext="<?php echo $l['PLN']; ?>">PLN</option>
									<option value="QAR" data-subtext="<?php echo $l['QAR']; ?>">QAR</option>
									<option value="RON" data-subtext="<?php echo $l['RON']; ?>">RON</option>
									<option value="SAR" data-subtext="<?php echo $l['SAR']; ?>">SAR</option>
									<option value="SEK" data-subtext="<?php echo $l['SEK']; ?>">SEK</option>
									<option value="THB" data-subtext="<?php echo $l['THB']; ?>">THB</option>
									<option value="TMM" data-subtext="<?php echo $l['TMM']; ?>">TMM</option>
									<option value="TND" data-subtext="<?php echo $l['TND']; ?>">TND</option>
									<option value="TRL" data-subtext="<?php echo $l['TRL']; ?>">TRL</option>
									<option value="TTD" data-subtext="<?php echo $l['TTD']; ?>">TTD</option>
									<option value="TWD" data-subtext="<?php echo $l['TWD']; ?>">TWD</option>
									<option value="VEB" data-subtext="<?php echo $l['VEB']; ?>">VEB</option>
									<option value="VND" data-subtext="<?php echo $l['VND']; ?>">VND</option>
									<option value="ZAR" data-subtext="<?php echo $l['ZAR']; ?>">ZAR</option>
							</optgroup>
						</select>
					</div>
					<div class="span2">
					<label><h6><p class="text-info"><?php echo $l['LANGUAGE_STAFF']; ?></p></h6></label>
						<select class="selectpicker span2" multiple data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'LANGUAGE_STAFF?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>

						</select>
					</div>
				</div>
				<div class="alert alert-info">
					<strong><?php echo $l['LOCATION']; ?></strong>
				</div>
				<div class="row">
					<div class="span2">
						<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['COUNTRY']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Countries?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label><h6><p class="text-info"><?php echo $l['CITY']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Cityes?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['DISTRICT']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'District?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label><h6><p class="text-info"><?php echo $l['METRO']; ?></p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Metro?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['STREET']; ?>*</h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Street?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label class="control-label" for="inputinfo"><h6><?php echo $l['HOUSE_NUMBER']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="64/2">
								</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['APARTMENT_NUMBE']; ?></h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="2/56">
							</div>
							<label class="control-label" for="inputinfo"><h6><?php echo $l['REFINE_ON_THE_M']; ?></h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['MAP']; ?>">
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['CELL_PHONES']; ?>*</h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="+*(***)***-**-**,...">
							</div>
							<label class="control-label" class="span4" for="inputinfo"><h6><?php echo $l['ADDITIONALLY']; ?></h6></label>
							<div class="controls">
								<input type="text" class="span4" id="inputinfo" placeholder="<?php echo $l['SKYPE']; ?>">
							</div>
						</div>
					</div>
					<div class="span2">
					<label><h6><p class="text-info"><?php echo $l['FILIAL']; ?></p></h6></label>
						<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#ResFilial"><?php echo $l['ADD']; ?></button>
					</div>
				</div>
				<div id="ResFilial" class="collapse">
					<div class="alert alert-info">
						<strong><?php echo $l['Y_FILIAL']; ?></strong>
					</div>
					<div class="row">
						<div class="span2">
							<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['COUNTRY']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Countries?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label><h6><p class="text-info"><?php echo $l['CITY']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Cityes?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['DISTRICT']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'District?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label><h6><p class="text-info"><?php echo $l['METRO']; ?></p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Metro?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['STREET']; ?>*</h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Street?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
								<label class="control-label" for="inputinfo"><h6><?php echo $l['HOUSE_NUMBER']; ?></h6></label>
									<div class="controls">
										<input type="text" class="span2" id="inputinfo" placeholder="64/2">
									</div>
							</div>
						</div>
						<div class="span2">
							<div class="control-group info">
								<label class="control-label" for="inputinfo"><h6><?php echo $l['APARTMENT_NUMBE']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="2/56">
								</div>
								<label class="control-label" for="inputinfo"><h6><?php echo $l['REFINE_ON_THE_M']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['MAP']; ?>">
								</div>
							</div>
						</div>
						<div class="span2">
							<div class="control-group info">
								<label class="control-label" for="inputinfo"><h6><?php echo $l['CELL_PHONES']; ?>*</h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="+*(***)***-**-**,...">
								</div>
								<label class="control-label" class="span4" for="inputinfo"><h6><?php echo $l['ADDITIONALLY']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span4" id="inputinfo" placeholder="<?php echo $l['SKYPE']; ?>">
								</div>
							</div>
						</div>
						<div class="span2">
						<label><h6><p class="text-info"><?php echo $l['FILIAL']; ?></p></h6></label>
							<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#demo1"><?php echo $l['ADD']; ?></button>
						</div>
					</div>
				</div>
				<div id="demo1" class="collapse">
					<div class="alert alert-info">
						<strong><?php echo $l['Y_FILIAL']; ?></strong>
					</div>
					<div class="row">
						<div class="span2">
							<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['COUNTRY']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Countries?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label><h6><p class="text-info"><?php echo $l['CITY']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Cityes?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label><h6><p class="text-info"><?php echo $l['DISTRICT']; ?>*</p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'District?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
							<label><h6><p class="text-info"><?php echo $l['METRO']; ?></p></h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Metro?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['STREET']; ?>*</h6></label>
								<select class="selectpicker span2" data-style="btn-primary">
							<?php/*
					   
									include("../lib/Catalog.php");
									$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'Street?lang='.$_COOKIE['lang'], null);
									$data = json_decode( $data, true );
									


									foreach( $data as $it )
									{
										echo "<option>".$it."</option>";
									}
							*/?>
								</select>
								<label class="control-label" for="inputinfo"><h6><?php echo $l['HOUSE_NUMBER']; ?></h6></label>
									<div class="controls">
										<input type="text" class="span2" id="inputinfo" placeholder="64/2">
									</div>
							</div>
						</div>
						<div class="span2">
							<div class="control-group info">
								<label class="control-label" for="inputinfo"><h6><?php echo $l['APARTMENT_NUMBE']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="2/56">
								</div>
								<label class="control-label" for="inputinfo"><h6><?php echo $l['REFINE_ON_THE_M']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['MAP']; ?>">
								</div>
							</div>
						</div>
						<div class="span2">
							<div class="control-group info">
								<label class="control-label" for="inputinfo"><h6><?php echo $l['CELL_PHONES']; ?>*</h6></label>
								<div class="controls">
									<input type="text" class="span2" id="inputinfo" placeholder="+*(***)***-**-**,...">
								</div>
								<label class="control-label" class="span4" for="inputinfo"><h6><?php echo $l['ADDITIONALLY']; ?></h6></label>
								<div class="controls">
									<input type="text" class="span4" id="inputinfo" placeholder="<?php echo $l['SKYPE']; ?>">
								</div>
							</div>
						</div>
						<div class="span2">
						<label><h6><p class="text-info"><?php echo $l['FILIAL']; ?></p></h6></label>
							<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#demo"><?php echo $l['ADD']; ?></button>
						</div>
					</div>
				</div>
				<div class="alert alert-info">
					<strong><?php echo $l['CONTACTS']; ?></strong>
				</div>
				<div class="row">
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['NAME']; ?>*</h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['NATIVE_LANGUA']; ?>">
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['NAME']; ?>* <i class="icon-exclamation-sign"   rel="tooltip"  title="<?php echo $l['TO_THE_FOREIGNERS']; ?>"></i></h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['LATYN']; ?>">
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['SAYT']; ?></h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="http\\kafe.com">
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['EMAIL']; ?>*</h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['EMAIL']; ?>">
							</div>
						</div>
					</div>
					<div class="span2">
						<div class="control-group info">
							<label class="control-label" for="inputinfo"><h6><?php echo $l['PASSWORD_']; ?> <i class="icon-exclamation-sign"   rel="tooltip"  title="<?php echo $l['IF_YOU_ARE_NOT']; ?>"></i></h6></label>
							<div class="controls">
								<input type="text" class="span2" id="inputinfo" placeholder="<?php echo $l['HAVING_AN_ACCOU']; ?>">
							</div>
						</div>
					</div>
					<div class="span2">
					<label><h6><p class="text-info"><?php echo $l['ADD_FOTO']; ?></p></h6></label>
						<button class="btn btn-primary btn-block" type="button" class="accordion-toggle" data-toggle="collapse" data-parent="#accordionServise" href="#collapseTwo"><?php echo $l['ADD']; ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="accordion-group">
		<div id="collapseTwo" class="accordion-body collapse">
			<div class="accordion-inner">
				<div class="alert alert-info">
					<strong>Ваше объявление успешно добавленно! Добавьте фотографии.</strong>
				</div>
				<div class="row">
					<div class="span3">
						<div class="alert alert-info" rel="tooltip"  title="При выборе способа выделение банером, будет отоброжаться это изображение">
							<center><strong>Главное изображение</strong></center>
							<div class="fileupload fileupload-new" data-provides="fileupload">
							  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
							  <div>
							<center>
							<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
								<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
							</center>
							  </div>
							</div>
						</div>
					</div>
					<div class="span3">
						<label>Изображение 2</label>
							<div class="fileupload fileupload-new" data-provides="fileupload">
							  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
							  <div>
							<center>
							<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
								<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
							</center>
							  </div>
							</div>
					</div>
					<div class="span3">
						<label>Изображение 3</label>
							<div class="fileupload fileupload-new" data-provides="fileupload">
							  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
							  <div>
							<center>
							<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
								<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
							</center>
							  </div>
							</div>
					</div>
					<div class="span3">
						<label>Изображение 4</label>
							<div class="fileupload fileupload-new" data-provides="fileupload">
							  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
							  <div>
							<center>
							<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
								<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
							</center>
							  </div>
							</div>
					</div>
				</div>
						<button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#demo4"><?php echo $l['ADD']; ?> еще 4</button>
				<div id="demo4" class="collapse">
					<div class="row">
						<div class="span3">
								<label>Изображение 5</label>
								<div class="fileupload fileupload-new" data-provides="fileupload">
								  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
								  <div>
								<center>
								<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
									<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
								</center>
								  </div>
								</div>
						</div>
						<div class="span3">
							<label>Изображение 6</label>
								<div class="fileupload fileupload-new" data-provides="fileupload">
								  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
								  <div>
								<center>
								<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
									<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
								</center>
								  </div>
								</div>
						</div>
						<div class="span3">
							<label>Изображение 7</label>
								<div class="fileupload fileupload-new" data-provides="fileupload">
								  <div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
								  <div>
								<center>
								<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
									<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
								</center>
								  </div>
								</div>
						</div>
						<div class="span3">
							<label>Изображение 8</label>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="fileupload-preview thumbnail" style="width: 175px; height: 150px;"></div>
								  <div>
								<center>
								<span class="btn btn-file btn-primary btn-mini"><span class="fileupload-new">Выбрать</span><span class="fileupload-exists">Изменить</span><input type="file" /></span>
									<a href="#" class="btn fileupload-exists btn-primary btn-mini" data-dismiss="fileupload">Удалить</a>
								</center>
								  </div>
								</div>
						</div>
					</div>
				</div>
					<p>
						<button class="btn btn-warning btn-block" type="button" data-toggle="collapse" data-target="#UploadImageRestoran">Загрузить все изображения</button>
					</p>
					<div id="UploadImageRestoran" class="collapse">
						<div class="progress progress-danger progress-striped">
							<div class="bar" style="width: 80%"></div>
						</div>
					</div>
					<p>
						<button class="btn btn-warning btn-block" type="button"  class="accordion-toggle" data-toggle="collapse" data-parent="#accordionServise" href="#collapseResPromo">Все готово, не пуха не пера..</button>
					</p>
			</div>
		</div>
	</div>
	<div class="accordion-group">
		<div id="collapseResPromo" class="accordion-body collapse">
			<div class="accordion-inner">
						<div class="row">
							<div class="span12">
								<legend><h4><?php echo $l['YOUR_AD_HAS_THE']; ?></h4></legend>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<blockquote>
									<p><h5>
									<?php echo $l['YOUR_AD_HAS_THE_SER']; ?>
									</br>
									<?php echo $l['THE_ANNOUNCEMEN_38']; ?>, <em><?php echo $l['THE_LANGUAGE_PA']; ?></em>
									</br>
									<?php echo $l['THIS_AD_IS_OPTI']; ?>
									</h5></p>
								</blockquote>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<legend><h4><?php echo $l['WANT_MORE']; ?></h4></legend>
							</div>
						</div>
						<div class="row">
							<div class="span6">
								<blockquote>
									<p><h5>
									<?php echo $l['SECTION_FIND_SER']; ?>
									</br>
									<?php echo $l['SHOW_MY_ADS']; ?>
									</br>
									<?php echo $l['CLICK_ON_THE']; ?> <i class="icon-cog"></i> <?php echo $l['NEXT_TO_THE_DES']; ?>
									</br>
									<?php echo $l['SELECT_ONE_O']; ?>
									</br>
									</h5></p>
								</blockquote>
							</div>
							<div class="span6">
								<li class="span5">
									<div class="thumbnail"  href="#">
										<center><h5><?php echo $l['HIGHLIGHT_THIS']; ?> (<?php echo $l['OPENS_A_MODAL_W']; ?>)</h5></center>
									</div>
								</li>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<legend><h4><?php echo $l['BECOME_OUR_PART']; ?></h4></legend>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<blockquote>
									<p><h5>
									<?php echo $l['AFFILIATE_PROGR']; ?> 
									</br>
									<center><a class="btn btn-link" href="../kurator"><?php echo $l['GO_TO_AFFILIATE']; ?></a></center>
									</h5></p>
								</blockquote>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<legend><h4><?php echo $l['THE_BOOK_OF_COM']; ?></h4></legend>
							</div>
						</div>
						<div class="row">
							<div class="span12">
								<blockquote>
									<p><h5>
									<?php echo $l['POINT_TO_SHORTC']; ?>  
									</br>
									</h5></p>
								</blockquote>
								<center><textarea rows="3"></textarea></center>
							<a class="btn btn-primary btn-block" href="index.php"><?php echo $l['SEND_OR_SIMPLY']; ?></a>
							</div>
						</div>
					</div>
				</div>
   			</div>
		</div>
	</div>
</body>
<script>
        $(window).on('load', function () {

            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });

            // $('.selectpicker').selectpicker('hide');
        });

</script>
</html>