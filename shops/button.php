		<div class="row">
			<div class="span3">
				<a class="btn btn-block btn-primary" href="addnew.php"><?php echo $l['add_announcement']; ?></a>
			</div>
			<div class="span3">
				<a class="btn btn-block btn-primary" href="index.php"><?php echo $l['all_ads']; ?></a>
			</div>
			<div class="span3">
				<a class="btn btn-block btn-primary" href="myads.php"><?php echo $l['my_ads']; ?></a>
			</div>
			<div class="span3">
				<a class="btn btn-block btn-primary" onclick="location.href='../'"><?php echo $l['go_back']; ?></a>
			</div>
		</div>
