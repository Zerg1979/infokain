<div id="trans_search_div">




<div class="row">
	<div class="span6">
		<div class="part_border_1 car_color"></div>
			<div class="alert alert-error">
				<a class="close" data-dismiss="alert" href="#">&times;</a>
					В этой области храняться столбцы с заданным параметром</br><strong>Внимание! Изъять столбец из таблицы можно только с одним-сохраненным параметром!</strong> 
			</div>
			<div class="alert alert-error" id="aft">
				Выбранные параметры
			</div>
	<div class="part_border_1 car_color"></div>
	</div>
	<div class="span6">
	<div class="part_border_1 car_color"></div>
		<div class="alert alert-error">
		<a class="close" data-dismiss="alert" href="#">&times;</a>
			В этой области хранятся столбцы которые Вы можете добавить в таблицу</br><strong>Внимание! Вы можете добавить все параметры в таблицу, но она может оказаться нечитаемой!</strong>
		</div>
		<div class="alert alert-error" id="aft2">
			Столбцы с параметрами которые можно добавить
		</div>
			
		<div class="part_border_1 car_color"></div>
	</div>
</div>
<div>

<!-- POPOVER STRING -->
<div id="search_pop"  class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title">A Title</h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; margin-left:20px; right:10px; top:6px">Закрыть</a><p><div class="spax">
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i>Сохранить параметр и изъять столбец</a><br><br>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i>Изъять столбец без параметра</a><br><br>
<input type="text" class="search_column" style="margin-left:20px;" placeholder="Начните вводить" data-provide="typeahead"></div>
</div>
</div>
</div>
<!-- POPOVER NUMERIC -->
<div id="search_pop2" style="display:none" class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title">A Title</h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; right:10px; top:6px">Закрыть</a><p><div style="float:left; width:300px; height:150px">
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i>Сохранить параметр и изъять столбец</a><br><br>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i>Изъять столбец без параметра</a><br><br>
<center>

</center>
</div>
</div>
</div>
</div>
<!-- END POPOVER -->

<div id="posY">
<div style="position:relative; z-index:10; bottom:-55px;" class="span2">
<label>Показано <select data-size="4" style="width:60px; float:left" id="ad_number"  class="selectpicker span1"  size="1" name="transport_search_table_length" aria-controls="transport_search_table">
<option value="10" selected="selected">10</option>
<option value="25">25</option><option value="50">50</option>
<option value="100">100</option>
</select></label></div>
<div style="position:relative; float:left; z-index:10; bottom:-55px;" class="span2">
<button class="btn" type="button" data-toggle="button-checkbox"  style="position:relative;" onclick="showPhotos()" >С фото</button>

</div>
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="transport_search_table">
    
    <thead>
    
        <tr>
        	<th width="4px">
           
            </th>
			<th>
            <a col_id="1" title="Страна" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="2" title="Город" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="3" title="Валюта" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="4" title="Действие" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="5" title="Тип" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="6" title="Кузов" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
            <a col_id="7" title="Марка" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
			<th >
           <a col_id="8" title="Модель" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
           <a col_id="9" title="Двери(шт)" div="trans_search_div" onclick="showPopoverNum(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="10" title="Привод" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="11" title="Топливо" div="trans_search_div" onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="12" title="Цена" div="trans_search_div" onclick="showPopoverNum(this)"><i class="icon-search"></i>
            </th>
            <th >
            <a col_id="13" title="Дата" div="trans_search_div"><i class="icon-search"></i>
            </th>
             <th >
            <a col_id="14" title="ID" div="trans_search_div"  onclick="showPopover(this)"><i class="icon-search"></i>
            </th>
		</tr>
        <tr role="row">
		<th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-sort aria-label><div class="DataTables_sort_wrapper"></div></th>
		<th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-sort="ascending" aria-label="Страна: activate to sort column descending"><div class="DataTables_sort_wrapper">Страна</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Город: activate to sort column ascending"><div class="DataTables_sort_wrapper">Город</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Валюта: activate to sort column ascending"><div class="DataTables_sort_wrapper">Валюта</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Действие: activate to sort column ascending"><div class="DataTables_sort_wrapper">Действие</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Тип: activate to sort column ascending"><div class="DataTables_sort_wrapper">Тип</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Кузов: activate to sort column ascending"><div class="DataTables_sort_wrapper">Кузов</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Марка: activate to sort column ascending"><div class="DataTables_sort_wrapper">Марка</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Модель: activate to sort column ascending"><div class="DataTables_sort_wrapper">Модель</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Двери(шт): activate to sort column ascending"><div class="DataTables_sort_wrapper">Двери(шт)</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Привод: activate to sort column ascending"><div class="DataTables_sort_wrapper">Привод</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Топливо: activate to sort column ascending"><div class="DataTables_sort_wrapper">Топливо</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Цена: activate to sort column ascending"><div class="DataTables_sort_wrapper">Цена</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">Дата</div></th>
		<th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">ID</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Цена: activate to sort column ascending"><div class="DataTables_sort_wrapper">Фото</div></th>
        <th class="ui-state-default" role="columnheader" tabindex="0" aria-controls="transport_search_table" rowspan="1" colspan="1" style="width: 0px;" aria-label="Дата: activate to sort column ascending"><div class="DataTables_sort_wrapper">VIP</div></th>
        </tr>
	</thead>
        
	<tbody>
		
	</tbody>
	<tfoot>
		<tr>
			<th ></th>
			<th >Страна</th>
			<th >Город</th>
			<th >Валюта</th>
			<th >Действие</th>
			<th >Тип</th>
			<th >Кузов</th>
			<th >Марка</th>
			<th >Модель</th>
            <th >Двери(шт)</th>
            <th >Привод</th>
            <th >Топливо</th>
            <th >Цена</th>
            <th >Дата</th>
			<th >ID</th>
		</tr>
	</tfoot>
    </table> 
    </div>   
</div>
</div>