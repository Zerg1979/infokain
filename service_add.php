<?

$output = array(
	"aaData" => array()
);
$output['aaData'] = array(array(  
								  category => 'Ремонт', 
								  field => 'Легковые авто', 
								  service_name => 'Замена масла', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="remont_legkovoy_maslo" />'
								),
						   array(
							category => 'Ремонт', 
								  field => 'Легковые авто', 
								  service_name => 'Замена свечей', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="remont_legkovoy_svechi" />' 
								),
						   array( 
						   category => 'Ремонт', 
								  field => 'Легковые авто', 
								  service_name => 'Замена резины', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="remont_legkovoy_rezina" />' 
								),
							array(  
								  category => 'Ремонт', 
								  field => 'Грузовые авто', 
								  service_name => 'Замена масла', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="remont_gruzovoy_maslo" />' 
								),
						   array(
							category => 'Ремонт', 
								  field => 'Грузовые авто', 
								  service_name => 'Замена свечей', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="remont_gruzovoy_svechi" />' 
								),
						   array( 
						   category => 'Ремонт', 
								  field => 'Грузовые авто', 
								  service_name => 'Замена резины', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="remont_gruzovoy_rezina" />' 
								),
							array(  
								  category => 'Прошивка', 
								  field => 'Телефоны', 
								  service_name => 'Операционная система', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="proshivka_phone_os" />' 
								),
						   array(
							category => 'Прошивка', 
								  field => 'Телефоны', 
								  service_name => 'BlueTooth', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="proshivka_phone_bluetooth" />' 
								),
						   array( 
						   category => 'Прошивка', 
								  field => 'Телефоны', 
								  service_name => 'Разрешение камеры', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="proshivka_phone_camera" />' 
								),
							array( 
							category => 'Прошивка', 
								  field => 'ТВ тюнер', 
								  service_name => 'Евро каналы', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="proshivka_tvtuner_eurochannels" />' 
								),
						   array( 
						   category => 'Прошивка', 
								  field => 'ТВ тюнер', 
								  service_name => 'Музыкальные каналы', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="proshivka_tvtuner_muzicchannels" />' 
								),
						   array( 
						   category => 'Прошивка', 
								  field => 'ТВ тюнер', 
								  service_name => 'Мультфильмы', 
								  price => '<input type="text" style="width:46px" value="0" />', 
								  currency => '', 
								  checkbox => '<input type="checkbox" value="proshivka_tvtuner_cartoons" />' 
								)
);

function normJsonStr($str){
    $str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
    return iconv('cp1251', 'utf-8', $str);
}
echo normJsonStr(json_encode($output));


?>