<?php
	if ( isset($_GET['lang']) )
	{
		$lang = $_GET['lang'];
		SetCookie("lang", $lang, time()+7776000);
	}
	else
	{
		if ( empty( $_COOKIE['lang'] ) ) 
		{ 
			$languages = array( 'tm','ru','en','ar','bg','hu','vi','ht','el','da','he','id','es','it','ca','lv','lt','hm','de','nl','no','fa','pl','pt','ro','sk','sl','tr','uk','fi','fr','cs','sv','et','zh','th','hi','ja');		

			if ( in_array( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2), $languages ) )
			{ 
				$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
			} 
			else
			{
				$lang = "en";
			}
			SetCookie("lang", $lang, time()+7776000);
		}
		else
		{
			$lang = $_COOKIE['lang'];
		}
	}
	$l = parse_ini_file("../lang/" . $lang . ".ini");
?>
