<!-- 

	Google maps form 
	By Zerg 2014

	usage:
	1. include this maps.php into your page
	2. You need have input elements with class "maps"

-->

<style type="text/css">

.maps_form h4 {
	color: white;
}

.maps_form .modal-header {
	background-color: #51a351;
	border-top-left-radius: 6px;
	border-top-right-radius: 6px;
	-webkit-border-top-left-radius: 6px;
	-webkit-border-top-right-radius: 6px;
}

.modal-body {
	overflow: hidden !important;
}


</style>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">

var GM = function() {

	// Map elements
	this.mapEl = null;

	// Maps init
	this.init = function() {
		var scope = this;
		this.mapEl = $('.maps');
		if(this.mapEl.length == 0) return;
		
		this.mapEl.on('focus', function() {
			scope.showMaps();
		});

		$("#mapSave").on("click", function(){
      scope.saveMaps(); 
    });
	}

	// Show Maps
	this.showMaps = function() {
		var scope = this;
		var country = $("#select_dict_country").val();
		var city = $("#select_dict_cities").val();
		var address = country + " " + city;
		var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, 
    function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      scope.initMaps( results[0].geometry.location + '' );
	  	}
    });
		$("#maps").modal('show');
	}

	// Save Maps
	this.saveMaps = function() {
		$("#maps").modal('hide');
    $(".maps").val($("#mapCoordinates").text());  
	}

	// Init Maps
	this.initMaps = function(geoAddress) {
		geoAddress = geoAddress.substring( 1 , geoAddress.length -1 );
    $("#mapCoordinates").text( geoAddress );

    var latlngStr = geoAddress.split(',',2);
    var lat = parseFloat(latlngStr[0]);
    var lng = parseFloat(latlngStr[1]);
    var latlng = new google.maps.LatLng( lat, lng );
    var mapOptions = {
        zoom: 15,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    map = new google.maps.Map(document.getElementById('maps_modal'), mapOptions);

    var marker = new google.maps.Marker({
        position: latlng,
        title: 'Точка',
        map: map,
        draggable: true
    });

    google.maps.event.addListener(marker, 'drag', function() {
        $("#mapCoordinates").text(marker.getPosition());
    });
    
	}
}

$(function() {
	var gm = new GM();
	gm.init();
});

</script>



<div class="modal hide maps_form" id="maps" tabindex="-1" role="dialog" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Определение месторасположения на карте</h4>
    </div>

    <div class="modal-body row" >
        <div class="span8" id="maps_modal" style="height:400px; border:1px solid #ccc;"></div>
        <div class="span3" id="maps_info" style="height:400px;">
            <dl>
            <dt>Координаты маркера:</dt>
            <dd id="mapCoordinates">...</dd>
            <dt><input id="mapAdres" type="hidden" value=""/></dt>
            <dt>Перетащите маркер если не точно</dt>
            </dl>
        </div>
    </div>

    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
        <button class="btn btn-success" id="mapSave" >Сохранить</button>
    </div>
</div>