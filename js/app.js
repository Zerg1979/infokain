var InfokainApp = function() {

	// Адресная книга
	this.addressBook = new AddressBook();

	// Книга услуг
	this.servicesBook = new ServicesBook();

	// Услуги (приложение)
	this.services = new InfokainServices();

	// Книга магазинов
	this.shopsBook = new ShopsBook();

	// Магазины (приложение)
	this.shops = new InfokainShops();

	// Обработчики форм
	this.formUtils = new FormUtils();

	// Инициализация приложения
	this.init = function() {
		var scope = this;
		this.bindBootstarpElements();
		this.progress.start('#progress-id');
		// Инициализация адресной книги
		this.addressBook.init(function() {
			
			// Инициализация текущего раздела системы
			if(!AppRouteInfo) {
				scope.progress.done();
				alert("Rouning failed");
			}

			switch(AppRouteInfo.id) {

				// Инициализация услуг
				case 0:
					scope.servicesBook.init(function() {
						// Инициализация услуг (приложения)
						scope.services.init(function() {
						scope.progress.done();
					});
					});
					break;

				// Инициализация магазинов
				case 1:
					scope.shopsBook.init(function() {
						// Инициализация магазинов (приложения)
						scope.shops.init(function() {
						scope.progress.done();
					});
					});
					break;
			}
		});
	}

	// Создаем елементы управления bootstrap
	this.bindBootstarpElements = function() {
		// Создаем tooltip
		$('body').tooltip({ selector: "[rel=tooltip]", placement: "left" });
		$('#currency').selectpicker({'selectedText': 'cat'});
	}

	// Прогресс
	this.progress = {
		start: function(selector) {
			$('#table-place').css('opacity', '.5');
			NProgress.configure({target: selector});
			NProgress.start();
		},
		done: function() {
			NProgress.done();
			$('#table-place').css('opacity', '1');
		}	
	}
}

var infApp = new InfokainApp();

$(function () {
	if(infApp) infApp.init();
});