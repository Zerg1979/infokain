/*
	Form Utils
	By Zerg 2014
*/

var FormUtils = function() {

	// Элементы управления
	
	this.setSelectDisabled = function(el, disabled) {
		$(el).attr('disabled', disabled).trigger("liszt:updated");
		$(el).selectpicker('refresh');
	}

	this.setEnabled = function() {

	}

	// Валидаторы
	this.numberInputValidator = function(el) {
		var owner = $(el).parent().parent();
		var val = Number($(el).val());
		if(isNaN(val) || val == 0) {
			owner.removeClass("success").addClass("error");
		} else {
			owner.removeClass("error").addClass("success");
		}
	}

	// Информативное окно
	this.showInfo = function(msg, style) {
		this.showWindow({
				width: 350,
				title: 'Информация',
				message: msg,
				buttons: [
					{
						ui: style,
						text: 'Ок'
					}
				]
			});
	}

	// Всплывающее окно
	/*
	options = {
		width: 150,
		title: 'test',
		message: 'example',
		buttons: [
			{
				ui: 'default/primary/success',
				text: 'OK',
				onclick: function() { .... }
			},
			{
				text: 'OK',
				onclick: function() { .... }
			}
		]
	}
	*/
	this.showWindow = function(options) {
		if(!options.width) options.width = 400;
		if(!options.title) options.title = '';
		if(!options.message) options.message = '';
		if(!options.buttons) options.buttons = [];
		$("#modal-form-id").remove();
		var content = '<div style="width: '+options.width+'px;" id="modal-form-id" class="modal fade">'+
									'  <div style="width: '+options.width+'px;" class="modal-dialog">'+
									'    <div class="modal-content">'+
									'      <div class="modal-header">'+
									'        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
									'        <h4 class="modal-title">'+options.title+'</h4>'+
									'      </div>'+
									'      <div class="modal-body">'+
									'        <p>' + options.message + '</p>'+
									'      </div>'+
									'      <div class="modal-footer">';
									for(var i = 0; i < options.buttons.length; i++) {
										if(!options.buttons[i].ui) options.buttons[i].ui = "default";
										if(!options.buttons[i].text) continue;
										content+='<button id="mf-button-'+i+'" type="button" class="btn btn-'+options.buttons[i].ui+'" data-dismiss="modal">'+options.buttons[i].text+'</button>';
									}
				content+=	'      </div>'+
									'    </div><!-- /.modal-content -->'+
									'  </div><!-- /.modal-dialog -->'+
									'</div><!-- /.modal -->';
		$("body").append(content);
		for(var i = 0; i < options.buttons.length; i++)
			$("#mf-button-"+i).on("click", options.buttons[i].onclick);

		var left = $(document).width()/2-options.width/2;		
		$("#modal-form-id").css("left", left);
		$("#modal-form-id").css("margin-left", 0);
		$("#modal-form-id").modal('show');
	}
}