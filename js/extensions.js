if(!String.prototype.addSlasches)
{
  String.prototype.escape = function() {
    return this.replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
  }
}