/*
	Curl
	By Zerg 2014
*/

var ICurl = {

	// Загрузка справочника
	loadDict: function(dictName, params, success) {
		var scope = this;
		ubescapedParams = {};
		for(var key in params) if(params.hasOwnProperty(key)) ubescapedParams[unescape(key)] = unescape(params[key]);

		$.ajax({
			type: 'get',
			url: '/api/curl.php?method='+dictName,
			data: ubescapedParams,
			dataType: 'json',
			success: function(res) {
				if(res) {
					var dict = [];
					for(var key in res){
				    if(res.hasOwnProperty(key)){
				      dict.push({
				      	name: escape(key),
				      	value: res[key]
				      });
				    }
  				}
  				success(dict);
				} else { scope._onError(); }
			},
			error: function() {
				scope._onError();
			}
		});	
	},

	// Обработчик ошибок
	_onError: function() {
		alert("Ошибка сервиса, попробуйте позже.");
	}
}