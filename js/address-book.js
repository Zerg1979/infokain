/*
	Address Book
	By Zerg 2014
*/

var AddressBook = function() {

	// Страны
	this.countries = null;

	// Города текущей страны
	this.cities = null;

	// Языки
	this.languages = null;

	// Текущая страна
	this.currentCountry = null;

	// Инициализация
	this.init = function(success) {
		var scope = this;
		// Загружаем страны
		this._loadCountries(function() {
			if(scope.countries.length>0) {
				// Устанавливаем текущую страну
				scope.setCurrentCountry(scope.countries[0], function() {
					//Загружаем языки
					scope._loadLanguages(function() {
						success();
					});
				});
			}
		});
	}

	// Установить текущую страну
	this.setCurrentCountry = function(country, success) {
		this.currentCountry = country;
		this._loadCities(country, function() {
			success();
		});
	}

	// Получить страну по имени
	this.getCountryByName = function(name) {
		for(var i=0; i<this.countries.length; i++) {
			if(this.countries[i].name == name) return this.countries[i];
		}
		return null;
	}

	// Получить город по имени
	this.getCityByName = function(name) {
		for(var i=0; i<this.cities.length; i++) {
			if(this.cities[i].name == name) return this.cities[i];
		}
		return null;
	}

	/*
		PRIVATE
	*/

	// Загрузка справочника стран
	this._loadCountries = function(success) {
		var scope = this;
		ICurl.loadDict("countries", {}, function(dict) {
			scope.countries = dict;
			success();
		});
	}

	// Загрузка справочника городов
	this._loadCities = function(country, success) {
		var scope = this;
		ICurl.loadDict("cities", {country : country.name}, function(dict) {
			scope.cities = dict;
			success();
		});
	}

	// Загрузка справочника языков
	this._loadLanguages = function(success) {
		var scope = this;
		ICurl.loadDict("languages", {}, function(dict) {
			scope.languages = dict;
			success();
		});
	}
} 