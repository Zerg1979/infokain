/*
	Shops Book
	By Zerg 2014
*/

var ShopsBook = function() {

	// Идентификатор магазина
	this.parrentId = null;

	// Категории товаров
	this.categories = null;

	// Виды товаров текущей категории
	this.kinds = null;

	// Текущая категория
	this.currentCategory = null;

	// Инициализация
	this.init = function(success) {
		var scope = this;
		// Загружаем категории
		this._loadCategories(function() {
			if(scope.categories.length>0) {
				// Устанавливаем текущую категорию
				scope.setCurrentCategory(scope.categories[0], function() {
					success();
				});
			}
		});
	}

	// Установить текущую категорию
	this.setCurrentCategory = function(category, success) {
		this.currentCategory = category;
		this._loadKinds(category, function() {
			success();
		});
	}

	// Получить категорию по имени
	this.getCategoryByName = function(name) {
		for(var i=0; i<this.categories.length; i++) {
			if(this.categories[i].name == name) return this.categories[i];
		}

		return null;
	}

	// Получить вид товара по имени
	this.getKindByName = function(name) {
		for(var i=0; i<this.kinds.length; i++) {
			if(this.kinds[i].name == name) {
				var kinds = this.kinds[i];
				return kinds;
			}
		}
		return null;
	}

	// Получить модели товаров по виду
	this.getMarks = function(kind, success) {
		this._loadMarks(kind, function(marks) {
			success(marks);
		});
	}

	/*
		PRIVATE
	*/

	// Загрузка справочника категорий товаров
	this._loadCategories = function(success) {
		var scope = this;
		ICurl.loadDict("goods_category", {}, function(dict) {
			scope.categories = dict;
			success();
		});
	}

	// Загрузка справочника видов товаров
	this._loadKinds = function(category, success) {
		var scope = this;
		ICurl.loadDict("kinds", {category : category.name}, function(dict) {
			scope.kinds = dict;
			success();
		});
	}

	// Загрузка справочника моделей товара по виду
	this._loadMarks = function(kind, success) {
		var scope = this;
		ICurl.loadDict("marks", {kind : kind.name}, function(marks) {
			success(marks);
		});
	}
}