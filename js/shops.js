/*
	Class Shops
	By Zerg 2014
*/

var InfokainShops = function() {

	// Выбранные виды товаров
	this.selectedKinds = [];

	// Инициализация
	this.init = function(success) {
		this.showCategories();
		this.showKinds();
		this.showCountries();
		this.showCities();
		success();
	}

	// RENDER

	// Отобразить список стран
	this.showCountries = function() {
		var scope = this;
		$('#select_dict_country option').remove();
		var html = "";
		var countries = infApp.addressBook.countries;

		for(var i=0; i<countries.length; i++)
			html += "<option value='" + countries[i].name + "'>" + countries[i].value + "</option>";

		$("#select_dict_country").html(html);
		$("#select_dict_country").on('change', function() {
			scope.onCountrySelect($(this).val());
		});
		$("#select_dict_country").attr('disabled', false).trigger("liszt:updated");
		$('#select_dict_country').selectpicker('refresh');
	}

	// Отобразить список городов
	this.showCities = function(value) {
		var scope = this;
		$('#select_dict_cities option').remove();
		var html = "";
		for(var i=0; i<infApp.addressBook.cities.length; i++)
			html += "<option value='" + infApp.addressBook.cities[i].name + "'>" + infApp.addressBook.cities[i].value + "</option>";
		$("#select_dict_cities").html(html);
		$("#select_dict_cities").on('change', function() {
			scope.onCitySelect($(this).val());
		});
		infApp.formUtils.setSelectDisabled("#select_dict_cities", false);
	}

	// Выбор страны
	this.onCountrySelect = function(country_name) {
		var scope = this;
		infApp.formUtils.setSelectDisabled("#select_dict_cities", true);
		var country = infApp.addressBook.getCountryByName(country_name);
		infApp.addressBook.setCurrentCountry(country, function() {
			scope.showCities();
			infApp.formUtils.setSelectDisabled("#select_dict_cities", false);
		});
	}

	// Выбоор города
	this.onCitySelect = function(city_name) {
		debugger;
	}

	// Отобразить категории
	this.showCategories = function() {
		var scope = this;
		var html = "";
		for(var i=0; i<infApp.shopsBook.categories.length; i++)
			html += "<option value='" + infApp.shopsBook.categories[i].name + "'>" + infApp.shopsBook.categories[i].value + "</option>";
		$("#group_select_id").html(html);
		$("#group_select_id").on('change', function() {
			scope.onCategorySelect($(this).val());
		});
		$('#group_select_id').selectpicker('refresh');
	}

	// Отобразить виды товаров
	this.showKinds = function() {
		var scope = this;
		$('.z-kind-tr').remove();
		var tbl = $("#table-place table tbody");
		for(var i=0; i<infApp.shopsBook.kinds.length; i++) {
			var html = '<tr class="z-kind-tr point" name="'+infApp.shopsBook.kinds[i].name+'">'+
									'<td>'+infApp.shopsBook.currentCategory.value+'</td>'+
									'<td>'+infApp.shopsBook.kinds[i].value+'</td>'+
									'<td><input onclick="event.cancelBubble = true;" type="checkbox" value="select"></td>'+
								'</tr>';
			tbl.append(html);
		}
		$('.z-kind-tr').on('click', function() {
			scope.onKindSelect($(this));
		});

		$('.z-kind-tr input').on('change', function() {
			scope.onKindSelectChange($(this));
		});		
	}

	// END RENDER

	// Выбор категории
	this.onCategorySelect = function(category_name) {
		var scope = this;
		var category = infApp.shopsBook.getCategoryByName(category_name);
		infApp.progress.start('#progress-id');
		infApp.shopsBook.setCurrentCategory(category, function() {
			scope.showKinds();
			infApp.progress.done();
		});
	}

	// Выбор вида товара
	this.onKindSelect = function(el) {
		var chkEl = $('input', el);
		var kind = infApp.shopsBook.getKindByName(el.attr("name"));
		kind.category = infApp.shopsBook.currentCategory;
		if(chkEl.prop("checked")) {
			chkEl.prop("checked", null);
			this.removeSelectedKind(kind);
		} else {
			chkEl.prop("checked", "checked");
			this.addSelectedKind(kind);
		}
	}

	// Выбор вида товара (чекбокс)
	this.onKindSelectChange = function(el) {
		var kind = infApp.shopsBook.getKindByName(el.parent().parent().attr("name"));
		kind.category = infApp.shopsBook.currentCategory;
		if(el.prop("checked")) {
			this.addSelectedKind(kind);
		} else {
			this.removeSelectedKind(kind);
		}
	}

	// Добавление вида товара в выбранные
	this.addSelectedKind = function(kind) {
		var scope = this;
		infApp.progress.start();
		infApp.shopsBook.getMarks(kind, function(marks) {
			kind.marks = marks;
			scope.selectedKinds.push(kind);
			scope.showSelectedKinds();
			infApp.progress.done();
		});
	}

	// Добавление нового вида товара в выбранные
	this.addNewKind = function(name) {
		var kind = {
			name: name,
			value: name,
			category: infApp.shopsBook.currentCategory,
			marks: []
		};
		this.selectedKinds.push(kind);
		this.showSelectedKinds();
		$('#kind_searh_id').val("");
	}

	// Удаление вида товара из выбранных
	this.removeSelectedKind = function(kind) {
		this.selectedKinds = this.removeInArray(this.selectedKinds, kind.name);
		this.showSelectedKinds();
		$("input", $(".z-kind-tr[name='" + kind.name + "']")).prop("checked", null);
	}

	// Отобразить список выбраных видов товаров
	this.showSelectedKinds = function() {
		var scope = this;
		$("#selected_kinds tbody").empty();
		var html = "";
		for(var i = 0; i<this.selectedKinds.length; i++) {
			var row = this.getSelectedRow({
				category: this.selectedKinds[i].category,
				kind: this.selectedKinds[i]
			});
			html += row;
		}
		$("#selected_kinds tbody").html(html);
		$(".kind_selectpicker").each(function(index, el) {
			var sp = $(el).selectpicker({
				selectedText: 'cat',
			});
			$(el).on('change', function() {
				var kind_name = $(this).attr("name");
				for(var i=0; i<scope.selectedKinds.length; i++) {
					if(scope.selectedKinds[i].name == kind_name) {
						scope.selectedKinds[i].selectedMarks = $(this).val();
					}
				}
			});
		});

		// Восстанавливаем выбранные значения марок
		for(var i = 0; i<this.selectedKinds.length; i++) {
			if(this.selectedKinds[i].selectedMarks)
				$('[name="'+this.selectedKinds[i].name+'"]').selectpicker('val', this.selectedKinds[i].selectedMarks);
		}

	}

	// Ошибка при сохранении
	this.onError = function() {
		alert("Перегрузите страницу !!!");
	}

	// СОХРАНЕНИЕ

	// Отправка данных на сервер
	this.sendData = function() {

		if(!this.checkFormData()) {
			return;
		}

		var scope = this;
		var data = this.prepareFormData();
		$.ajax({
			url: "sendform.php",
			data: data,
			type: "POST",
			success: function(response) {
				try {
					var res = $.parseJSON(response);
					if(infApp.shopsBook.parrentId == null) infApp.shopsBook.parrentId = res.id;
					scope.onSuccessSendData();
				} catch(ex) {
					scope.onError();
				}
			},
			error: function(response) {
				scope.onError();
			}
		});	
	}

	// Получить "шапку" объявления
	this.getHeaderFormData = function() {
		
		var name_native = $("#input_name_native").val();
		var name_eng = $("#input_name_eng").val();
		var email = $("#inputinfo_email").val();
		var phone = $("#cell_phones").val();
		var country = $("#select_dict_country").val();
		var city = $("#select_dict_cities").val();

		var header = {
			razdel : 5,
			name: name_native,
			email: email,
			phone: phone,
			country: country,
			city: city,
			currency: this.currentCurrency,
			action: 1,
			price: 0,
			currency: '',
			attributes : {
			}
		};

		if(infApp.shopsBook.parrentId != null)
			header.attributes.depend_add = infApp.shopsBook.parrentId;

		return header;
	}

	// Проверка формы перед отправкой
	this.checkFormData = function() {
		if($("#inputinfo_email").val() == "") {
			var msg = "Поле Email пустое";
			infApp.formUtils.showInfo(msg, 'info');
			return false;	
		}

		// Проверка марок
		for(var i=0; i<this.selectedKinds.length; i++) {
			if(!this.selectedKinds[i].selectedMarks || this.selectedKinds[i].selectedMarks.length == 0){
				infApp.formUtils.showWindow({
				width: 350,
				title: 'Информация',
				message: 'Если вы не нашли новую марку впишите ее она добавиться как новая',
				buttons: [
					{
						ui: 'info',
						text: 'Добавить марку',
						onclick: function() {
							$('#link-mainshop-id').click();
						}
					}
				]
			});
				return false;	
			}
		}
		return true;
	}

	// Подготовка данных для отправки
	this.prepareFormData = function() {

		var sendData = this.getHeaderFormData();
		var index = 1;
		for(var i=0; i<this.selectedKinds.length; i++) {
		 	for(var m=0; m<this.selectedKinds[i].selectedMarks.length; m++) {
		 		var categoryName = 'category'+index;
		 		var kindName = 'kind'+index;
		 		var markName = 'mark'+index;
				sendData.attributes[categoryName] = unescape(this.selectedKinds[i].category.name);
				sendData.attributes[kindName] = unescape(this.selectedKinds[i].name);
				sendData.attributes[markName] = unescape(this.selectedKinds[i].selectedMarks[m]);
				index++;							
		 	}
		}
		return sendData;
	}

	// Успешно добавлен магазин
	this.onSuccessSendData = function() {
		infApp.formUtils.showWindow({
			width: 350,
			title: 'Информация',
			message: 'Информация успешно добавлена',
			buttons: [
				{
					ui: 'info',
					text: 'Добавить филиал',
					onclick: function() {
						$('#depend-info-id').show();
						$('#link-mainshop-id').click();
					}
				},
				{
					ui: 'default',
					text: 'Завершить',
					onclick: function() {
						document.location.href = "index.php";
					}
				}
			]
		});
	}

	// Private
	this.getSelectedKind = function(name) {
		for(var i=0; i<this.selectedKinds.length; i++) {
			if(this.selectedKinds[i].name == name) {
				var kind = this.selectedKinds[i];
				return kind;
			}
		}
		return null;
	}

	this.removeInArray = function(array, name) {
		var index = -1;
		for(var i=0; i<array.length; i++)
			if(array[i].name == name) index = i;
		if(index >= 0) array.splice(index, 1);
		return array;
	}

	this.getSelectedRow = function(data) {
		var html = "<tr>" +
			"<td>" +
			"		<input class='input-large' id='disabledInput' name='" + data.category.name + "' type='text' placeholder='" + data.category.value + "' disabled>" +
			"	</td>" +
			"	<td>" +
			"		<input class='input-large' id='disabledInput' name='" + data.kind.name + "' type='text' placeholder='" + data.kind.value + "' disabled>" +
			"	</td>" +
			"	<td>" +
			"		<select name='" + data.kind.name +"' class='kind_selectpicker span3' data-style='btn-info' multiple data-live-search='true' data-container='body' data-size='auto'>";
			
			for(var i=0; i<data.kind.marks.length; i++) {
				html += "<option value='" + data.kind.marks[i].name + "'>" + data.kind.marks[i].value + "</option>";
			}

			html += "</select>" +
			"	</td>" +
			"	<td>" +
			"		<center><a class='btn btn-mini btn-danger' type='button' rel='tooltip'  title='убрать выбранное' onclick='infApp.shops.removeSelectedKind({name:\"" + data.kind.name + "\",value:\"" + data.kind.value + "\"});'><i class='icon-remove'></i></a></center>" +
			"	</td>" +
			"</tr>";
		return html;
	}
}
