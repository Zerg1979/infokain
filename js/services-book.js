/*
	Services Book
	By Zerg 2014
*/

var ServicesBook = function() {

	// Категории услуг
	this.categories = null;

	// Услуги текущей категории
	this.services = null;

	// Текущая категория
	this.currentCategory = null;

	// Идентификатор объявления
	this.parrentId = null;

	// Инициализация
	this.init = function(success) {
		var scope = this;
		// Загружаем категории
		this._loadCategories(function() {
			if(scope.categories.length>0) {
				// Устанавливаем текущую категорию
				scope.setCurrentCategory(scope.categories[0], function() {
					success();
				});
			}
		});
	}

	// Установить текущую категорию
	this.setCurrentCategory = function(category, success) {
		this.currentCategory = category;
		this._loadServices(category, function() {
			success();
		});
	}

	// Получить категорию по имени
	this.getCategoryByName = function(name) {
		for(var i=0; i<this.categories.length; i++) {
			if(this.categories[i].name == name) return this.categories[i];
		}

		return null;
	}

	// Получить услугу по имени
	this.getServiceByName = function(name) {
		for(var i=0; i<this.services.length; i++) {
			if(this.services[i].name == name) {
				var service = this.services[i];
				return service;
			}
		}
		return null;
	}

	/*
		PRIVATE
	*/

	// Загрузка справочника категорий услуг
	this._loadCategories = function(success) {
		var scope = this;
		ICurl.loadDict("category", {}, function(dict) {
			scope.categories = dict;
			success();
		});
	}

	// Загрузка справочника услуг
	this._loadServices = function(category, success) {
		var scope = this;
		ICurl.loadDict("service", {category : category.name}, function(dict) {
			scope.services = dict;
			success();
		});
	}
}