/*
	Class Services
	By Zerg 2014
*/

var InfokainServices = function() {

	// Выбранные услуги
	this.selectedServices = [];

	// Текущая валюта
	this.currentCurrency = null;

	// Инициализация
	this.init = function(success) {
		this.currentCurrency	 = $("#currency").val();
		this.showCategories();
		this.showServices();
		this.showCountries();
		this.showCities();
		this.showLanguages();
		success();
	}

	// RENDER

	// Отобразить список стран
	this.showCountries = function() {
		var scope = this;
		$('#select_dict_country option').remove();
		var html = "";
		var countries = infApp.addressBook.countries;

		for(var i=0; i<countries.length; i++)
			html += "<option value='" + countries[i].name + "'>" + countries[i].value + "</option>";

		$("#select_dict_country").html(html);
		$("#select_dict_country").on('change', function() {
			scope.onCountrySelect($(this).val());
		});
		$("#select_dict_country").attr('disabled', false).trigger("liszt:updated");
		$('#select_dict_country').selectpicker('refresh');
	}

	// Отобразить список городов
	this.showCities = function(value) {
		var scope = this;
		$('#select_dict_cities option').remove();
		var html = "";
		for(var i=0; i<infApp.addressBook.cities.length; i++)
			html += "<option value='" + infApp.addressBook.cities[i].name + "'>" + infApp.addressBook.cities[i].value + "</option>";
		$("#select_dict_cities").html(html);
		$("#select_dict_cities").on('change', function() {
			scope.onCitySelect($(this).val());
		});
		infApp.formUtils.setSelectDisabled("#select_dict_cities", false);
	}

	// Отобразить категории
	this.showCategories = function() {
		var scope = this;
		var html = "";
		for(var i=0; i<infApp.servicesBook.categories.length; i++)
			html += "<option value='" + infApp.servicesBook.categories[i].name + "'>" + infApp.servicesBook.categories[i].value + "</option>";
		$("#group_select_id").html(html);
		$("#group_select_id").on('change', function() {
			scope.onCategorySelect($(this).val());
		});
		$('#group_select_id').selectpicker('refresh');
	}

	// Отобразить услуги
	this.showServices = function() {
		var scope = this;
		$('.z-service-tr').remove();
		var tbl = $("#table-place table tbody");
		for(var i=0; i<infApp.servicesBook.services.length; i++) {
			var html = '<tr class="z-service-tr point" name="'+infApp.servicesBook.services[i].name+'">'+
									'<td>'+infApp.servicesBook.currentCategory.value+'</td>'+
									'<td>'+infApp.servicesBook.services[i].value+'</td>'+
									'<td><input onclick="event.cancelBubble = true;" type="checkbox" value="select"></td>'+
								'</tr>';
			tbl.append(html);
		}
		$('.z-service-tr').on('click', function() {
			scope.onServiceSelect($(this));
		});

		$('.z-service-tr input').on('change', function() {
			scope.onServiceSelectChange($(this));
		});		
	}

	// Отобразить языки
	this.showLanguages = function() {
		var scope = this;
		$('#select_dict_language option').remove();
		var html = "";
		for(var i=0; i<infApp.addressBook.languages.length; i++)
			html += "<option value='" + infApp.addressBook.languages[i].name + "'>" + infApp.addressBook.languages[i].value + "</option>";
		$("#select_dict_language").html(html);
		infApp.formUtils.setSelectDisabled("#select_dict_language", false);
	}

	// END RENDER

	// Выбор страны
	this.onCountrySelect = function(country_name) {
		var scope = this;
		infApp.formUtils.setSelectDisabled("#select_dict_cities", true);
		var country = infApp.addressBook.getCountryByName(country_name);
		infApp.addressBook.setCurrentCountry(country, function() {
			scope.showCities();
			infApp.formUtils.setSelectDisabled("#select_dict_cities", false);
		});
	}

	// Выбоор города
	this.onCitySelect = function(city_name) {
		debugger;
	}

	// Выбор категории
	this.onCategorySelect = function(category_name) {
		var scope = this;
		var category = infApp.servicesBook.getCategoryByName(category_name);
		infApp.progress.start('#progress-id');
		infApp.servicesBook.setCurrentCategory(category, function() {
			scope.showServices();
			infApp.progress.done();
		});
	}

	// Выбор услуги
	this.onServiceSelect = function(el) {
		var chkEl = $('input', el);
		var service = infApp.servicesBook.getServiceByName(el.attr("name"));
		service.category = infApp.servicesBook.currentCategory;
		if(chkEl.prop("checked")) {
			chkEl.prop("checked", null);
			this.removeSelectedService(service);
		} else {
			chkEl.prop("checked", "checked");
			this.addSelectedService(service);
		}
	}

	// Выбор услуги (чекбокс)
	this.onServiceSelectChange = function(el) {
		var service = infApp.servicesBook.getServiceByName(el.parent().parent().attr("name"));
		service.category = infApp.servicesBook.currentCategory;
		if(el.prop("checked")) {
			this.addSelectedService(service);
		} else {
			this.removeSelectedService(service);
		}
	}

	// Добавление услуги в выбранные
	this.addSelectedService = function(service) {
		this.selectedServices.push(service);
		this.showSelectedServices();
	}

	// Добавление новой услуги в выбранные
	this.addNewService = function(name) {
		var service = {
			name: name,
			value: name,
			category: infApp.servicesBook.currentCategory,
		};
		this.selectedServices.push(service);
		this.showSelectedServices();
		$('#search_service_id').val("");
	}

	// Удаление услуги из выбранных
	this.removeSelectedService = function(service) {
		this.selectedServices = this.removeInArray(this.selectedServices, service.name);
		this.showSelectedServices();
		$("input", $(".z-service-tr[name='" + service.name + "']")).prop("checked", null);
	}

	// Отобразить список выбраных
	this.showSelectedServices = function() {
		$("#selected_services tbody").empty();
		var html = "";
		for(var i = 0; i<this.selectedServices.length; i++) {
			var row = this.getSelectedRow({
				category: this.selectedServices[i].category,
				service: this.selectedServices[i]
			});
			html += row;
		}
		$("#selected_services tbody").html(html);
		this.bindPopover();
	}	

	// Создать popover для фотографий
	this.bindPopover = function() {
		var scope = this;
		var popover = $("[data-bind='popover']").popover({
		   trigger: 'click',
		   html: true,

		   template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 style="float: left;" class="popover-title"></h3><div style="cursor: pointer;float: right;margin: 10px;" class="icon-remove-sign" onclick="$(this).parent().parent().parent().find(\'a\').click();"></div><div class="popover-content"><p></p></div></div></div>',

		   content: function(){
		   		var serviceName = $(this).data('content');
		   		var selectedService = scope.getSelectedService(serviceName);
		   		if(selectedService && selectedService.photo && selectedService.photo!="") {
		   			var form = scope.getPhotoForm(selectedService);
		   		} else {
		   			var form = scope.getPhotoUploadForm(serviceName);
		   		}
		      return form;
		   }
		});
	}

	// Добавление фотографии к услуге
	this.onOpenPhoto = function(serviceName) {
		window.setTimeout(function() {
			setupUploader(serviceName);
		}, 500);
	}

	// Загрузили фото услуги
	this.onPhotoUploadEnd = function(serviceName, url) {
		this.setPhotoUrlToSelectedService(serviceName, url);
		this.showSelectedServices();
	}

	// Удалить фото услуги
	this.onDeletePhoto = function(name) {
		for(var i=0; i<this.selectedServices.length; i++) {
			if(this.selectedServices[i].name == name) {
				this.selectedServices[i].photo = "";
			}
		}
		this.showSelectedServices();
	}

	// Основной обработчик ошибок
	this.onError = function() {
		alert("Перегрузите страницу !!!");
	}

	// Смена валюты
	this.changeCurrency = function(currency) {
		this.currentCurrency = currency;
		$(".currency_value").html(this.currentCurrency);
	}

	// Добавление цены
	this.setPrice = function(service, price) {
		for(var i=0; i<this.selectedServices.length; i++) {
			if(this.selectedServices[i].name == service) {
				this.selectedServices[i].price = price;
			}
		}
	}

	// СОХРАНЕНИЕ

	// Отправка данных на сервер
	this.sendData = function() {

		if(!this.checkFormData()) {
			alert("Заполните обязательные поля формы !!!");
			return;
		}

		var scope = this;
		var data = this.prepareFormData();
		$.ajax({
			url: "sendform.php",
			data: data,
			type: "POST",
			success: function(response) {
				try {
					var res = $.parseJSON(response);
					if(infApp.servicesBook.parrentId == null) infApp.servicesBook.parrentId = res.id;
					scope.onSuccessSendData();
					// alert("Объявление добавлено. ID:"+res.id);
					// window.location.href = "addnew.php";
				} catch(ex) {
					scope.onError();
				}
			},
			error: function(response) {
				scope.onError();
			}
		});	
	}

	// Получить "шапку" объявления
	this.getHeaderFormData = function() {
		
		var name_native = $("#input_name_native").val();
		var name_eng = $("#input_name_eng").val();
		var email = $("#inputinfo_email").val();
		var phone = $("#cell_phones").val();
		var country = $("#select_dict_country").val();
		var city = $("#select_dict_cities").val();

		var header = {
			razdel : 3,
			price: 0,
			name: name_native,
			email: email,
			phone: phone,
			country: country,
			city: city,
			currency: this.currentCurrency,
			action: 1,
			attributes : {
			}
		};

		if(infApp.servicesBook.parrentId != null)
			header.attributes.depend_add = infApp.servicesBook.parrentId;

		return header;
	}

	// Проверка формы перед отправкой
	this.checkFormData = function() {
		if($("#inputinfo_email").val() == "") return false;
		return true;
	}

	// Подготовка данных для отправки
	this.prepareFormData = function() {

		var sendData = this.getHeaderFormData();
		for(var i=0; i<this.selectedServices.length; i++) {
			var categoryName = 'category'+(i+1);
			var serviceName = 'service'+(i+1);
			var priceName = 'price'+(i+1);
			var photoName = 'photo'+(i+1);
			if(this.selectedServices[i].category.name != "") sendData.attributes[categoryName] = unescape(this.selectedServices[i].category.name);
			if( this.selectedServices[i].name != "") sendData.attributes[serviceName] = unescape(this.selectedServices[i].name);
			sendData.attributes[priceName] = !isNaN(Number(this.selectedServices[i].price)) ? 
				Number(this.selectedServices[i].price) : 0;
			if(this.selectedServices[i].photo && this.selectedServices[i].photo != "") sendData.attributes[photoName] = this.selectedServices[i].photo;
		}

		return sendData;
	}

	// Успешно добавлено объявление
	this.onSuccessSendData = function() {
		infApp.formUtils.showWindow({
			width: 350,
			title: 'Информация',
			message: 'Объявление успешно добавлено',
			buttons: [
				{
					ui: 'success',
					text: 'Добавить филиал',
					onclick: function() {
						$('#depend-info-id').show();
						$('#link-mainservice-id').click();
					}
				},
				{
					ui: 'default',
					text: 'Завершить',
					onclick: function() {
						document.location.href = "index.php";
					}
				}
			]
		});
	}

	// Private

	this.getSelectedService = function(name) {
		for(var i=0; i<this.selectedServices.length; i++) {
			if(this.selectedServices[i].name == name) {
				var service = this.selectedServices[i];
				return service;
			}
		}
		return null;
	}

	this.removeInArray = function(array, name) {
		var index = -1;
		for(var i=0; i<array.length; i++)
			if(array[i].name == name) index = i;
		if(index >= 0) array.splice(index, 1);
		return array;
	}

	this.getSelectedRow = function(data) {
		if(!data.service.price) data.service.price = '';
		var html = "<tr>" +
			"<td>" +
			"		<input class='input-large' id='disabledInput' name='" + data.category.name + "' type='text' placeholder='" + data.category.value + "' disabled>" +
			"	</td>" +
			"	<td>" +
			"		<input class='input-large' id='disabledInput' name='" + data.service.name + "' type='text' placeholder='" + data.service.value + "' disabled>" +
			"	</td>" +
			"	<td>" +
			"		<div class='control-group success' class='span2'>" +
			"		   <div class='controls'>" +
			"			<input type='text' id='inputSuccess' onkeyup='infApp.formUtils.numberInputValidator(this);' onchange='infApp.services.setPrice(\""+data.service.name+"\", $(this).val());' class='span2' placeholder='Цена' rel='tooltip' value='"+data.service.price+"'  title='минимальная цена за данную услугу'>" +
			"		  </div>" +
			"	</td>" +
			"	<td>" +
			"		<span class='input-small uneditable-input currency_value' rel='tooltip'  title='Вы указываете цену в этой валюте'>" + this.currentCurrency + "</span>" +
			"	</td>" +
			"	<td>" +
			"		<center><a href='#' data-bind='popover' name='"+data.service.name+"' onmouseup='infApp.services.onOpenPhoto(\""+data.service.name+"\");' class='btn btn-mini btn-success' type='button' data-content='" + data.service.name + "' data-original-title='Изображение''><i class='icon-picture'></i></a></center>" +
			"	</td>" +
			"	<td>" +
			"		<center><a class='btn btn-mini btn-danger' type='button' rel='tooltip'  title='убрать выбранное' onclick='infApp.services.removeSelectedService({name:\"" + data.service.name + "\",value:\"" + data.service.value + "\"});'><i class='icon-remove'></i></a></center>" +
			"	</td>" +
			"</tr>";
		return html;
	}

	this.getPhotoUploadForm = function(serviceName) {

		var html = '<div class="FOTO" name = "'+serviceName+'"><div class="span3">'+
          '<div name="userpic" class="upload">'+
              '<div id="preview" class="upload__preview"></div>'+
              '<div class="upload__link">'+
                      '<input class="upload-link__btn btn btn-small" type="button" value="Обзор"><br/>'+
                      '<input class="upload-link__inp" style="width:90px;" name="photo1" style="cursor: pointer" type="file" accept=".jpg,.jpeg,.gif" />'+
                      '<input name="attributes[photo1]" type="hidden" />'+
                      '<input class="upload-link__btn btn btn-small" type="button" style="cursor: pointer;" value="Загрузить фото">'+ 
                     ' <div class="progress progress-info progress-striped">'+
                          '<div class="bar uploud-progress" style="width: 0%"></div>'+
                      '</div>'+
              '</div>'+
          '</div>'+
				'</div></div>';
		return html;
	}

	this.getPhotoForm = function(service) {
		var html = '<div><img style="width:197px;" src="'+service.photo+'"/><input style="position: absolute;left: 60px;bottom: 15px;" class="upload-link__btn btn btn-small" type="button" value="Удалить" onclick="infApp.services.onDeletePhoto(\''+service.name+'\');"></div>';

		return html;
	}

	this.setPhotoUrlToSelectedService = function(serviceName, url) {
		for(var i=0; i<this.selectedServices.length; i++) {
			if(this.selectedServices[i].name == serviceName) {
				this.selectedServices[i].photo = url;
			}
		}
	}
}