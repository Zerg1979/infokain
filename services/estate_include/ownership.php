<label><?php echo $l['ownership']; ?></label>
	<select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" name="attributes[ownership]">
		<option></option>
		<option value="bought"><?php echo $l['bought']; ?></option>
		<option value="heritage"><?php echo $l['heritage']; ?></option>
		<option value="donation"><?php echo $l['donation']; ?></option>
	</select>
