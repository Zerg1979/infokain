<label><?php echo $l['total_area']; ?></label>
	  <input disabled class="span1" name="attributes[total_area]" size="16" type="number">
		<select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" name="attributes[total_area_units]">
			<option></option>
			<option data-subtext="<?php echo $l['square_meter']; ?>"><p><?php echo $l['m']; ?><sub>2</sub></p></option>
			<option data-subtext="<?php echo $l['square_yard']; ?>"><p><?php echo $l['yd']; ?><sup>2</sup></p></option>
			<option data-subtext="<?php echo $l['square_foot']; ?>"><p><?php echo $l['ft']; ?><sup>2</sup></p></option>
		</select>
