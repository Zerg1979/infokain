<?php include('../includes/langdetect.php'); ?>
<!DOCTYPE html> 
<html> 
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>INFOKAIN.COM - Информация под рукой</title>

    <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen" />   
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/bootstrap-select.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/nprogress.css" media="all" />


</head>
<body>

<?php include('../includes/info.php'); ?>

<center><h2 ><p  class="text-success"><?php echo $l['services_add']; ?></p></h2></center>
<p><p><div class="part_border_1 ser_color"></div></p></p>
<?php include('buttons2.php'); ?>
<p><p><div class="part_border_1 ser_color"></div></p></p>

</center>
<!-- ZDES NACHINAYETSYA BOLSHAYA FORMA -->
<form class="sendFormEstate">
<input type='hidden' name='razdel' value='3'>
<div class="forms MainServise">
	<div class="container">
    <!-- Таблица для выбора услуг -->
		<div id="depend-info-id" class="alert alert-success" style="display: none;">
				<center><strong>Если в филлиале услуги отличаются произведите изменения</strong></center>
		</div>
		<div class="alert alert-success">
				<center><strong><?php echo $l['after_select']; ?> <a class="slider checkElements enabledElements" next="servicecontacts"><?php echo $l['contact_info']; ?></a></strong></center>
		</div>
	</div>
		<p><p><div class="part_border_1 ser_color"></div></p></p>
	<div class="container">
		<div class="alert alert-info">
				<center><strong>Выбранные услуги</strong></center>
		</div>
		<table class="table table-hover" id="selected_services">
			<thead>
				<tr>
					<th>
						<div class="alert alert-success">
							<strong><?php echo $l['category']; ?></strong>
						</div>
					</th>
					<th>
						<div class="alert alert-success">
							<strong><?php echo $l['servise']; ?></strong>
						</div>
					</th>
					<th>
						<div class="alert alert-success">
							<strong><?php echo $l['price']; ?></strong>
						</div>
					</th>
					<th style="vertical-align: top;">
						<select id="currency" class="selectpicker span2" data-style="btn-success" data-container="body" data-width="100px" onchange="infApp.services.changeCurrency($(this).val());">
							<?php include('../includes/All_add_new_include/curency.php'); ?>
						</select>
					</th>
					<th>
						<div class="alert alert-success">
							<strong><i class="icon-briefcase" rel="tooltip"  title="загрузите изображения услуг" placement="bottom"></i></strong>
						</div>
					</th>
					<th>
						<div class="alert alert-success">
							<strong><i class="icon-ok"></i></strong>
						</div>
					</th>
				</tr>	
			</thead>
			<tbody>
					
			</tbody>
		</table>
	</div>
		<p><p><div class="part_border_1 ser_color"></div></p></p>
	<div class="container">
		<div class="alert alert-info">
				<center><strong>Отметьте галочкой предоставляемые услуги</strong></center>
		</div>

		<!-- Таблица услуг -->
		<div id="table-place">
			<table id="services_list_table" class="table table-hover">
			<thead>
				<tr>
					<th>
						Категория
					</th>
					<th>
						Услуга
					</th>
					<th>
						Выбор
					</th>
				</tr>	
			</thead>
			<tbody>
				<tr id="services_list_head_id">
					<td>
					<select id="group_select_id"  class="selectpicker span4" data-style="btn-success" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
					</select>
					</td>
					<td>
					<input type="text" id="search_service_id" class="span6" placeholder="Начните вводить для поиска услуг">
					</td>
					<td>
						<center><a class="btn btn-mini btn-success" onclick="infApp.services.addNewService($('#search_service_id').val());" type="button" rel="tooltip"  title="Если Вы не нашли нужную услугу, введите в поле услуга полное название вашей услуги и нажмите эту кнопку, она добавиться как новая в выбранной категории. Убедитесь в правильности выбора категории"><i class="icon-plus"></i></a></center>
					</td>
				</tr>
				<tr><td colspan=3 style="border: none; background-color: transparent;"><div id="progress-id" style="overflow:hidden;margin-top: -18px; height: 2px;">&nbsp</div></td></tr>	
				
			</tbody>
		</table>

		</div>
		

	</div>
	</div>
</div>
    <!-- Таблица для выбора услуг -->

    <!-- Информация об организации -->
<div class="forms servicecontacts" style="display: none">
	<div class="container">
	<div class="alert alert-success">
		<strong><?php echo $l['organisation_type']; ?></strong>
	</div>
	<div class="row">
		<div class="span2">
			<label><h6><p class="text-success" ><?php echo $l['ownership_form']; ?></p></h6></label>
				<select class="selectpicker span2" data-style="btn-success" data-container="body" name="attributes[ownership_form]">
					<option  value="state"><?php echo $l['state']; ?></option>
					<option  value="private_enterprise"><?php echo $l['private_enterprise']; ?></option>
					<option  value="individual_face"><?php echo $l['individual_face']; ?></option>
				</select>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputsuccess"><h6><?php echo $l['the_number_of']; ?> <i class="icon-exclamation-sign" rel="tooltip"  title="<?php echo $l['how_many']; ?>"></i></h6></label>
					<div class="controls">
						<input type="text" class="span2" id="inputsuccess" placeholder="5"></input>
					</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label><h6><p class="text-success"><?php echo $l['mode_of_operation']; ?> <i class="icon-exclamation-sign" rel="tooltip"  title="<?php echo $l['around_the_clock']; ?>"></i></p></h6></label>
					<div class="btn-group">
						<select class="selectpicker" data-style="btn-success" data-width="67px" data-container="body" name="attributes[mode_of_operation]">
							<option>0:00</option>
							<option>0:30</option>
							<option>1:00</option>
							<option>1:30</option>
							<option>2:00</option>
							<option>2:30</option>
							<option>3:00</option>
							<option>3:30</option>
							<option>4:00</option>
							<option>4:00</option>
							<option>4:30</option>
							<option>4:30</option>
							<option>5:00</option>
							<option>5:30</option>
							<option>6:00</option>
							<option>6:30</option>
							<option>7:00</option>
							<option>7:30</option>
							<option>8:00</option>
							<option>8:30</option>
							<option>9:00</option>
							<option>9:30</option>
							<option>10:00</option>
							<option>10:30</option>
							<option>11:00</option>
							<option>11:30</option>
							<option>12:00</option>
							<option>12:30</option>
							<option>13:00</option>
							<option>13:30</option>
							<option>14:00</option>
							<option>14:30</option>
							<option>15:00</option>
							<option>15:30</option>
							<option>16:00</option>
							<option>16:30</option>
							<option>17:00</option>
							<option>17:30</option>
							<option>18:00</option>
							<option>18:30</option>
							<option>19:00</option>
							<option>19:30</option>
							<option>20:00</option>
							<option>20:30</option>
							<option>21:00</option>
							<option>21:30</option>
							<option>22:00</option>
							<option>22:30</option>
							<option>23:00</option>
							<option>23:30</option>
							<option>24:00</option>
								</select>
						<select class="selectpicker" data-style="btn-success" data-width="75px" data-container="body" name="attributes[mode_of_operation]">
							<option>24:00</option>
							<option>23:30</option>
							<option>23:00</option>
							<option>22:30</option>
							<option>22:00</option>
							<option>21:30</option>
							<option>21:00</option>
							<option>20:30</option>
							<option>20:00</option>
							<option>19:30</option>
							<option>19:00</option>
							<option>18:30</option>
							<option>18:00</option>
							<option>17:30</option>
							<option>17:00</option>
							<option>16:30</option>
							<option>16:00</option>
							<option>15:30</option>
							<option>15:00</option>
							<option>14:30</option>
							<option>14:00</option>
							<option>13:30</option>
							<option>13:00</option>
							<option>12:30</option>
							<option>12:00</option>
							<option>11:30</option>
							<option>11:00</option>
							<option>10:30</option>
							<option>10:00</option>
							<option>9:30</option>
							<option>9:00</option>
							<option>8:30</option>
							<option>8:00</option>
							<option>7:30</option>
							<option>7:00</option>
							<option>6:30</option>
							<option>6:00</option>
							<option>5:30</option>
							<option>5:00</option>
							<option>4:30</option>
							<option>4:30</option>
							<option>4:00</option>
							<option>4:00</option>
							<option>3:30</option>
							<option>3:00</option>
							<option>2:30</option>
							<option>2:00</option>
							<option>1:30</option>
							<option>1:00</option>
							<option>0:30</option>
							<option>0:00</option>
						</select>
					</div>
			</div>
		</div>
		<div class="span2">
		<label><h6><p class="text-success"><?php echo $l['days_off']; ?></p></h6></label>
			<select class="selectpicker span2" multiple data-style="btn-success">
				<option  value="no"><?php echo $l['n_o']; ?></option>
				<option  value="monday"><?php echo $l['monday']; ?></option>
				<option  value="tuesday"><?php echo $l['tuesday']; ?></option>
				<option  value="wednesday"><?php echo $l['wednesday']; ?></option>
				<option  value="thursday"><?php echo $l['thursday']; ?></option>
				<option  value="friday"><?php echo $l['friday']; ?></option>
				<option  value="saturday"><?php echo $l['saturday']; ?></option>
				<option  value="sunday"><?php echo $l['sunday']; ?></option>
			</select>
		</div>
		<div class="span2">
		<label><h6><p class="text-success"><?php echo $l['payment_methods']; ?></p></h6></label>
			<select class="selectpicker span2" multiple id="attributes_payment_methods" name="attributes[payment_methods][]" data-style="btn-success">
<!--				<optgroup label="<?php/* echo $l['cashless_payments']; */?>">-->
					<option  value="american_express"><?php echo $l['american_express']; ?></option>
					<option  value="cirrus"><?php echo $l['cirrus']; ?></option>
					<option  value="diners_club"><?php echo $l['diners_club']; ?></option>
					<option  value="eurocard"><?php echo $l['eurocard']; ?></option>
					<option  value="jcb"><?php echo $l['jcb']; ?></option>
					<option  value="maestro"><?php echo $l['maestro']; ?></option>
					<option  value="mastercard"><?php echo $l['mastercard']; ?></option>
					<option  value="union"><?php echo $l['union']; ?></option>
					<option  value="visa"><?php echo $l['visa']; ?></option>
					<option  value="visa_electron"><?php echo $l['visa_electron']; ?></option>
<!--				</optgroup>
				<optgroup label="<?php/* echo $l['cash']; */?>">-->
						<option value="usd" data-subtext="<?php echo $l['usd']; ?>">usd</option>
						<option value="eur" data-subtext="<?php echo $l['eur']; ?>">eur</option>
						<option value="cny" data-subtext="<?php echo $l['cny']; ?>">cny</option>
						<option value="jpy" data-subtext="<?php echo $l['jpy']; ?>">jpy</option>
						<option value="inr" data-subtext="<?php echo $l['inr']; ?>">inr</option>
						<option value="brl" data-subtext="<?php echo $l['brl']; ?>">brl</option>
						<option value="gbp" data-subtext="<?php echo $l['gbp']; ?>">gbp</option>
						<option value="rub" data-subtext="<?php echo $l['rub']; ?>">rub</option>
						<option value="aed" data-subtext="<?php echo $l['aed']; ?>">aed</option>
						<option value="ars" data-subtext="<?php echo $l['ars']; ?>">ars</option>
						<option value="aud" data-subtext="<?php echo $l['aud']; ?>">aud</option>
						<option value="bgn" data-subtext="<?php echo $l['bgn']; ?>">bgn</option>
						<option value="bhd" data-subtext="<?php echo $l['bhd']; ?>">bhd</option>
						<option value="cad" data-subtext="<?php echo $l['cad']; ?>">cad</option>
						<option value="chf" data-subtext="<?php echo $l['chf']; ?>">chf</option>
						<option value="clp" data-subtext="<?php echo $l['clp']; ?>">clp</option>
						<option value="cop" data-subtext="<?php echo $l['cop']; ?>">cop</option>
						<option value="crc" data-subtext="<?php echo $l['crc']; ?>">crc</option>
						<option value="czk" data-subtext="<?php echo $l['czk']; ?>">czk</option>
						<option value="dop" data-subtext="<?php echo $l['dop']; ?>">dop</option>
						<option value="dzd" data-subtext="<?php echo $l['dzd']; ?>">dzd</option>
						<option value="eek" data-subtext="<?php echo $l['eek']; ?>">eek</option>
						<option value="egp" data-subtext="<?php echo $l['egp']; ?>">egp</option>
						<option value="hkd" data-subtext="<?php echo $l['hkd']; ?>">hkd</option>
						<option value="hrk" data-subtext="<?php echo $l['hrk']; ?>">hrk</option>
						<option value="huf" data-subtext="<?php echo $l['huf']; ?>">huf</option>
						<option value="ils" data-subtext="<?php echo $l['ils']; ?>">ils</option>
						<option value="isk" data-subtext="<?php echo $l['isk']; ?>">isk</option>
						<option value="jod" data-subtext="<?php echo $l['jod']; ?>">jod</option>
						<option value="kes" data-subtext="<?php echo $l['kes']; ?>">kes</option>
						<option value="krw" data-subtext="<?php echo $l['krw']; ?>">krw</option>
						<option value="kwd" data-subtext="<?php echo $l['kwd']; ?>">kwd</option>
						<option value="lbp" data-subtext="<?php echo $l['lbp']; ?>">lbp</option>
						<option value="lkr" data-subtext="<?php echo $l['lkr']; ?>">lkr</option>
						<option value="mad" data-subtext="<?php echo $l['mad']; ?>">mad</option>
						<option value="mur" data-subtext="<?php echo $l['mur']; ?>">mur</option>
						<option value="mxn" data-subtext="<?php echo $l['mxn']; ?>">mxn</option>
						<option value="myr" data-subtext="<?php echo $l['myr']; ?>">myr</option>
						<option value="nok" data-subtext="<?php echo $l['nok']; ?>">nok</option>
						<option value="nzd" data-subtext="<?php echo $l['nzd']; ?>">nzd</option>
						<option value="omr" data-subtext="<?php echo $l['omr']; ?>">omr</option>
						<option value="pen" data-subtext="<?php echo $l['pen']; ?>">pen</option>
						<option value="php" data-subtext="<?php echo $l['php']; ?>">php</option>
						<option value="pkr" data-subtext="<?php echo $l['pkr']; ?>">pkr</option>
						<option value="pln" data-subtext="<?php echo $l['pln']; ?>">pln</option>
						<option value="qar" data-subtext="<?php echo $l['qar']; ?>">qar</option>
						<option value="ron" data-subtext="<?php echo $l['ron']; ?>">ron</option>
						<option value="sar" data-subtext="<?php echo $l['sar']; ?>">sar</option>
						<option value="sek" data-subtext="<?php echo $l['sek']; ?>">sek</option>
						<option value="thb" data-subtext="<?php echo $l['thb']; ?>">thb</option>
						<option value="tmm" data-subtext="<?php echo $l['tmm']; ?>">tmm</option>
						<option value="tnd" data-subtext="<?php echo $l['tnd']; ?>">tnd</option>
						<option value="trl" data-subtext="<?php echo $l['trl']; ?>">trl</option>
						<option value="ttd" data-subtext="<?php echo $l['ttd']; ?>">ttd</option>
						<option value="twd" data-subtext="<?php echo $l['twd']; ?>">twd</option>
						<option value="veb" data-subtext="<?php echo $l['veb']; ?>">veb</option>
						<option value="vnd" data-subtext="<?php echo $l['vnd']; ?>">vnd</option>
						<option value="zar" data-subtext="<?php echo $l['zar']; ?>">zar</option>
<!--				</optgroup>-->
			</select>
		</div>
		<div class="span2">
		<label><h6><p class="text-success"><?php echo $l['language_staff']; ?></p></h6></label>
			<select disabled id="select_dict_language" data-live-search="true" data-container="body" data-size="auto" class="selectpicker span2" multiple id="attributes_more" name="attributes[more][]" data-style="btn-success">
			</select>
		</div>
	</div>
	<div class="alert alert-success">
		<strong><?php echo $l['location']; ?></strong>
	</div>
	<div class="row">
		<div class="span2">
			<div class="control-group success">
				<label><h6><p class="text-success"><?php echo $l['country']; ?>*</p></h6></label>
					<select disabled id="select_dict_country" data-live-search="true" data-container="body" data-size="auto" class="selectpicker span2" data-style="btn-success" name="attributes[country]">
					</select>
				<label><h6><p class="text-success"><?php echo $l['city']; ?>*</p></h6></label>
					<select disabled id="select_dict_cities"  data-live-search="true" class="selectpicker span2" data-container="body" data-size="auto" data-style="btn-success" name="attributes[mode_of_operation]">
					</select>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label><h6><p class="text-success"><?php echo $l['district']; ?>*</p></h6></label>
					<select disabled class="selectpicker span2" data-style="btn-success" name="attributes[mode_of_operation]">
					</select>
				<label><h6><p class="text-success"><?php echo $l['metro']; ?></p></h6></label>
					<select disabled class="selectpicker span2" data-style="btn-success" name="attributes[mode_of_operation]">
					</select>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputsuccess"><h6><?php echo $l['street']; ?>*</h6></label>
					<select disabled class="selectpicker span2" data-style="btn-success" name="attributes[mode_of_operation]">
					</select>
				<label class="control-label" for="inputsuccess"><h6><?php echo $l['house_number']; ?></h6></label>
					<div class="controls">
						<input disabled type="text" class="span2 inform" id="inputsuccess" formname="house_number" name="house_number" placeholder="64/2">
					</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputsuccess"><h6><?php echo $l['apartment_numbe']; ?></h6></label>
				<div class="controls">
					<input disabled type="text" class="span2 inform" id="inputsuccess" formname="apartment_numbe" name="apartment_numbe" placeholder="2/56">
				</div>
				<label class="control-label" for="inputsuccess"><h6><?php echo $l['refine_on_the_m']; ?></h6></label>
				<div class="controls">
					<input disabled type="text" class="span2 maps" name="attributes[maps]" placeholder="<?php echo $l['map']; ?>">


					
				</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6><?php echo $l['cell_phones']; ?>*</h6></label>
				<div class="controls">
					<input disabled type="text" class="span4 inform" id="cell_phones" formname="cell_phones" name="cell_phones" placeholder="+*(***)***-**-**,...">
				</div>
				<label class="control-label" class="span4" for="inputinfo"><h6><?php echo $l['additionally']; ?></h6></label>
				<div class="controls">
					<input disabled type="text" class="span4" id="inputsuccess" formname="additionally" name="additionally" placeholder="<?php echo $l['skype']; ?>">
				</div>
			</div>
		</div>
		<div class="span2">
		</div>
	</div>
	<div class="alert alert-success">
		<strong><?php echo $l['contacts']; ?></strong>
	</div>
	<div class="row">
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6><?php echo $l['name']; ?>*</h6></label>
				<div class="controls">
					<input disabled type="text" class="span2 inform" id="input_name_native" placeholder="<?php echo $l['native_langua']; ?>">
				</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6><?php echo $l['name']; ?>* <i class="icon-exclamation-sign"   rel="tooltip"  title="<?php echo $l['to_the_foreigners']; ?>"></i></h6></label>
				<div class="controls">
					<input disabled type="text" class="span2 inform" id="input_name_eng" placeholder="<?php echo $l['latyn']; ?>">
				</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6><?php echo $l['sayt']; ?></h6></label>
				<div class="controls">
					<input disabled type="text" class="span2 inform" id="inputinfo" placeholder="http\\kafe.com">
				</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6><?php echo $l['email']; ?>*</h6></label>
				<div class="controls">
					<input disabled type="text" class="span2 inform" id="inputinfo_email" placeholder="<?php echo $l['email']; ?>">
				</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6>Период размещение </h6></label>
				<div class="controls">
					<select class="selectpicker span2" data-style="btn-success" name="attributes[country]">
						<option>15 дней</option>
						<option>30 дней</option>
						<option>45 дней</option>
						<option>60 дней</option>
					</select>
				</div>
			</div>
		</div>
		<div class="span2">
			<div class="control-group success">
				<label class="control-label" for="inputinfo"><h6>Отправка объявления </h6></label>
				<div class="controls">
					<button id="sendbuttonp" class="btn btn-success btn-block" type="button" onclick="infApp.services.sendData();" class="accordion-toggle" data-toggle="collapse" data-parent="#accordionservise" href="#collapsetwo"> <?php echo $l['add']; ?></button>
				</div>
			</div>
		</div>
	</div>
		<ul class="pager">
			<li class="previous">
				<a id="link-mainservice-id" class="slider" next="MainServise">&larr; Вернуться к выбору услуг</a>
			</li>
		</ul>
	</div>
</div>
    <!-- Информация об организации -->
</form>

</body>
</html>
    <script src="../js/jquery-1.9.1.js"></script>
	<script src="../js/bootstrap-select.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/sendForm.js"></script>
    <script src="../js/dependCatalog.js"></script>
    <script src="../uploderFiles/uploud.js"></script>
    <script src="../uploderFiles/FileAPI.min.js"></script>
    <script src="../uploderFiles/FileAPI.exif.js"></script>

    <!-- INFOKAIN APP -->
    <script src="../js/extensions.js"></script>
    <script src="../js/icurl.js"></script>
    <script src="../js/address-book.js"></script>
    <script src="../js/services-book.js"></script>
    <script src="../js/services.js"></script>
    <script src="../js/shops-book.js"></script>
    <script src="../js/shops.js"></script>
    <script src="../js/form-utils.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/nprogress.js"></script>

<script>
        $(window).on('load', function () {

            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });

            // $('.selectpicker').selectpicker('hide');
        });

</script>
<!-- Google maps -->
<?php include('../includes/maps.php'); ?>

<!-- Идентификатор раздела НЕ УДАЛЯТЬ !!! -->
<script>var AppRouteInfo = {id: 0, name: 'services'};</script>