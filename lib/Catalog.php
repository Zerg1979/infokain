<?php

class Curl
{

    static public $curl_obj = null;

    /**
     *   Make a http call to another web server.
     *   @param string hostname
     *   @param int  portname
     *   @param string http method
     *   @param string path on remote server
     *   @param mixed array or string of post data
     */

    function makeHttpCall( $host = 'http://infokain2.me', $port, $method, $path, $data )
    {
        if (!Curl::$curl_obj ) Curl::$curl_obj = curl_init();
        $handle = Curl::$curl_obj;

        if ( count( $data ) && $method === 'HTTP_GET' )
        {
            throw new \InvalidArgumentException( "Make http call, get should not have data " . $host . $path . ' : ' . json_encode( $data ) );
        }

        curl_setopt( $handle, CURLOPT_URL, $host . '/' . $path );

        if ( $method !== 'HTTP_GET' )
        {
            curl_setopt( $handle, CURLOPT_POST, true );
    		curl_setopt( $handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json" ) );
        }
        else
        {
            curl_setopt( $handle, CURLOPT_POST, false );
        }

        curl_setopt( $handle, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $handle, CURLOPT_CONNECTTIMEOUT, 2 );
        curl_setopt( $handle, CURLOPT_PORT, $port );

        if ( $data )
        {
            curl_setopt( $handle, CURLOPT_POSTFIELDS, $data );
        }

        $response = curl_exec( $handle );

        curl_close( $handle );
        Curl::$curl_obj = null;

        return $response;
    }
}
?>
