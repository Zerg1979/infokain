  <script src="../js/bootstrap-select.js"></script>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<center><h3 id="myModalLabel"><?php echo $l['Modal_units']; ?></h3></center>
	</div>
	<div class="modal-body" id="units_modal">
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $l['wil_be_good']; ?> 
		</div>
				<div class="row">
					<div class="span3">
						<center><label><?php echo $l['apartment_area']; ?></label></center>
						<select id="units_apart_area" class="selectpicker" data-width="90px" data-style="btn-inverse" data-container="body">
							<option value="m2"><?php echo $l['m2']; ?></option>
							<option value="yd2"><p><?php echo $l['yd2']; ?></p></option>
							<option value="ft2"><p><?php echo $l['ft2']; ?></p></option>
						</select>
					</div>
					<div class="span3">
						<center><label><?php echo $l['area']; ?></label></center>
							<select class="selectpicker" id="units_area" data-width="70px" data-style="btn-inverse" data-container="body" id="attributes_area" name="attributes[area]">
								<option meter='100' value="a"><?php echo $l['weaving']; ?></option>
								<option meter='1' value="ha"><?php echo $l['ha']; ?></option>
								<option meter='0.003861' value="ml 2"><?php echo $l['square_mile']; ?></option>
								<option meter='2.471' value="acre"><?php echo $l['acre']; ?></option>
							</select>
					</div>
					<div class="span3">
						<center><label><?php echo $l['ceiling_height']; ?></label></center>
							<select class="selectpicker" id="units_ceiling_height" data-width="70px" data-style="btn-inverse" data-container="body" name="attributes[ceiling_height_units]">
								<option value="m"><?php echo $l['meter']; ?></option>
								<option value="ya"><?php echo $l['yard']; ?></option>
								<option value="ft"><?php echo $l['foot']; ?></option>
								<option value="syak"><?php echo $l['syaku_japan']; ?></option>
								<option value="souk"><?php echo $l['souk_thailand']; ?></option>
								<option value="chi"><?php echo $l['china_chi']; ?></option>
							</select>
					</div>
					<div class="span2">
						<center><label><?php echo $l['currency']; ?></label></center>
						<select class="selectpicker" id="units_currency" data-width="90px" data-style="btn-inverse" data-size="auto" data-live-search="true"   data-container="body" name="currency">
							<option value="usd" ><?php echo $l['usd']; ?></option>
							<option value="eur" ><?php echo $l['eur']; ?></option>
							<option value="cny" ><?php echo $l['cny']; ?></option>
							<option value="jpy" ><?php echo $l['jpy']; ?></option>
							<option value="inr" ><?php echo $l['inr']; ?></option>
							<option value="brl" ><?php echo $l['brl']; ?></option>
							<option value="gbp" ><?php echo $l['gbp']; ?></option>
							<option value="rub" ><?php echo $l['rub']; ?></option>
							<option value="aed" ><?php echo $l['aed']; ?></option>
							<option value="ars" ><?php echo $l['ars']; ?></option>
							<option value="aud" ><?php echo $l['aud']; ?></option>
							<option value="bgn" ><?php echo $l['bgn']; ?></option>
							<option value="bhd" ><?php echo $l['bhd']; ?></option>
							<option value="cad" ><?php echo $l['cad']; ?></option>
							<option value="chf" ><?php echo $l['chf']; ?></option>
							<option value="clp" ><?php echo $l['clp']; ?></option>
							<option value="cop" ><?php echo $l['cop']; ?></option>
							<option value="crc" ><?php echo $l['crc']; ?></option>
							<option value="czk" ><?php echo $l['czk']; ?></option>
							<option value="dop" ><?php echo $l['dop']; ?></option>
							<option value="dzd" ><?php echo $l['dzd']; ?></option>
							<option value="eek" ><?php echo $l['eek']; ?></option>
							<option value="egp" ><?php echo $l['egp']; ?></option>
							<option value="hkd" ><?php echo $l['hkd']; ?></option>
							<option value="hrk" ><?php echo $l['hrk']; ?></option>
							<option value="huf" ><?php echo $l['huf']; ?></option>
							<option value="ils" ><?php echo $l['ils']; ?></option>
							<option value="isk" ><?php echo $l['isk']; ?></option>
							<option value="jod" ><?php echo $l['jod']; ?></option>
							<option value="kes" ><?php echo $l['kes']; ?></option>
							<option value="krw" ><?php echo $l['krw']; ?></option>
							<option value="kwd" ><?php echo $l['kwd']; ?></option>
							<option value="lbp" ><?php echo $l['lbp']; ?></option>
							<option value="lkr" ><?php echo $l['lkr']; ?></option>
							<option value="mad" ><?php echo $l['mad']; ?></option>
							<option value="mur" ><?php echo $l['mur']; ?></option>
							<option value="mxn" ><?php echo $l['mxn']; ?></option>
							<option value="myr" ><?php echo $l['myr']; ?></option>
							<option value="nok" ><?php echo $l['nok']; ?></option>
							<option value="nzd" ><?php echo $l['nzd']; ?></option>
							<option value="omr" ><?php echo $l['omr']; ?></option>
							<option value="pen" ><?php echo $l['pen']; ?></option>
							<option value="php" ><?php echo $l['php']; ?></option>
							<option value="pkr" ><?php echo $l['pkr']; ?></option>
							<option value="pln" ><?php echo $l['pln']; ?></option>
							<option value="qar" ><?php echo $l['qar']; ?></option>
							<option value="ron" ><?php echo $l['ron']; ?></option>
							<option value="sar" ><?php echo $l['sar']; ?></option>
							<option value="sek" ><?php echo $l['sek']; ?></option>
							<option value="thb" ><?php echo $l['thb']; ?></option>
							<option value="tmm" ><?php echo $l['tmm']; ?></option>
							<option value="tnd" ><?php echo $l['tnd']; ?></option>
							<option value="trl" ><?php echo $l['trl']; ?></option>
							<option value="ttd" ><?php echo $l['ttd']; ?></option>
							<option value="twd" ><?php echo $l['twd']; ?></option>
							<option value="veb" ><?php echo $l['veb']; ?></option>
							<option value="vnd" ><?php echo $l['vnd']; ?></option>
							<option value="zar" ><?php echo $l['zar']; ?></option>
						</select>
					</div>
				</div>

	</div>
	<script type="text/javascript">
		units_ceiling_height = "m";
		units_area = "a";
		units_apart_area = "m2";	
		units_currency = "usd";
		function save_units()
		{
			units_area = $("#units_area").val();
			units_apart_area = $("#units_apart_area").val();
			units_ceiling_height = $("#units_ceiling_height").val();
			units_currency = $("#units_currency").val();
			$.ajax({url:"../user_room/ajax.php?action=save_units", type:"post", data:{"units_area":units_area, "units_ceiling_height":units_ceiling_height, "units_apart_area":units_apart_area, "units_currency":units_currency},success:function(){alert("saved");}});
		}
	</script>
	
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true" id="units_modal_close"><?php echo $l['close']; ?></button>
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="save_units();"><?php echo $l['save']; ?></button>
  </div>
