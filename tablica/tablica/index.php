<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0;maximum-scale=1.0; user-scalable=0;"/>
    <title>INFOKAIN.COM - Информация под рукой</title>

   <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen" />
  
   <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="screen" />
	
	<script src="../js/jquery-1.9.1.js"></script>
   <script src="../js/bootstrap.min.js"></script>
	
	<script type="text/javascript"> var razdel = 1; </script>

</head>
<body>

  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../tablica/css/bootstrap-slider/bootstrap-slider.css" rel="stylesheet">
  <link href="../tablica/css/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">
  <link href="../tablica/css/section.css" rel="stylesheet">
  <link href="../tablica/css/modal.css" rel="stylesheet">
  <link href="../tablica/css/main.css" rel="stylesheet">
  <link rel="stylesheet" href="../tablica/css/slick.css" type="text/css" media="screen" />   
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> 


  <!-- MASK -->
  <div id="mask"></div>

  <!-- <div class=bannersWrap style='height:0'>
    <a class="left carousel-control"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <div class=banners><div><img><div class=info></div><div class=price></div></div></div>
    <a class="right carousel-control"><span class="glyphicon glyphicon-chevron-right"></span></a>
  </div> -->
<div class="slick-container-info">
  <a href="#" onclick="tLayout.showCarousel(); return false;">Открыть и найти что то интересное в баннерах</a>
</div>
<div class="slick-container-info-cnt"></div>
<div class="slick-container">
  <div class="slick-buttons">
    <a href="javascript:void(0);" title="Запустить" onclick="tLayout.carousel.slickPlay();" class="icon-play "></a>
    <a href="javascript:void(0);" title="Пауза" onclick="tLayout.carousel.slickPause();" class="icon-pause"></a>
    <a href="javascript:void(0);" title="Предыдущий" onclick="tLayout.countSlidesShown--;tLayout.carousel.slickPrev();" class="icon-step-backward"></a>
    <a href="javascript:void(0);" title_close="Закрыть" title_tmpl="Можно будет закрыть через {0} секунд" onclick="tLayout.hideCarousel();" class="icon-resize-small disabled"></a> 
  </div>
  <div class="slick">
    <!-- <div class="slide" title="Марка:<br>Модель:" style="background: url(car1.jpg); background-size: 150px; background-repeat: no-repeat;"></div>
    <div class="slide" title="Марка:<br>Модель:" style="background: url(car2.jpg); background-size: 150px; background-repeat: no-repeat;"></div>
    <div class="slide" title="Марка:<br>Модель:" style="background: url(car3.jpg); background-size: 150px; background-repeat: no-repeat;"></div>
    <div class="slide" title="Марка:<br>Модель:" style="background: url(car4.jpg); background-size: 150px; background-repeat: no-repeat;"></div>
    <div class="slide" title="Марка:<br>Модель:" style="background: url(car5.jpg); background-size: 150px; background-repeat: no-repeat;"></div>
    <div class="slide" title="Марка:<br>Модель:" style="background: url(car6.jpg); background-size: 150px; background-repeat: no-repeat;"></div> -->
  </div>
  <div class="slick-progress-bar">
    <div class="slick-progress"></div>
  </div>
</div>
 
<!-- POPOVER STRING -->
<div id="search_pop"  class="popover fade top in" style="display:">
	<!--div class="arrow"></div-->
<div class="popover-inner">
<h3 id="popTitle" class="popover-title"><?php echo $l['row action']; ?></h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; margin-left:20px; right:10px; top:6px"><?php echo $l['close']; ?></a><p><div class="spax">
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i> <?php echo $l['only remove']; ?></a><br><br>
<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-magnet"></i> <?php echo $l['save and renmove']; ?></a><br><br>
<center><select id="search_column" class="selectpicker search_column span3" simple data-container="body"  data-live-search="true" data-size="auto" ></select></center></div>
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-trash"></i> <?php echo $l['cleare']; ?></a><br><br>
</div>
</div>
</div>
<!-- POPOVER NUMERIC -->
<div id="search_pop2" style="display:" class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitle" class="popover-title"><?php echo $l['row action']; ?></h3>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; right:10px; top:6px"><?php echo $l['close']; ?></a><p><div style="float:left; width:300px; height:150px">

<a class="pointer_a" onclick="saveParam()" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-remove"></i> <?php echo $l['only remove']; ?></a>
<br />
<br />
<center>
</center>

<br />
<a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-ok"></i><?php echo $l['save and renmove']; ?></a>
<center><a class="pointer_a" from="table" onclick="removeParam(this)" style="color:#000; margin-left:20px; font:normal 13px Arial"><i class="icon-trash"></i> <?php echo $l['cleare']; ?></a></center>

</div>
</div>
</div>
</div>
<!-- POPOVER RADIO -->

<div id="search_pop3" style="display:" class="popover fade top in">
<div class="arrow"></div>
<div class="popover-inner">
<h3 id="popTitleRadio" class="popover-title"><?php echo $l['units change']; ?></h3>
<center>
</br>
<!--<label class="radio">
  <input type="radio" name="optionsRadios" id="optionsRadios1" value="m2" checked>
  <?php echo $l['m2']; ?>
</label>
<label class="radio">
  <input type="radio" name="optionsRadios" id="optionsRadios2" value="yd2">
  <?php echo $l['yd2']; ?>
</label><label class="radio">
  <input type="radio" name="optionsRadios" id="optionsRadios2" value="ft2">
  <?php echo $l['ft2']; ?>
</label>
-->
<div class="btn-group btn-group-vertical" data-toggle="buttons-radio" id="popoverRadioButtonGroup">
  <a type="button" id="popRadio1" class="btn btn-link"><?php echo $l['m2']; ?></a>
  <a type="button" id="popRadio2" class="btn btn-link"><?php echo $l['yd2']; ?></a>
  <a type="button" id="popRadio3" class="btn btn-link"><?php echo $l['ft2']; ?></a>
</div>
<p>
</p>
</br>
<a type="button" id="popRadio3" class="btn btn-success" onclick="closeSearchModal()"><?php echo $l['apply']; ?></a>
</center>
<div class="popover-content"><a class="pointer_a" onclick="closeSearchModal()" style="position:absolute; right:10px; top:6px"><?php echo $l['close']; ?></a><p><div style="float:left; width:300px; height:auto">
<center>

</center>
</div>
</div>
</div>
</div>
<!-- END POPOVER -->
<!-- Modal units -->
<div class="modal hide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<?php include('includes/units_modal.php'); ?>
</div>

<!-- Modal units -->


<div id="posY">
<div style="position:relative; z-index:10; bottom:-55px;" class="span2">
</div> 
</div>
  <div class="slick-cnt" style="height:150px;">&nbsp;</div>
  <div class=tblBar>
    <div style="margin-top: 3px;"></div>
    <input class="input-large" type="text" class="form-control" placeholder='<?php echo $l['search']; ?>' style="margin-top: 9px;height: 17px;">
    <div style="margin-top: 10px;">
      <span style="margin-left: 10px;">Шрифт:</span>
      <div class="btn-group" data-toggle="buttons-radio">
      <button type="button" class="btn btn-default active font" value=0><i class="icon-resize-small"></i></button> 
      <button type="button" class="btn btn-primary font" value=1><i class="icon-resize-full"></i></button> 
      <button type="button" class="btn btn-info font" value=2><i class="icon-resize-full"></i><i class="icon-resize-full"></i></button> 
      <button type="button" class="btn btn-success font" value=3><i class="icon-resize-full"></i><i class="icon-resize-full"></i><i class="icon-resize-full"></i></button> 
     </div>
      <span style="margin-left: 10px;">Фото:</span>
      <div class="btn-group" data-toggle="buttons-radio">
			<button type="button" class="btn btn-default active foto" value=0><i class="icon-resize-small"></i></button> 
			<button type="button" class="btn btn-primary  foto" value=1><i class="icon-resize-full"></i></button> 
			<button type="button" class="btn btn-info  foto" value=2><i class="icon-resize-full"></i><i class="icon-resize-full"></i></button> 
			<button type="button" class="btn btn-success  foto" value=3><i class="icon-resize-full"></i><i class="icon-resize-full"></i><i class="icon-resize-full"></i></button> 

		<!-- <label class="btn btn-default active">
          <input type="radio" value=0> Off
        </label>
        <label class="btn btn-primary">
          <input type="radio" value=1> S
        </label>
        <label class="btn btn-info">
          <input type="radio" value=2> M
        </label>
        <label class="btn btn-success">
          <input type="radio" value=3> L
        </label> -->
     </div>
     <div></div>
     
     <div></div>
    </div>
  </div>
  <div style="height:60px;">&nbsp;</div>
  <div id="table_base" style='table-hover overflow-y: hidden; overflow-x: hidden;'>
    <div class=hTable></div>
    <div class=mTable style="font-size:18px;">
      <div>
        <div>
          <div></div>
          <div></div>
        </div>
      </div>
      <!--div id="jsqroll_div">
      </div-->
          <div></div>
          <div></div>
    </div>

  </div>
  
  <div id="dialog_window_minimized_container"></div>

  <script src="../tablica/js/$.js"></script>
  <script src="../tablica/css/bootstrap-slider/bootstrap-slider.js"></script>
  <script src="../tablica/css/bootstrap-select/bootstrap-select.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../tablica/js/table.js"></script>
 
  <script src="../tablica/js/db.js"></script>
  <script src="../tablica/js/banners.js"></script>
  <script src="../tablica/js/modal.php"></script>
  <script src="../tablica/js/mainjs.php<?php echo "?razdel=".$_GET[razdel]; ?>"></script>
  <script src="../js/query.js"> </script>
  <script src="js/layout.js"> </script>
  <script src="js/slick.js"> </script>
  <script src="js/spin.min.js"> </script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<body>
</html>