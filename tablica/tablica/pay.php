<?php

	//require_once '../includes/data_from_ip.php';
	@session_start();
	if(!isset($_COOKIE['country']))
	{
		setcookie("country", $ip_country, time()+7776000);
	}
	
	if(!isset($_COOKIE['city']))
	{
		setcookie("city", $ip_city, time()+7776000);
	}
	
	if(!isset($_COOKIE['currency']))
	{
		setcookie("currency", $ip_currency, time()+7776000);
	}

	if ( isset($_GET['lang']) )
	{
		$lang = $_GET['lang'];
		setcookie("lang", $lang, time()+7776000);
	}
	else
	{
		if ( empty( $_COOKIE['lang'] ) ) 
		{ 
			$languages = array( 'tm','ru','en','ar','bg','hu','vi','ht','el','da','he','id','es','it','ca','lv','lt','hm','de','nl','no','fa','pl','pt','ro','sk','sl','tr','uk','fi','fr','cs','sv','et','zh','th','hi','ja');		

			if ( in_array( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2), $languages ) )
			{ 
				$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
			} 
			else
			{
				$lang = "en";
			}
			setcookie("lang", $lang, time()+7776000);
		}
		else
		{
			$lang = $_COOKIE['lang'];
		}
	}
	$lang="ru";
	
	$l = parse_ini_file("../lang/" . $lang . ".ini");
?>

<?php
/*	if($_COOKIE['auth']!=1)
	{
		header('Location: index2.php');
		die();
	}
	*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru-RU" xml:lang="en">
<head>



    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>INFOKAIN.COM - Информация под рукой</title>

    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="../css/bootstrap-select.css" type="text/css" media="screen" />
	
   <script src="../js/jquery-1.9.1.js"></script>
   <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-select.min.js"></script>
   <script src="../js/tooltip.js"></script>
   <script src="../js/bootstrap-collapse.js"></script>
<script type="text/javascript">
/*    window.onload = function() {
        var countries = document.getElementById('countries4');
        countries.onchange = function() {
			var country = countries.options[countries.selectedIndex].value;
            document.getElementById('country_link').innerHTML=countries.options[countries.selectedIndex].value;
        }
        
        var cities = document.getElementById('cities4');
        cities.onchange = function() {
            document.getElementById('city_link').innerHTML=cities.options[cities.selectedIndex].value;
        }
        
        var currencies = document.getElementById('currencies');
        currencies.onchange = function() {
            document.getElementById('currency_link').innerHTML=currencies.options[currencies.selectedIndex].value;
        }
        
        var languages = document.getElementById('languages');
        languages.onchange = function() {
            document.getElementById('lang_link').innerHTML=languages.options[languages.selectedIndex].value;
        }
        
        $('#countries4').on('change', function() {
			$.ajax({
                    url: 'get_cities.php?country='+$('#countries4').val(),
                    type: 'GET',
                    success: function(data) {
                        $('#cities4').html(data).selectpicker('refresh');
                    },
                    error: function() {
                        alert("boom");
                    }
                });
			});
    }
*/

</script>

<script>

	

function Go(Obj) {
    document.getElementsByName(Obj.name)[1].disabled=(1-Obj.checked);
}
</script>


</head>

<body>
<div class="container">
	<div class="alert alert-info"><center><h4><?php echo $l['Invoice for payment allocation method declaration']; ?> "Тут разные методы"</h4><center></div>
	<div class="alert alert-success"><center><h4><?php echo $l['Payable']; ?> 100 IM = 100$</h4><center></div>
	<div class="row">
		<div class="span4">
			<div class="alert alert-success"><center><h4><?php echo $l['ad']; ?></h4><center></div>
			<table class="table">
				<tr>
					<td><h5><p class="text-success"><?php echo $l['ad id']; ?> </p></h5></td>
					<td><h5><p class="text-info">12345</p></td>
				</tr>
				<tr>
					<td><h5><p class="text-success"><?php echo $l['owner']; ?> </p></h5></td>
					<td><h5><p class="text-info">turkmeninfo@yandex.ru</p></td>
				</tr>
			</table>
			<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#ads_content"><h5><?php echo $l['Ad Content']; ?></h5></button>
			<div id="ads_content" class="collapse">
			Привет я твое объявление
			</div>
		</div>
		<div class="span4">
		<?php if (isset($_SESSION["id"])) {?>
			<div class="alert alert-success"><center><h4><?php echo $l['authorized']; ?></h4><center></div>
			<table class="table">
				<tr>
					<td><h5><p class="text-success"><?php echo $l['user']; ?> </p></h5></td>
					<td><h5><p class="text-info">turkmeninfo@yandex.ru</p></td>
				</tr>
				<tr>
					<td><h5><p class="text-success"><?php echo $l['In the bank']; ?> </p></h5></td>
					<td><h5><p class="text-info">99 IM <span class="label label-important"><?php echo $l['insufficiently']; ?></span></p></td>
				</tr>
				<tr>
					<td>ИЛИ</td>
					<td></p></td>
				</tr>
				<tr>
					<td><h5><p class="text-success">На счету: </p></h5></td>
					<td><h5><p class="text-info">101 IM <span class="label label-success"><?php echo $l['enough']; ?></span></p></td>
				</tr>
			</table>
			<label class="checkbox">
				<input name='test' type="checkbox" onclick="Go(this)" />
				<?php echo $l['I agree with the']; ?> <a type="button" class="btn btn-link" href="../../about.php" target="_blank"><?php echo $l['the terms and conditions ']; ?></a>
			</label>
			<input name='test' type="button" class="btn btn-success btn-block btn-large" disabled="disabled" value="<?php echo $l['pay']; ?>" />
			<center><a type="btn" class="btn btn-link" onClick='self.close()'><h5><?php echo $l['Refuse payment']; ?></h5></a></center>
			
			
			<?php } else {?>
			<div class="alert alert-danger"><center><h4><?php echo $l['NOT AUTHORIZED']; ?></h4><center></div>
			<div class="thumbnail">
				<center><legend><h4><?php echo $l['Authorization']; ?></h4></legend></center>
				<p></p>
				<center>
				<form class="form-horizontal" action="../user_room/login.php?redirect=../tablica/pay.php" method="post">
				  <div class="control-group">
					<div class="input-prepend">
					  <span class="add-on">@</span><input class="span2" id="prependedInput" type="text" placeholder="Email" name="username">
					</div>
				  </div>
				  <div class="control-group">
					<div class="input-prepend">
					  <span class="add-on"><i class="icon-th"></i></span><input class="span2" id="prependedInput" size="16" type="text" placeholder="<?php echo $l["Password"]; ?>" name="password">
					</div>
				  </div>
				  <div class="control-group form-inline">
						<button class="btn btn-primary" name="login"><?php echo $l["Sign in"]?></button>
						 <label class="checkbox">
							<input type="checkbox" name="remember"> <?php echo $l['Remember']; ?>
						  </label>
				  </div>
						<button class="btn btn-success "><?php echo $l['Forgot your password']; ?></button>
				</form>	
				</center>
			</div>
			<?php }?>
		</div>
		<div class="span4">
			<div class="alert alert-success"><center><h4><?php echo $l['other methods']; ?></h4><center></div>
			<div class="thumbnail">
				<form class="form-horizontal">
				  <img src="../img/pay/webmoney.png" class="img-rounded" href="...">
				  <img src="../img/pay/visa.png" class="img-rounded" href="...">
				  <img src="../img/pay/yandex-money.png" class="img-rounded" href="...">
				  <img src="../img/pay/paypal.png" class="img-rounded" href="...">
				</form>	
				</center>
			</div>
			<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#webmoney"><img src="../img/pay/webmoney.png" class="img-rounded" href="..."></button>
			<div id="webmoney" class="collapse">
			<label class="checkbox">
				<input name='test2' type="checkbox" onclick="Go(this)" />
				Согласен с <a type="button" class="btn btn-link" href="../about.php" target="_blank">условиями сайта</a>
			</label>
			<input name='test2' type="button" class="btn btn-success btn-block btn-large" value="Оплатить" />
			</div>
			<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#visa"><img src="../img/pay/visa.png" class="img-rounded" href="..."></button>
			<div id="visa" class="collapse">
			<label class="checkbox">
				<input name='test2' type="checkbox" onclick="Go(this)" />
				Согласен с <a type="button" class="btn btn-link" href="../about.php" target="_blank">условиями сайта</a>
			</label>
			<input name='test2' type="button" class="btn btn-success btn-block btn-large" value="Оплатить" />
			</div>
		</div>
	</div>
</div>
</body>
</html>
