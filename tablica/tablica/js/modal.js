toBanners = [];

function modalWindow(title, content, id){

  var win = {
    title: title,
    setTabContent: function(num, content){
      tabContent[num] = content;
      this.dom.find('.tabs>div').eq(num).html(content);
    },
    setSubTabContent: function(num, content){
      tabContent[num] = content;
      this.dom.find('.subTabs>div').eq(num).html(content);
    },
    close: function(){
      this.dom.remove();
      var self = this;
      setTimeout(function(){
        var i = ModalWindows.indexOf(self);
        ModalWindows.splice(i,1);
      },5);
      
    },
    minimize: function(){
      
      this.x = win.dom.offset().left;
      this.y = win.dom.offset().top;
      
      this.minimizedDom = $(htmlMin);
      this.minimizedDom.find('.title').text(this.title);
      var self=this;
      this.minimizedDom.click(function(){self.maximize()});
      
      $('#dialog_window_minimized_container').append(this.minimizedDom);
      this.dom.remove();
      
    },
    maximize: function() {
      
      $(document.body).append(win.dom);
      this._init();
      this.minimizedDom.remove();
    },
    selectTab: function(num){
      this.dom.find('.tabs > div[active]').removeAttr("active");
      this.dom.find('.tabs-btn li[active]').removeAttr("active");
      this.dom.find('.tabs > div').eq(num).attr("active","");
      this.dom.find('.tabs-btn li').eq(num).attr("active","");
    },
    selectSubTab: function(num){
      this.dom.find('.subTabs-btn li[active]').removeAttr("active");
      this.dom.find('.subTabs > div[active]').removeAttr("active");
      this.dom.find('.subTabs > div').eq(num).attr("active","");
      this.dom.find('.subTabs-btn li').eq(num).attr("active","");
      // Фикс на слайдеры
      this.dom.find('.slider').css({'width':''});
    },
    _init: function(){
      var self=this;
      // Обработчики табов  
      for(i=0;i<5;i++) {
        self.dom.find('.tabs-btn li').eq(i).click(function(){
          self.selectTab($(this).index());
        })
      }
            
      for(i=0;i<4;i++) {
        self.dom.find('.subTabs-btn li').eq(i).click(function(){
          self.selectSubTab($(this).index());
        })
      }
      
      this.dom.mousedown(function(){
        $('.modal-window').css({'z-index':11});
        $(this).css({'z-index':12,'box-shadow':'0px 0px 12px 0px rgba(64, 255, 0, 0.8)'});
      }).mouseup(function(){
        $(this).css({'box-shadow':'0px 0px 12px 0px rgba(127, 127, 127, 0.25)'});
      })
      
      // Cлайдеры 
      this.dom.find('.slider').slider();
      
      
      this.dom.find('.subTabs > div:eq(0) .slider').on('slide', function(e){
          
          var mult = 0.15;
          self.dom.find('.subTabs > div:eq(0) .table tr td:eq(0) .label').text(e.value);
          self.dom.find('.subTabs > div:eq(0) .table tr td:eq(1) .label:eq(0)').text((e.value*mult).toFixed(2));
          self.dom.find('.subTabs > div:eq(0) .table tr td:eq(2) .label:eq(0)').text((e.value*mult).toFixed(2));
      });
      
      this.dom.find('.subTabs > div:eq(1) .slider').on('slide', function(e){
          
          var mult = 0.3;
          self.dom.find('.subTabs > div:eq(1) .table tr td:eq(0) .label').text(e.value);
          self.dom.find('.subTabs > div:eq(1) .table tr td:eq(1) .label:eq(0)').text((e.value*mult).toFixed(1));
          self.dom.find('.subTabs > div:eq(1) .table tr td:eq(2) .label:eq(0)').text((e.value*mult).toFixed(1));
          $("#colored_num_days").val(e.value);
          $("#colored_pay_sum").val(e.value*mult);
          
      });
      
      this.dom.find('.subTabs > div:eq(2) .slider').on('slide', function(e){
          
          var mult = 0.6;
          self.dom.find('.subTabs > div:eq(2) .table tr td:eq(0) .label').text(e.value);
          self.dom.find('.subTabs > div:eq(2) .table tr td:eq(1) .label:eq(0)').text((e.value*mult).toFixed(1));
          self.dom.find('.subTabs > div:eq(2) .table tr td:eq(2) .label:eq(0)').text((e.value*mult).toFixed(1));
      });
      
      // Вкладка 4.4 (в полоску банеров)
      
      toBanners[id] = {
        list: [],
        price: 0,
        cur: {
          image: 1,
          country: '',
          city: '',
          section: '',
          days: 1,
          price: 1.2
        }
      }
      
      this.dom.find('.subTabs > div:eq(3) .banner-imgs > img').click(function(){
        self.dom.find('.subTabs > div:eq(3) .banner-imgs > img').removeClass('active');
        $(this).addClass('active');
        toBanners[id].cur.image = $(this).index()+1;
      });
      
      var s = self.dom.find('.subTabs > div:eq(3) .banner-view select:eq(0)').empty();
      s.append('<option>Выбирите город</option>');
      $.getJSON("ajax_proxy.php?sub=cat&path=/countries",function(d){
        $.each(d,function(k,v){s.append('<option value="'+k+'">'+v+'</option>')});
        s.change();
      });
      
      this.dom.find('.subTabs > div:eq(3) .banner-view select:eq(0)').on('change', function(){
        var s = self.dom.find('.subTabs > div:eq(3) .banner-view select:eq(1)').empty().prop('disabled', true);
        if(!$(this).val())return;
        toBanners[id].cur.country = $(this).val();
        s.append('<option>Выбирите город</option>');
        $.getJSON("ajax_proxy.php?sub=cat&path=/countries/"+$(this).val(),function(d){
          $.each(d,function(k,v){s.append('<option value="'+k+'">'+v+'</option>')});
          s.prop('disabled', false);
        });
      });
      
      this.dom.find('.subTabs > div:eq(3) .banner-view select:eq(1)').on('change', function(){
        toBanners[id].cur.city = $(this).val();
      });
      
      this.dom.find('.subTabs > div:eq(3) .banner-view select:eq(2)').on('change', function(){
        toBanners[id].cur.section = $(this).val();
      });
      
      this.dom.find('.subTabs > div:eq(3) .banner-imgs > img:eq(0)').addClass('active');
      
      this.dom.find('.subTabs > div:eq(3) .slider').on('slide', function(e){
          
          toBanners[id].cur.days = e.value;
          toBanners[id].cur.price = 1.2 * e.value;
      });
      
      this.dom.find('.subTabs > div:eq(3) .slider + button').click(function(e){
        
        if(!toBanners[id].cur.country || !toBanners[id].cur.city || !toBanners[id].cur.section )return
        toBanners[id].list.push(toBanners[id].cur);
        toBanners[id].price += toBanners[id].cur.price;
        self.dom.find('.subTabs>div:eq(3) .banner-view select:eq(0)').val($(".subTabs > div:eq(3) .banner-view select:eq(0)>option:first").val());
        self.dom.find('.subTabs>div:eq(3) .banner-view select:eq(1)').val($(".subTabs > div:eq(3) .banner-view select:eq(0)>option:first").val());
        self.dom.find('.subTabs>div:eq(3) .banner-view select:eq(2)').val($(".subTabs > div:eq(3) .banner-view select:eq(0)>option:first").val());
        self.dom.find('.subTabs > div:eq(3) .table tr td:eq(0) .label').text(toBanners[id].list.length);
        self.dom.find('.subTabs > div:eq(3) .table tr td:eq(1) .label:eq(0)').text(toBanners[id].price.toFixed(1));
        self.dom.find('.subTabs > div:eq(3) .table tr td:eq(2) .label:eq(0)').text(toBanners[id].price.toFixed(1));
        
        self.dom.find('.subTabs > div:eq(3) .banner-imgs > img').removeClass('active');
        self.dom.find('.subTabs > div:eq(3) .banner-imgs > img:eq(0)').addClass('active');
        
        self.dom.find('.subTabs>div:eq(3) .items').append('<div>'+toBanners[id].cur.country+', '+toBanners[id].cur.city+', '+toBanners[id].cur.section+' ('+toBanners[id].cur.days+' дней)</div>');
        toBanners[id].cur = { image: 1, country: '', city: '',section: '', price: 1.2, days: 1 };
        self.dom.find('.subTabs > div:eq(3) .slider').val(1);
      });
      
      
      
              
      // Обработчики закрытия и сворачивания
      this.dom.find('header>div>div:eq(0)').click(function(){self.minimize()})
      this.dom.find('header>div>div:eq(1)').click(function(){self.close()})
      this.dom.find('header>div>div:eq(0), header>div>div:eq(1)').mousedown(function(e){e.stopPropagation()})
      
      // Обработчик перемещения
      this.dom.find('header').mousedown(function(e){
        var dx = e.clientX - self.dom.offset().left;
        var dy = e.clientY - self.dom.offset().top;
        $(document.body).mousemove(function(e){
          self.dom.css({top: e.clientY-dy, left: e.clientX-dx});
        }).mouseup(function(){
          $(document.body).off('mousemove');
        })
      })
    }
  }
    
  var html = '<div class=modal-window><header><span>'+title+'</span><div><div></div><div></div></div></header><ul class=tabs-btn><li><span class=comment></span></li><li><span class=contact></span></li><li><span class=image></span></li><li active><span class=star></span></li><li><span class=locked></span></li></ul><div class=tabs><div>1</div><div>2</div><div>3</div><div active><ul class=subTabs-btn><li active><span class=refresh></span></li><li><span class=lightbulb></span></li><li><span class=pin></span></li><li><span class=flag></span></li></ul><div class="subTabs"><div active>Таб 4.1</div><div>Таб 4.2</div><div>Таб 4.3</div><div>Таб 4.4</div></div></div><div>5</div></div></div>';
  
  var htmlMin = '<div style="display:block" class="dialog_window_minimized ui-widget ui-state-default ui-corner-all"><span class="title"></span><span class="ui-icon ui-icon-newwin"></span></div>';
  
  win.dom = $(html);
  
  // Установка контента в окно
  for(var i in content) {
    if(!content.hasOwnProperty(i))continue;
    if(content[i] instanceof Array){
      for(var j in content[i]) {
        if(!content[i].hasOwnProperty(j))continue;
        win.dom.find('.subTabs > div').eq(j).html(content[i][j]);
      }
    } else {
      win.dom.find('.tabs > div').eq(i).html(content[i]);
    }
  }
  
  win._init();
    
  
  
  
      
  
  win.selectTab(0);
  win.selectSubTab(0);
  
  // Показываем окно
  $(document.body).append(win.dom.css({opacity:0}));
  if(!lastCoord){
    lastCoord = {}
    var y = lastCoord.y = (($(window).height() - win.dom.outerHeight()) / 4) + $(window).scrollTop();
    var x = lastCoord.x = (($(window).width() - win.dom.outerWidth()) / 4) + $(window).scrollLeft();
  }else{
    var y = lastCoord.y += 25;
    var x = lastCoord.x += 25;
  }
  win.dom.css({top:y,left:x,opacity:0}).mousedown().animate({opacity:1},200,function(){$(this).mouseup()});
  
  ModalWindows.push(win);
  
  return win;
}

ModalWindows = [];

lastCoord = null;

function open_win_transport(id){

  var banner = adsDB.getById(id) || banners.getById(id);
  
  // Атрибуты первой вкладки
  var attrs = {
    type: "Тип",
    marka: "Марка",
    style: "Кузов",
    price: "Цена",
    privod: "Привод",
    toplivo: "Топливо"
  }
  
  var htmlAttrs = '';
  for(var key in attrs) { if(banner.hasOwnProperty(key))
    htmlAttrs += '<div><div>'+attrs[key]+'</div><div>'+banner[key]+'</div></div>'; 
  }
  
  // Атрибуты второй вкладки
  attrs = {
    country: "Страна",
    city: "Город",
    name: "Имя",
    phone: "Телефон",
    chat: "Онлайн чат",
    mail: "Написать"
  }
  
  // Заглушка для контактов
  banner.name = 'Гриша'; 
  banner.phone = '+78125556699';
  banner.chat = '<button class="btn btn-default">Начать чат</button>';
  banner.mail = '<textarea class="form-control"></textarea><br><button class="btn btn-default">Написать</button>';
  
  var contactAttrs = '';
  for(var key in attrs) { contactAttrs += '<div><div>'+attrs[key]+'</div><div>'+banner[key]+'</div></div>'; }
  
  // 4 Картинки
  var imagesHtml = '';
  for(var key in banner.image){ imagesHtml += '<img src="'+banner.image[key]+'" alt="">'; }
  
  modalWindow('Объявление #'+id,[
    '<h4>Текст объявления</h4><div class="attrs">'+htmlAttrs+contactAttrs+'</div>',
    '<h4>Фотографии</h4><div class="imgs">'+imagesHtml+'</div>',
    '<h4>Контакты</h4><iframe width="300" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?ie=UTF8&amp;t=m&amp;ll='+banner.map+'&amp;spn=20.405939,26.367188&amp;z=4&amp;output=embed"></iframe>',
    [
    '<form><h4>Каждые сутки наверх</h4><img src="css/images/24-top.jpg"><h5>Количество суток</h5><input class="slider  slider-horizontal" type="text" data-slider-min="1" data-slider-max="30" data-slider-value="1"><table class="table"><tr><th>Дней</th><th>К оплате</th><th>В валюте</th></tr><tr><td><span class="label label-warning">1</span></td><td><span class="label label-success">0.15</span> <span class="label label-info" rel="tooltip" title="Infokain Many бонусные средства на сайте которые Вы можете купить за любую валюту, а также заработать на них, еслы Вы их купите со скидкой , а потом продадите. Подробности в партнерской программе">IM</span></td><td><span class="label label-info">0.15</span> <span class="label label-success">$</span></td></tr></table><button type="button" class="btn btn-primary">Оплатить</button> </form>',
    
    '<h4><form id="coloredform" action="pay.php" method="get">Яркая строчка</h4><img src="css/images/selected.jpg"><h5>Количество суток</h5><input class="slider  slider-horizontal" type="text" data-slider-min="1" data-slider-max="30" data-slider-value="1"><input type="hidden" id="colored_num_days" value="0"><input type="hidden" id="colored_pay_sum" value="0"><table class="table"><tr><th>Дней</th><th>К оплате</th><th>В валюте</th></tr><tr><td><span class="label label-warning">1</span></td><td><span class="label label-success">0.3</span> <span class="label label-info" rel="tooltip" title="Infokain Many бонусные средства на сайте которые Вы можете купить за любую валюту, а также заработать на них, еслы Вы их купите со скидкой , а потом продадите. Подробности в партнерской программе">IM</span></td><td><span class="label label-info">0.3</span> <span class="label label-success">$</span></td></tr></table><button type="button" class="btn btn-primary" onClick="$('+"'#coloredform'"+').submit();">Оплатить</button><input type="sumbit" value=""></form>',
    
    '<h4>Верхняя позиция</h4><img src="css/images/top-selected.jpg"><h5>Количество суток</h5><input class="slider  slider-horizontal" type="text" data-slider-min="1" data-slider-max="30" data-slider-value="1"><table class="table"><tr><th>Дней</th><th>К оплате</th><th>В валюте</th></tr><tr><td><span class="label label-warning">1</span></td><td><span class="label label-success">0.6</span> <span class="label label-info" rel="tooltip" title="Infokain Many бонусные средства на сайте которые Вы можете купить за любую валюту, а также заработать на них, еслы Вы их купите со скидкой , а потом продадите. Подробности в партнерской программе">IM</span></td><td><span class="label label-info">0.6</span> <span class="label label-success">$</span></td></tr></table><button type="button" class="btn btn-primary">Оплатить</button>',
    
    '<h4>В полосу рекламы</h4><div class="help">Выберите фотографию</div><div class="banner-imgs">'+imagesHtml+'</div><div class="help">Выберите область отображения</div><div class="banner-view"><select class="form-control"></select><select class="form-control"></select><select class="form-control"><option>Выбирите город</option><option value=1>Транспорт</option><option value=2>Недвижимость</option><option value=3>Рестораны</option></select></div><div class="help">Выберите количество суток</div><input class="slider  slider-horizontal" type="text" data-slider-min="1" data-slider-max="30" data-slider-value="1"><button style="margin-bottom:10px" type="button" class="btn btn-primary">Добавить позицию</button><div class="items"></div><table class="table"><tr><th>Позиций</th><th>К оплате</th><th>В валюте</th></tr><tr><td><span class="label label-warning">0</span></td><td><span class="label label-success">0</span> <span class="label label-info" rel="tooltip" title="Infokain Many бонусные средства на сайте которые Вы можете купить за любую валюту, а также заработать на них, еслы Вы их купите со скидкой , а потом продадите. Подробности в партнерской программе">IM</span></td><td><span class="label label-info">0</span> <span class="label label-success">$</span></td></tr></table><button type="button" class="btn btn-primary">Оплатить</button>'        
    ],
    '<h4>Пожаловаться</h4><div class="radio"><label><input type="radio" name="moder" value="opt1" checked>Не дозвонился</label></div><div class="radio"><label><input type="radio" name="moder" value="opt2" checked>Повторяется</label></div><div class="radio"><label><input type="radio" name="moder" value="opt3" checked>Нереальная цена</label></div><div class="radio"><label><input type="radio" name="moder" value="opt4" checked>Это спам</label></div><div class="radio"><label><input type="radio" name="moder" value="opt5" checked>Не актуально</label></div><div class=captcha><img src="http://vk.com/captcha.php?sid=230800254149&s=1"><button type="button" class="btn btn-link">Обновить</button><div><input type="text" class="form-control" style="width:64px;" placeholder="код"></div></div><button type="button" class="btn btn-primary">Отправить</button>'
  ], id);
  
  
}

											
									