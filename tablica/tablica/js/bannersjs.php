<?php
  if(isset($_GET["razdel"]))
  	$razdel = intval($_GET["razdel"]);
  	
  	
 	if ( isset($_GET['lang']) )
	{
		$lang = $_GET['lang'];
		SetCookie("lang", $lang, time()+7776000);
	}
	else
	{
		if ( empty( $_COOKIE['lang'] ) ) 
		{ 
			$languages = array( 'tm','ru','en','ar','bg','hu','vi','ht','el','da','he','id','es','it','ca','lv','lt','hm','de','nl','no','fa','pl','pt','ro','sk','sl','tr','uk','fi','fr','cs','sv','et','zh','th','hi','ja');		

			if ( in_array( substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2), $languages ) )
			{ 
				$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
			} 
			else
			{
				$lang = "en";
			}
			SetCookie("lang", $lang, time()+7776000);
		}
		else
		{
			$lang = $_COOKIE['lang'];
		}
	}
	//$lang="ar";
	
	//echo "../../lang/estete/" . $lang . ".ini";
	$l = parse_ini_file("../../lang/Table_window/" . $lang . ".ini");
	//var_dump($l);
 	
 ?>	

function getbanners()
{
  $.getJSON("http://localhost/infokain-web3Abdy/ads.php?action=get&format=json&q=banners&category=1",function(data){
    for(key in data.aaData){
      index.banners[data.aaData[key].ad_id] = data.aaData[key];
      index.banners_pre.push(data.aaData[key].ad_id);
    }
        
    for(var i=0;i<6;i++) {
      swapBanner(i,0);
      $('.banners > div:eq('+i+')').mouseover(function(){
        index.banner_hover = $(this).index();
      }).mouseout(function(){index.banner_hover = -1;})
    }
    
    /*setInterval(function() {
      var rnd = getRandomInt(0,5);
      if(index.banner_hover==rnd){
        rnd++;
        if(rnd==6)rnd=0;
      }
      swapBanner(rnd, 300);
    }, 3000);*/
  });

}

function banners(){

  var banners = [];
  var bannersById = {};
  var count = 0;
  var width = 150;
  var maxWidth = 150;
  var cycles = 0;
  var cursor = 0;
  var isMoved = false;
  
  var _JqBannersWrap = $('.bannersWrap');
  var _JqBanners = $('.banners');
  var block = _JqBanners.find('>div').clone();
  _JqBanners.empty();
  
  $.getJSON('../ads.php?action=get&format=json&q=banners&category=1',function(data){
    for(key in data){
      banners.push(data[key]);
      bannersById[data[key].id] = data[key];
    }
    view();
  });
  
  _JqBannersWrap.find('a').click(function(){
    move($(this).hasClass('left')?-1:1);
  });
  
  /*_JqBanners.mouseenter(function(){
    if(interval) clearInterval(interval);
  }).mouseleave(function(){
    interval = setInterval(function(){move(1)}, 3000)
  })
  
  _JqBanners.mouseleave();
  */
  $(window).resize(function(){view()});
  
  function move(direct, func){
    if(isMoved)return;
    isMoved=true;
    // Создать слева или справа
    
    _JqBanners.css('z-index',9);
    
    if(direct>0){
      var w = _JqBanners.children().first().width(); // direct
      _JqBanners.append(block.clone().css({width:w,position:'absolute',right:-(w+10)}));
    }else{
      var w = _JqBanners.children().last().width(); // direct
      _JqBanners.prepend(block.clone().css({width:w,position:'absolute',left:-(w)}));
    }
    
    // Выбор блока
    if(direct>0){
      var index = cursor++ +count;
      while(index>banners.length-1)index-=banners.length;
      if(cursor>banners.length-1){cursor=0;cycles++}
      dbToBanner(count, banners[index]);
    }else{
      var index = --cursor;
      while(index<0)index+=banners.length;
      if(cursor<0)cursor=banners.length-1;
      dbToBanner(0, banners[index]);
    }
    
    // Сдвинуть
    var lefts = [];
    var completed = 0;
    _JqBanners.css({height:_JqBanners.height()+40});
    _JqBanners.children().each(function(){lefts.push($(this).position().left)})
    var targetOffset = (direct>0)?-(w+10):(w+10);
    _JqBanners.children().each(function(){
      $(this).css({position:'absolute',left:lefts[$(this).index()]}).animate({left:lefts[$(this).index()] + targetOffset}, 500, 
      function(){completed++; if(completed>count){
        // Удалить лишний
        if(direct>0){
          _JqBanners.children().first().remove(); // direct
        }else{
          _JqBanners.children().last().remove();
        }
        _JqBanners.children().css({position:'',left:'',right:''});
        _JqBanners.css({height:'','z-index':11});
        isMoved = false;
      }});
    })
  }
  
  function view(){
    var allWidth = _JqBanners.width();
    var reWrite = false;
    var lastCount = count;
    count = Math.ceil((allWidth)/(maxWidth+10));
    width = Math.floor((allWidth)/count);
    var delta = allWidth-width*count;
    
    if(count<lastCount)_JqBanners.find('>div:gt('+(count-1)+')').remove();
    
    for(var i=0;i<count;i++){
      if(i>=lastCount){
        _JqBanners.append(block.clone().css('width',width-((delta-->0)?9:10)));
        dbToBanner(i, banners[(i+cursor>banners.length-1)?(i+cursor)-banners.length:i+cursor]);
      }else{
        _JqBanners.children().eq(i).css('width',width-((delta-->0)?9:10));
      }
    }
    _JqBannersWrap.css({'height':''});
  };
  
  function dbToBanner(index, obj) {
    var dom = _JqBanners.find('>div').eq(index);
    dom.find('img').attr('src', obj.banner_image);
    dom.find('.price').text('$'+obj.price);
    //dom.find('.price').text(obj.id-1);
    dom.find('.info').html('<div>Марка: '+obj.marka+'</div><div>Модель: '+obj.model+'</div><div>Кузов: '+obj.style+'</div>');
    dom.off('click');
    dom.click(function(){ open_win_transport(obj.id); });
  }
  
  obj = {
    getById: function(id){
      return bannersById[id];
    }
  }
  
  return obj;
}