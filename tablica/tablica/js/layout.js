/*
	Layout Table
	By Zerg 2014
*/

var TLayout = function() {

	// Colls positions
	this.colls = [];

	this.carouselHideEnable = false;

	this.countSlidesShown = 0;

	// Carousel
	this.carousel = null;

	// Initialization
	this.init = function() {

		this.showMask();

		this.colls = [];

		// Set Head Colls Info
		if(this.colls.length == 0) {
			
			this.setHeadCollsInfo();
			var hWidth = 0;
			for(var i=0;i<this.colls.length;i++) {
				hWidth  = hWidth+this.colls[i];
			}
			$(".hTable").width(hWidth+20);
			$(".tblBar").width(hWidth+20);
			// Carousel
			this.carouselInit();
		}

		//$(".mTable").width($(".hTable").width());
		$("#table_base").width($(".hTable").width()+20);

		// Height
		var h = $('.hTable').position().top + $('.hTable').height();
		$(".mTable").height($(document).height() - h);

		//this.setHeadCollsWidth();
		this.setBodyCollsWidth();

		this.hideMask();
	}

	this.setHeadCollsInfo = function() {
		var scope = this;
		var hCols = $('.hTable > div');
		hCols.each(function(index, el) {
			scope.colls.push(el.offsetWidth);
		});
	}

	this.setBodyCollsWidth = function() {
		var scope = this;
		var count = this.colls.length;
		var mTable = $(".mTable>div>div>div>div");
		var countSkip = count - mTable.length/2;
		var margin = countSkip * 52+"px";
		mTable.each(function(index, el) {
			if(countSkip == 0)
				$(el).width(scope.colls[index]);
			else
				if(countSkip != 0) countSkip--;
			if(index == scope.colls.length-1) $(el).css('width', '100%');
		});
		$(".mTable").css("margin-left", margin);
	}

	this.setHeadCollsWidth = function() {
		var scope = this;
		var count = this.colls.length;
		var mTable = $(".hTable>div");
		mTable.each(function(index, el) {
			$(el).width(scope.colls[index]);
		});
	}

	this.carouselInit = function() {
		var scope = this;
		if(this.carousel == null) {
			$.ajax({
				url: "../ads.php?q=banners",
				dataType: 'json',
				success: function(res) {
					if(res && res.length) {
						var slideHtml = ""
						for(var i=0;i<res.length; i++) {
							 slideHtml = slideHtml + scope.getSlideItem(res[i]);
						}
						$(".slick").append(slideHtml);
						scope.slidesLayout();
					}
				}
			});
		}
		// Set carousel position
		var cWidth = $(".slick-container").width();
		var wWidth = $(window).width();
		var left = wWidth/2 - cWidth/2;
		$(".slick-container").css("left", left);
	}

	this.getSlideItem = function(slide) {
		var slideProps = "";
		for(prop in slide) if (slide.hasOwnProperty(prop)) {
    	slideProps+=prop+": "+slide[prop]+"<br>";
		}
		return "<div onclick='open_win_transport("+slide.id+");' class='slide' title='"+ slideProps + "' style='background: url(" + slide.banner_image + "); background-size: 150px; background-repeat: no-repeat;'>";
	}

	this.slidesLayout = function() {
		var scope= this;
		this.carousel = $('.slick').slick({
			  slidesToShow: 4,
			  slidesToScroll: 1,
			  autoplay: true,
			  autoplaySpeed: 3000,
			  onAfterChange: function() {
			  	scope.countSlidesShown++;
			  	scope.setCarouselProgress(this.slideCount, this.currentSlide);
			  	if(this.slideCount*2<=scope.countSlidesShown) {
			  		scope.enableHideCarousel();
			  	} else {
			  		var title = $(".slick-buttons a.icon-resize-small").attr("title_tmpl");
			  		title = title.replace("{0}", (this.slideCount*2-scope.countSlidesShown)*3);
			  		$(".slick-buttons a.icon-resize-small").attr("title", title);
			  	}
			  }
			});
		// Tooltips init
		$(".slick .slide").tooltip({
			content: function (callback) {
				//debugger;
     		callback($(this).prop('title'));
  		}
		});
	}

	this.setCarouselProgress = function(slideCount, currentSlide) {
		var progress = ((currentSlide+1)/slideCount)*100;
		$(".slick-progress").css("height", progress+"%");
	}

	this.enableHideCarousel = function() {
		this.carouselHideEnable = true;
		$(".slick-buttons a.icon-resize-small").removeClass("disabled");
		$(".slick-buttons a.icon-resize-small").attr("title", $(".slick-buttons a.icon-resize-small").attr("title_close"));
	}

	this.hideCarousel = function() {
		if(!this.carouselHideEnable) return;
		$(".slick-container").hide();
		$(".slick-cnt").hide();
		$(".slick-container-info").show();
		$(".slick-container-info-cnt").show();
		this.init();
	}

	this.showCarousel = function() {
		$(".slick-container-info").hide();
		$(".slick-container-info-cnt").hide();
		$(".slick-container").show();
		$(".slick-cnt").show();
		this.init();
	}

	this.showMask = function() {
		$("#mask").css("display", "block");
	}

	this.hideMask = function() {
		$("#mask").css("display", "none");
	}
}

var tLayout = new TLayout();

var opts = {
  lines: 13, // The number of lines to draw
  length: 20, // The length of each line
  width: 10, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#fefefe', // #rgb or #rrggbb or array of colors
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: 'auto', // Top position relative to parent in px
  left: '50%' // Left position relative to parent in px
};

if(!String.prototype.addSlasches)
{
  String.prototype.escape = function() {
    return this.replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
  }
}

$(function() {
	var target = document.getElementById('mask');
	var spinner = new Spinner(opts).spin(target);
	tLayout.showMask();
	$(window).on("resize", function() {
		window.location.reload();
		// tLayout.init();
	});
});