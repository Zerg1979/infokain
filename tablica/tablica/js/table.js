function updateScroll()
{
	_JqScroll = _JqTable.find('>div:last');

}

function mainTable(){

  // 1. Приватные переменные таблицы
  
  var _JqBar = $('.tblBar');
  var _JqCount = _JqBar.find('>div:first');
  var _JqTable = $('.mTable');
  var _JqTblMain = _JqTable.find('>div:first');
  var _JqTblFiller = _JqTblMain.find('>div');
  var _JqTblBlocks = _JqTblFiller.find('>div');
  var _JqTblBlock = _JqTblFiller.find('>div:eq(0)');
  var _JqTblBlockNext = _JqTblFiller.find('>div:eq(1)');

  var _JqHeadTable = $('.hTable');

/*  var _JqScroll = _JqTable.find('>div:last');
  var _JqScrollLift = _JqScroll.find('>div:first');
  var _JqScrollProgress = _JqScroll.find('>div:last');
  var _ScroollLiftClass = 'active';


/*  var _JqScroll = _JqTable.find('body');
  var _JqScrollLift = _JqScroll.find('');
  var _JqScrollProgress = _JqScroll.find('>div:last');
  var _ScroollLiftClass = 'active';
*/
  
  var cacheCols = [[],[],[]];

  var _imageOn = 0;
  var _elemHeightDefault = 30;
  var _elemHeightImages = [30,102,152,202]; 
  var _elemHeight = _elemHeightDefault;
  var _tblHeight = _JqTable.height();
  
  var _elemCount = 0;
  var _elemMinimized = true;
  var _offset = 0;
  var _attrs = {}
  var _sort;
  var _sortDesc;
  
  var blockPos = -10;
  var blockNextPos = -10;
  var PreventScrollEv = false;
  var blockSize = 200;

  // Обработчики cобытий таблицы
  (function setEventHandlers(){
    // Скролбар
/*    _JqScrollLift.mousedown(function(e){
      var t = $(this);
      var my = _JqScroll.height()-t.height();
      var dy = e.clientY - t.addClass(_ScroollLiftClass).position().top;
      
      $(window).mousemove(function(e){
        var y = e.clientY-dy;
        t.css({'top':(y<0)?0:(y>my)?my:y});
        viewBodyTable();
      })
      
      $(window).one('mouseup', function(){
        t.removeClass(_ScroollLiftClass);
        $(window).off('mousemove mouseover');
      }).mouseover(function(e){
        if(e.which!=1){$(window).mouseup()}
      })
    });
    */
    // Показать / убрать автоселект
    _JqHeadTable.on('click','div.ctrls .search',(function(){
    	
    	console.log(this.parentNode.parentNode.attributes["data-attr"].childNodes[0].textContent);

    	if(this.parentNode.parentNode.attributes["data-attr"].childNodes[0].textContent == "price")
    	{
 		   	showPopoverNum(this);
 		   	
 		}
 		else
 			showPopover(this);
    	
    	//console.log(this);
      /*$(this).parent().hide();
      $(this).parent().next().show();
      $(this).parent().next().find('.selectpicker').selectpicker();
    })).on('click','div.search .remove',(function(){
      $(this).parent().hide();
      $(this).parent().prev().show();*/
    }));

    // Событие выбора автоселекта
    _JqHeadTable.on('change','div.search select',(function(){
      _attrs[ $(this).parents('.maxi').attr('data-attr') ][1] = $(this).val();
      blockPos = blockNextPos = -10;
      table.view();
    }));
    
    // Сброс выборки  
    _JqHeadTable.on('click','.mini .ctrl.remove',(function(){
      _attrs[ $(this).parents('.mini').attr('data-attr') ][1] = null;
      blockPos = blockNextPos = -10;
      table.view();
    }));
    
    // Удалятор столбца
    _JqHeadTable.on('click','.maxi div.ctrls > .ctrl.remove',(function(){
      _attrs[ $(this).parents('.maxi').attr('data-attr') ][1] = -1;
      blockPos = blockNextPos = -10;
      table.view();
    }));
    
    // Возврат столбца 
    _JqHeadTable.on('click','.mini .ctrl.maximize',(function(){
      _attrs[ $(this).parents('.mini').attr('data-attr') ][1] = null;
      blockPos = blockNextPos = -10;
      table.view();
    }));
    
    // Сортировщик столбца
    _JqHeadTable.on('click','div.ctrls .ctrl.sort',(function(){
      var attr = $(this).parents('.maxi').attr('data-attr');
      if(_sort==attr){
        _sortDesc = (_sortDesc==0) ? 1 : 0;
      }else{
        _sortDesc=1;
        _sort=attr;
      }
      blockPos = blockNextPos = -10;
      table.view();
    }));
    
    // Событие скролла (лагает иногда)
/*    _JqTblMain.scroll(function(){
      if(PreventScrollEv){
        PreventScrollEv = false;
      }else{
      
        _JqScrollLift.css({'top':$(this).scrollTop() / (_elemCount*_elemHeight-_tblHeight) * 
            (_tblHeight-_JqScrollLift.height())});
            
        viewBodyTable();
             
        setTimeout(function(){
        	t=$(this); 
          _JqScrollLift.css({'top':Math.round(t.scrollTop() / (_elemCount*_elemHeight-_tblHeight) * 
            (_tblHeight-_JqScrollLift.height()))});
              
        }, 5000);
      }
    });*/
  
    // Переключатель картинок
    $(".foto", _JqBar).click(function(ev){
      table.changeImage($(this).attr("value"));
    });

    // Переключатель шрифтов
    $(".font", _JqBar).click(function(ev){
      table.changeFont($(this).attr("value"));
    });
    
    // Ресайз
    $(window).resize(function(){normalizeТable()});
    
    // Общий поиск
    _JqBar.find('input[type="text"]').keyup(function(){
      var t=this;
      var a = setTimeout(function(){
        adsDB.setStateSearch($(t).val());
        table.setElemCount(adsDB.count());
        blockPos = blockNextPos = -10;
        viewBodyTable();
      },1);
    })
    
    // Смена единиц
    _JqHeadTable.on('change','.maxi > div:first-child > select',(function(ev){
      blockPos = blockNextPos = -10;
      _attrs[ $(this).parents('.maxi').attr('data-attr') ][3] = $(this).val();
      viewBodyTable();
    }));
  
  })();
 
  // Перерисовка тела таблицы
  function viewBodyTable(){
    var writeBlock = function (block, pos, n) {
      var adsList = adsDB.get(pos*blockSize, blockSize);
          //console.log(adsList);
      var sel=[];var doms={};for(var key in _attrs) {
        if(_attrs[key][1]==null) {
          var col = document.getElementById('values_'+n+'_'+key);
          //console.log(col);
          var chs = col.childNodes; while(chs.length)col.removeChild(chs[0]);
          if(_imageOn)col.className = 'isPhotos';
          doms[key] = col;
          sel.push(key);
        }
      }
      
      block.remove();
      
      for(var k=0;k<sel.length;k++){
      
        var attr = sel[k];
        var col = doms[attr];
        
        for(var i=0;i<adsList.length;i++){
        
          var ads = adsList[i];
          var value = ads[attr];
          if(_attrs[attr][3]>0){
            value *= (_attrs[attr][2][_attrs[attr][3]].mult);
            value = value.toFixed(2);
          }
          var elem = document.createElement("DIV");
          elem["title"] = value;
          elem.appendChild(document.createTextNode(value));
          $(elem).addClass("mtrow_"+i);
          if(adsList[i].colored && adsList[i].colored == 1) $(elem).addClass("ads_colored");
          if(adsList[i].ficherd && adsList[i].ficherd == 1) $(elem).addClass("ads_ficherd");
          $(elem).on("mouseover", selectTblRow);
          $(elem).on("mouseout", unselectTblRow);          
          if(k==0){
            var photos = document.createElement("DIV");
            photos.className = 'photos';
            if(_imageOn)photos.style.display = 'block';
            
            //console.log(images);
            
            var images = ads.image;
            for(var j=0;j<images.length;j++){
              var image = document.createElement("IMG");
              image.setAttribute("src", images[j]);
              photos.appendChild(image);
            }
            elem.appendChild(photos);
          }
          (function(){
            var id = ads.id;
            elem.onclick = function(){open_win_transport(id)}
          })();
          
          col.appendChild(elem);
        }
      }
        
     
      for(var k=0;k<sel.length;k++)block[0].appendChild(doms[sel[k]]);
      block[0].style.top = pos*blockSize*_elemHeight + 'px';
      _JqTblFiller[0].appendChild(block[0]);
    }
  
    // Смещение в кол-ве элементов
    var a = 0;
    //_tblHeight-_JqScrollLift.height();
    if(a>0){
      var offset = (_elemCount-_tblHeight/_elemHeight) * _JqScrollLift.position().top / 
      (_tblHeight-_JqScrollLift.height());
    }else{
      var offset = 0;
    }
    
    _JqTblMain.scrollTop(Math.round(offset * _elemHeight));
    
    var n = 0;
    var pos = Math.floor(offset/blockSize);
    if(pos!=blockPos){ blockPos=pos; writeBlock(_JqTblBlock, pos, 1); n++ }
    
    if((offset/blockSize-pos)<.5){
      pos--;
      if(pos<0){pos+=2}
    }else{
      pos++;
      if(pos*blockSize >= _elemCount){pos-=2}
    }
    
    if(pos!=blockNextPos){ blockNextPos=pos; writeBlock(_JqTblBlockNext, pos, 2); n++ }
    
    
    
    if(n>0){
      for(var i=0;i<cacheCols[1].length;i++){
        cacheCols[1][i].style.width = '';
        cacheCols[2][i].style.width = '';
      }
      normalizeТable();
    }

    tLayout.init();
  }
 

  function selectTblRow() {
    $("."+$(this).attr("class")).addClass("current_row");
  }

  function unselectTblRow() {
    $(".current_row").removeClass("current_row");
  }

  // ads radio popover
 
  // Перерисовка головы и тела таблицы
  function viewHeadTable(){
      
    _JqHeadTable.empty();
    _JqTblBlocks.empty();
    cacheCols = [[],[],[]];
    
    var html ='';
    where = {};
    
    for(var key in _attrs){
    
      var prev = _JqHeadTable.find('.mini').last();
    
      if(_attrs[key][1]==-1) {
      
        // 1. Cкрытые
        html = '<div data-attr="'+key+'" id="head_'+key+'" class="mini"><div class="ctrls"><span class="maximize ctrl"></span></div><div>'+stringPre('<b>'+_attrs[key][0]+'</b>')+'</div></div>';
        (!prev[0]) ? _JqHeadTable.prepend(html) : prev.after(html);
             
      }else if(_attrs[key][1]==null){
        // 2. Открытые
        var sortClass = (_sort != key)?'':( (_sortDesc == 1)?' up':' down');
        var meas='';/*if(_attrs[key][2]){
          meas += ' <select>';
          for(var i in _attrs[key][2]){
            meas += '<option'+((i==_attrs[key][3])?' selected':'')+' value='+i+'>'+_attrs[key][2][i].name+'</option>';
          }
          meas += '</select>';
        }*/
        if(key == "price")
	        text = '<a class="btn btn-link">usd</a>';
		else if(key == "ceiling_height")
	        text = '<a  href="#myModal" role="button" data-toggle="modal" class="btn btn-link">m</a>';
		else if(key == "total_area")
	        text = '<a class="btn btn-link">m<sup>2</sup></a>';
		else if(key == "area")
	        text = '<a class="btn btn-link">a</a>';
		else
			text = "";
	        
        
        _JqHeadTable.append('<div data-attr="'+key+'" id="head_'+key+'" class="maxi"><div>'+_attrs[key][0]+meas+'</div><div class="ctrls"><span class="search ctrl"></span><span class="sort'+sortClass+' ctrl"></span><span class="remove ctrl"></span>'+text+'</div><div class=search><select class="selectpicker dropup" data-live-search="true"><option></option></select><span class="remove ctrl"></span></div></div>');
        _JqTblBlock.append('<div data-attr="'+key+'" id="values_1_'+key+'"></div>');
        _JqTblBlockNext.append('<div data-attr="'+key+'" id="values_2_'+key+'"></div>');
        cacheCols[1].push(document.getElementById('values_1_'+key));
        cacheCols[2].push(document.getElementById('values_2_'+key));
        
        // Селекты
        //alert("number filter");
        var elems = adsDB.getUniq(key);
        for(var i=0;i<elems.length;i++){
          _JqHeadTable.find('#head_'+key+' .search select').append('<option>'+elems[i]+'</option>');
        }
        
      }else{
      	// ads
      	// select radio
        // 3. Минимизированные
        html = '<div data-attr="'+key+'" id="head_'+key+'" class="mini"><div class="ctrls"><span class="remove ctrl"></span></div><div>'+stringPre('<b>'+_attrs[key][0]+'</b>:'+_attrs[key][1])+'</div></div>';
        (!prev[0]) ? _JqHeadTable.prepend(html) : prev.after(html);
        // Формирование where
        where[key] = _attrs[key][1];
      }

      cacheCols[0].push(document.getElementById('head_'+key));
    }
        
    adsDB.setState(where, (_sort)?((_sortDesc==1)?{desc: _sort}:{asc: _sort}):{});
    table.setElemCount(adsDB.count());
    viewBodyTable();
  }
  
  // Нормализация (выравнивание) таблицы
  function normalizeТable(){
    
    var sum = 0;
    var count = 0;
    var selected = 0;
    
    for(var i=0;i<cacheCols[0].length;i++){
      if(cacheCols[0][i].className=='mini'){
        sum += 51;
        selected++;
      }else{
        sum += Math.max(cacheCols[1][count].clientWidth, cacheCols[2][count].clientWidth);
        count++;  
      }
    }
        
    sum = ($(window).width()>sum)?$(window).width():sum;
    sum -= selected*51+20;
    
    
    //document.styleSheets[3].cssRules[0].style.width = sum+'px';
            
    var delta = sum % count;
    var x = (sum - delta) / count;
    count = 0;
    var calcLength = 0;
    
    for(var i=0;i<cacheCols[1].length;i++) {
      calcLength = ((count++ < delta) ? x+1 : x);

      cacheCols[1][i].style.width = cacheCols[2][i].style.width = (calcLength+12) + 'px';
      
      document.getElementById('head_'+cacheCols[2][i].getAttribute('data-attr')).style.width = calcLength + 'px';
      
      // console.log(cacheCols[1][i].style.width);
      // console.log(cacheCols[2][i].style.width);
      // console.log(document.getElementById('head_'+cacheCols[2][i].getAttribute('data-attr')).style.width);
      // if(i==0){
      //   cacheCols[1][i].style.marginLeft = cacheCols[2][i].style.marginLeft = selected*51 + 'px';
      // }
    }
  }
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // ОБЪЕКТ КАК ВНЕШНИЙ ИНТЕРФЕЙС ТАБЛИЦЫ ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
  var table = {
  	//scroll:_JqScroll,
    getBlockSize: function(){
      return blockSize;
    },
    setElemCount: function(count){
      _elemCount = count;
      var allHeight = _elemHeight * count;
      _JqTblFiller.css({height:allHeight});
      var tmp = _tblHeight / allHeight;
      var h = _tblHeight * ((tmp>1)?1:tmp);
      //_JqScrollLift.css({height:(h<20)?20:h});
      _JqCount.text('Найдено объявлений: '+count);
    },
    changeImage: function(val) {
      _imageOn = (val==0) ? 0 : 1;
      _elemHeight = _elemHeightImages[val];
      _JqTblBlocks.find('.photos').css("height", (_elemHeight-30)+'px');
      _JqTblBlocks.find('.photos').css("width", '500%');
      _JqTblBlocks.find('.photos').css("background-color", 'transparent');
      _JqTblBlocks.find('.photos img').css("height", (_elemHeight-52)+'px');
      _JqTblBlocks.find('.photos img').css("width", 'auto');
       //_JqTblBlocks.find('.photos').parent().css("margin-bottom", (_elemHeight)+'px');
      
      if(_imageOn){
        _JqTblBlocks.removeClass('isPhotos1');
        _JqTblBlocks.removeClass('isPhotos2');
        _JqTblBlocks.removeClass('isPhotos3');
        _JqTblBlocks.find('.photos').show();
        if(val == 1) _JqTblBlocks.addClass('isPhotos1');
        if(val == 2) _JqTblBlocks.addClass('isPhotos2');
        if(val == 3) _JqTblBlocks.addClass('isPhotos3');
      }else{
        _JqTblBlocks.find('.photos').hide();
        _JqTblBlocks.removeClass('isPhotos1');
        _JqTblBlocks.removeClass('isPhotos2');
        _JqTblBlocks.removeClass('isPhotos3');
      }
      this.setElemCount(_elemCount);
    },
    changeFont: function(val) {
      if(val == 0) { $(".mTable").css("font-size", "18px"); }
      if(val == 1) { $(".mTable").css("font-size", "20px"); }
      if(val == 2) { $(".mTable").css("font-size", "24px"); }
      if(val == 3) { $(".mTable").css("font-size", "28px"); }
    },
    setHead: function(list){
      _attrs = {};
      for(i in list) _attrs[i] = (typeof list[i] == 'string') ? 
        [list[i], null, null, null] : [list[i].name, list[i].value, list[i].meas, 0];
    },
    setProgress: function(procent){
      //_JqScrollProgress.css({height:_tblHeight * procent});
      if(procent == 1){
        setTimeout(function(){
          _JqScrollProgress.css({width:0});
        }, 1000);
      }
    },
    view: function(){
      viewHeadTable();
    }
  }
  
  return table;
}
