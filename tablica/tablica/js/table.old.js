﻿tableSelectedAttrs = {
  country: "Страна",
  city: "Город",
  type: "Тип",
  marka: "Марка",
  style: "Кузов",
  price: "Цена",
  privod: "Привод",
  toplivo: "Топливо"
}

tableStateAttrs = {
  country: null,
  city: null,
  type: null,
  marka: null,
  style: null,
  price: null,
  privod: null,
  toplivo: null
}

tableAllAttrs = {
  country: "Страна",
  city: "Город",
  type: "Тип",
  marka: "Марка",
  style: "Кузов",
  price: "Цена",
  privod: "Привод",
  toplivo: "Топливо"
}

table = {
  selAttrs: tableSelectedAttrs,
  allAttrs: tableAllAttrs,
  stateAttrs: tableStateAttrs,
  sortAttr: null,
  sortDesc: null,
  page: 1,
  pages: 0,
  rows: 100,
  imageOn: 0,
  view: function(){
  
    // Обращение к базе данных
    // Формирование where
    where = {};
    for(var key in this.stateAttrs) {
      if(this.stateAttrs[key] && this.stateAttrs[key] != -1){
        where[key] = this.stateAttrs[key];
      }
    }
    
    // Формирование order
    
    var order = (this.sortAttr)?((this.sortDesc==1)?{desc: this.sortAttr}:{asc: this.sortAttr}):{};    
    
    var adsList = adsDB.select(where, order, [(this.page-1)*this.rows,this.rows]);
    this.pages = Math.ceil(adsDB.lastCount/this.rows);
        
    // Очистка таблицы
    $('.adsTbl > .main').empty();
    // И пажинатора
    $('.adsTbl .pagination').empty();
    
    
    // Рисуем пагинатор
    var tmp = Math.max(1, this.page-3);
    var max = Math.min(this.pages+1, tmp+6);
    if(this.page!=1){ $('.adsTbl .pagination').append('<li page=1><a>&laquo;</a></li>'); }    
    for(var i=tmp;i<max;i++){
      if(this.page==i){
        $('.adsTbl .pagination').append('<li page='+i+' class="active"><a page='+i+'>'+i+'</a></li>');
      }else{
        $('.adsTbl .pagination').append('<li page='+i+'><a>'+i+'</a></li>');
      }
    }
    if(this.page!=this.pages){ $('.adsTbl .pagination').append('<li page='+this.pages+'><a>&raquo;</a></li>'); } 
    
    // Рисуем голову
    var html ='';
    for(var key in this.selAttrs) {
      if(this.stateAttrs[key]==-1){
      
        
        
        // Cкрытые
        html = '<div data-attr="'+key+'" id="table_attr_'+key+'"><div class="head vertical"><div class="ctrls"><span class="maximize ctrl"></span></div><div>'+stringPre('<b>'+this.selAttrs[key]+'</b>')+'</div></div></div>';
        
        if(!$('.adsTbl > .main .vertical').last().parent()[0]){
          $('.adsTbl > .main').prepend(html);
        }else{
          $('.adsTbl > .main .vertical').last().parent().after(html);
        }
        
      }else if(!this.stateAttrs[key]){
        
        var sortClass = (this.sortAttr != key)?'':( (this.sortDesc == 1)?' up':' down');
              
        $('.adsTbl > .main').append('<div data-attr="'+key+'" id="table_attr_'+key+'"><div class="head hor"><div>'+this.selAttrs[key]+'</div><div class="ctrls"><span class="search ctrl"></span><span class="sort'+sortClass+' ctrl"></span><span class="remove ctrl"></span></div><div class=search><select class="selectpicker dropup" data-live-search="true"><option></option></select><span class="remove ctrl"></span></div></div><div class=values></div></div>');
      }else{
        // Минимизированные
        html = '<div data-attr="'+key+'" id="table_attr_'+key+'"><div class="head vertical"><div class="ctrls"><span class="remove ctrl"></span></div><div>'+stringPre('<b>'+this.selAttrs[key]+'</b>:'+this.stateAttrs[key])+'</div></div></div>'
        
        if(!$('.adsTbl > .main .vertical').last().parent()[0]){
          $('.adsTbl > .main').prepend(html);
        }else{
          $('.adsTbl > .main .vertical').last().parent().after(html);
        }
      }
      
      // Селекты
      var elems = adsDB.getUniq(key);
      for(var i=0;i<elems.length;i++){
        $('.adsTbl > .main #table_attr_'+key+' select').append('<option>'+elems[i]+'</option>');
      }
    }
    
    // Обработчик фиксации головы
    // TODO перебросить эвент в другое место.
    var table_y = $('.adsTbl .head').eq(0).offset().top;
    var navH = $('.adsTbl .nav').eq(0).height();
    
    $(window).scroll(function(e){
      if(table_y < $(window).scrollTop()){
        $('.adsTbl .head').css({position:'fixed'});
      }else{
        $('.adsTbl .head').css({position:''});
      }
    });
    
    // Заполняем тело данными
    var n=0;
    for(i in adsList){
    
      if(n<100){ for(var key in this.selAttrs){
        var elem = $('<div>'+adsList[i][key]+'</div>');
        var id = adsList[i].ad_id
        $('#table_attr_'+key+' .values').append(elem);
        elem.click( function(){open_win_transport(id)} );
      }}
      
      var photos = '';
      adsList[i].image.forEach(function(im){
        photos += '<img src="'+im+'">';
      });
      
      $('.adsTbl > .main .head.hor+.values:eq(0) > div:last').eq(0).prepend('<div class="photos">'+photos+'</div>');
      
      n++;
    }
    
    if(table.imageOn){
      $('.adsTbl > .main .values .photos').show();
      $('.adsTbl > .main .values').addClass('isPhotos');
    }
    
    // Выравниваем таблицу
    this.normalize();
  },
  normalize: function(){
    var sum = 0;
    var count = 0;
    var selected = 0;
    
    
    $('.adsTbl > .main > div').each(function(){
      
      if($(this).find('.head').hasClass('vertical')){
        sum += 40;
        selected++;
      }else{
        $(this).css({width:''});
        sum += $(this).width();
        count++;      
      } 
    });
    
    sum = ($(window).width()>sum)?$(window).width():sum;
    sum -= selected*40;
    
    $('.adsTbl > .main .photos').width( sum );
    
    var delta = sum % count;
    var x = (sum - delta) / count;
    count = 0;
    
    $('.adsTbl > .main > div').each(function(){
      if($(this).find('.head').hasClass('vertical')){
        var w = 40;
      }else{
        var w = (count < delta) ? x+1 : x;
        count++;
      }
      $(this).css({width: w});
      $(this).find('.head').css({width: w});
    });
  }
}


$(function(){  
  $(window).resize(function(){
    table.normalize();
  });
  
  $('.selectpicker').selectpicker();
  
  // Делегированные обработчики на таблицу
  
  // Показать / убрать автоселект
  $('.adsTbl').on('click','div.ctrls .search',(function(){
    $(this).parent().hide();
    $(this).parent().next().show();
    $(this).parent().next().find('.selectpicker').selectpicker();
  }));
  
  $('.adsTbl').on('click','div.search .remove',(function(){
    console.log( $(this) );
    $(this).parent().hide();
    $(this).parent().prev().show();
  }));

  // Событие выбора автоселекта
  $('.adsTbl').on('change','div.search select',(function(){
    var attr = $(this).parent().parent().parent().attr('data-attr');
    table.stateAttrs[attr] = $(this).val();
    table.page = 1;
    table.view();
  }));
  
  // Сброс выборки  
  $('.adsTbl').on('click','.head.vertical .ctrl.remove',(function(){
    var attr = $(this).parent().parent().parent().attr('data-attr');
    table.stateAttrs[attr] = null;
    table.page = 1;
    table.view();
  }));
  
  // Сортировщик столбца
  $('.adsTbl').on('click','div.ctrls .ctrl.sort',(function(){
    var attr = $(this).parent().parent().parent().attr('data-attr');
    if(table.sortAttr==attr){
      if(table.sortDesc==0){table.sortDesc=1}else{table.sortDesc=0}
    }else{
      table.sortDesc=1;
      table.sortAttr=attr;
    }
    table.view();
  }));
  
  // Удалятор столбца
  $('.adsTbl').on('click','.head.hor div.ctrls > .ctrl.remove',(function(){
    var attr = $(this).parent().parent().parent().attr('data-attr');
    table.stateAttrs[attr] = -1;
    table.view();
  }));
  
  // Возврат столбца 
  $('.adsTbl').on('click','.head.vertical .ctrl.maximize',(function(){
    var attr = $(this).parent().parent().parent().attr('data-attr');
    table.stateAttrs[attr] = null;
    table.view();
  }));
  
  // Пагинатор 
  $('.adsTbl .pagination').on('click','li',function(){
    var attr = $(this).attr('page');
    table.page = attr;
    table.view();
  });
  
  // Сколько страниц
  $('.adsTbl .nav > select').on('change', function(){
    var attr = $(this).val();
    table.page = 1;
    table.rows = attr;
    table.view();
  });
  
  // Картинки
  $('.adsTbl .nav > button').click(function(){
    table.imageOn = (table.imageOn)? 0 : 1;
    $('.adsTbl > .main .values .photos').toggle();
    $('.adsTbl > .main .values').toggleClass('isPhotos');
  }) 
  
});

function stringPre(str){
  result = '';
  for(var i=0;i<str.length;i++){
    if(str[i]!='<'&&str[i]!='/'&&str[i]!='>'){
      result += str[i]+'\n';
    }else{
      result += str[i];
    }
  }
  return result;
}