﻿$(function(){

  adsDB = adsDB();

  banners = banners();

  mTable = mainTable();
  loading();
  mTable.setElemCount(50);
    
  mTable.setHead({
    country: "Страна",
    city: ",,,,",
    type: "Тип",
    marka: "Марка",
    style: "Кузов",
    price: {            
      name: "Цена",
      meas: [    
        {name:"RUB"}, 
        {name:"USD", mult: 35.18}, 
        {name:"EUR", mult: 47.64}
      ]
    },
    privod: "Привод",
    toplivo: "Топливо"
  });
    
});

// Вспомогательные функции таблицы

function loading(){
  
  var start = 0;
  var updateCount = mTable.getBlockSize() * 2;
  var end = 0;
  var noCall = false;
  var count = 0;
  var req = new XMLHttpRequest();
  req.open("GET", 'http://localhost/infokain-web3Abdy/ads.php?action=get&format=json&q=ads&category=0', true);
  req.onprogress = function(ev){
    update(ev);
  };
  req.onreadystatechange = function(ev){
    if(req.readyState == 4 && req.status == 200){
    	//console.log("ending json");
      //update(ev);
    }
  };
  req.send(null);
  
  var fl = 1;
  
  var update = function(ev){
    if(noCall)return;
    if(start==0){start = req.response.indexOf('[{')+1}else{start=end+1}
    end = req.response.lastIndexOf('}]}');
    if(end==-1){
      end = req.response.lastIndexOf('},{');
    }else{
      var finaly = 1;
      noCall=true;
    }
    end++;
    var part_json = '[' + req.response.substring(start,end) + ']';
//    console.log(part_json);
 //   console.log("start parsing json ");

    var arr = JSON.parse(part_json);
    count += arr.length;
    mTable.setElemCount(count);
 //   console.log("success "+arr.length);
    
    for(i in arr){
      adsDB.insert(arr[i].id, arr[i]);
    }
    
    if((count > updateCount || finaly) && fl==1){
      mTable.view(); fl=0;
    }
    
    mTable.setProgress( ev.loaded / ev.total );
    
    tLayout.init();
  }
}

function stringPre(str){
  result = '';
  for(var i=0;i<str.length;i++){
    if(str[i]!='<'&&str[i]!='/'&&str[i]!='>'){
      result += str[i]+'\n';
    }else{
      result += str[i];
    }
  }
  return result;
}