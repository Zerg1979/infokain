

function adsDB(){

  var state = [];
  var ids = {};
  var indexes = {};
  var stateCount = 0;

  var obj = {
    getUniq: function(field){
      var result = [];
      var self = this;
      for(key in indexes[field]){
        result.push(key);
      }
      return result;
    },
    setState: function(where, order){
      where = where || {};
      order = order || {};
      state = [];
      
      for(id in ids) {
        ids[id]._search = 0;
      }
      
      var value=0; 
      for(attr in where)value++
      var ok = value;
      if(value!=0) {
        for(attr in where){
          var list = indexes[attr][where[attr]];
          for(var i=0;i<list.length;i++){
            ids[list[i]]._search++;
            if(value==1 && ids[list[i]]._search == ok ){
              state.push(ids[list[i]]);
            }
          }
          value--;
        }
      } else {
        for(id in ids){
          state.push(ids[id]);
        }
      }
      
      stateCount = state.length;

      if(order.desc || order.asc){
        var f = order.desc || order.asc;
        state.sort(function(a,b){return(a[f]>b[f])?1:(a[f]<b[f])?-1:0});
        if(order.asc)state.reverse();
      }  
    },
    setStateSearch: function(search){
      state = [];
      for(var i in indexes){
        for(var k in indexes[i]){
          if(k.indexOf(search)!==-1){
            for(var j in indexes[i][k]){
              if(state.indexOf(ids[indexes[i][k][j]])===-1){
                state.push(ids[indexes[i][k][j]]);
              }
            }
          }
        }
      }
      stateCount = state.length;
    },
    get: function(offset, count){
      return state.slice(offset, offset+count);
    },
    getById: function(id){
      return ids[id];
    },
    count: function(){
      return stateCount;
    },
    insert: function(id, ads) {
      
      ads._search = 0;
      //ads = 1;
      ids[id] = ads;
         
      for(attr in ads) {
        if(!indexes[attr])indexes[attr] = {};
        if(!indexes[attr][ads[attr]])indexes[attr][ads[attr]] = [];
        indexes[attr][ads[attr]].push(id);
      }
    }
  };
  
  return obj;
}