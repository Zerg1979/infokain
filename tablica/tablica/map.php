        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Определение месторасположения на карте</h3>
        </div>

        <div class="modal-body row" >
            <div class="span8" id="maps_modal" style="height:400px; border:1px solid #ccc;"></div>
            <div class="span3" id="maps_info" style="height:400px;">
                <dl>
                <dt>Координаты маркера:</dt>
                <dd id="mapCoordinates">...</dd>
                <dt><input id="mapAdres" type="hidden" value=""/></dt>
                <dd><h3>Перетащите маркер если не точно</h3></dd>
                </dl>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
            <button class="btn btn-primary" id="mapSave" >Сохранить</button>
        </div>
