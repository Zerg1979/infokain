index = {
  banners: [],
  banners_pre: [],
  banners_post: [],
  banner_hover: -1
}

function getbanners()
{
  $.getJSON("../banner_search.php",function(data){
    for(key in data.aaData){
      index.banners[data.aaData[key].ad_id] = data.aaData[key];
      index.banners_pre.push(data.aaData[key].ad_id);
    }
        
    for(var i=0;i<6;i++) {
      swapBanner(i,0);
      $('.banners > div:eq('+i+')').mouseover(function(){
        index.banner_hover = $(this).index();
      }).mouseout(function(){index.banner_hover = -1;})
    }
    
    setInterval(function() {
      var rnd = getRandomInt(0,5);
      if(index.banner_hover==rnd){
        rnd++;
        if(rnd==6)rnd=0;
      }
      swapBanner(rnd, 300);
    }, 3000);
  });

}


$(function(){
  
  $.getJSON("../banner_search.php",function(data){
    for(key in data.aaData){
      index.banners[data.aaData[key].ad_id] = data.aaData[key];
      index.banners_pre.push(data.aaData[key].ad_id);
    }
        
    for(var i=0;i<6;i++) {
      swapBanner(i,0);
      $('.banners > div:eq('+i+')').mouseover(function(){
        index.banner_hover = $(this).index();
      }).mouseout(function(){index.banner_hover = -1;})
    }
    
    setInterval(function() {
      var rnd = getRandomInt(0,5);
      if(index.banner_hover==rnd){
        rnd++;
        if(rnd==6)rnd=0;
      }
      swapBanner(rnd, 300);
    }, 3000);
  });
  
})

function swapBanner(q,t){
  
  if(index.banners_pre.length == 0){
    index.banners_pre = index.banners_post;
    index.banners_post = [];
  }
  
  var n = getRandomInt(0, index.banners_pre.length-1);
  var banner = index.banners[index.banners_pre[n]];
  
  var id = index.banners_pre.splice(n,1);
  index.banners_post.push(id[0]);  
       
  $('.banners > div').eq(q).animate({opacity: 0}, t, function(){
  
    $(this).find('img').attr('src', banner.banner_image);
    $(this).find('div.price').text('$'+banner.price);
    $(this).find('div.info').html('<div>Марка: '+banner.marka+'</div><div>Модель: '+banner.model+'</div><div>Кузов: '+banner.style+'</div>');
    $(this).off('click');
    $(this).click(function(){
      open_win_transport(id);
    });
    $(this).animate({opacity: 1}, t);
  });
    
  
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}