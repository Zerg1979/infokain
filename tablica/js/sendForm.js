(function(){
    var forms           = $(".forms")
      , slider          = forms.find(".slider")
      , forInput        = forms.find(".forInput")
      , enabledElements = forms.find(".enabledElements")
      , checkElements   = forms.find(".checkElements")
      , sendForm        = forms.find(".sendForm");

    var _slider = function(){
        var thisElem = $(this)
          , thisForm = thisElem.parents("div.forms")
          , nextForm = $("." + thisElem.attr("next"))
          , attr     = thisElem.attr("attribute")

        thisForm.hide("slider");
        nextForm.show("slider");
    }

    var _createInput = function(){
        var thisElem = $(this)
          , thisForm = thisElem.parents("div.forms")
          , attr     = thisElem.attr("attribute")
          , value    = thisElem.attr("value")
          , input    = thisForm.find('input[name=' + attr + ']')

        if (input.length){
            input.replaceWith("<input type='hidden' name='" + attr + "' value='" + value + "'>")
        }else{
            thisElem.append("<input type='hidden' name='" + attr + "' value='" + value + "'>")
        }
    }

    var _enabledElements = function(){
        var thisElem = $(this)
          , nextForm = thisElem.attr("next")
            
          $("." + nextForm).find("input").prop("disabled", false)
          $("." + nextForm).find("select").prop("disabled", false).selectpicker()
    }

    var _checkElements = function(){
        var thisElem = $(this)
          , thisForm = thisElem.parents("div.forms")
          , inputs   = thisForm.find("input")
          , select   = thisForm.find("select")

          inputs.each(function(){
              var thisElem = $(this)
              if (thisElem.val() == ""){
                    thisElem.prop("disabled", true)
              }
          })

          select.each(function(){
              var thisElem = $(this)
              if(thisElem.val() == null || thisElem.val() == ""){
                    thisElem.prop("disabled", true)
              }
          })

    }

    var _sendForm = function(){
        var thisElem = $(this)
          , sendForm = thisElem.attr("next")
          , errorForm = thisElem.prev()

        $.ajax({
            url: "sendform.php", 
            data: $("."+sendForm).serialize(), 
            method: "POST",
            success: function (data) {
                if(data == "true"){
                    $('.PRIVATE_PERSON_BIG').hide();
                    $('.'+sendForm).show();
                }
                errorForm.show()
                errorForm.html(data)
            }
        });
    }


    $("#otpravka").on("click", function(){
        $.ajax({
            url: "sendform.php", // указываем URL и i
            data: $(".sendFormEstate").serialize(), // тип загружаемых данных 
            method: "POST",
            success: function (data) {
            console.log(data)
        }
        });
    })



    slider.on("click", _slider);
    forInput.on("click", _createInput);
    enabledElements.on("click", _enabledElements);
    checkElements.on("click", _checkElements)
    sendForm.on("click", _sendForm)
})();

