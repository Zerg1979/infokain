﻿$(document).ready(function(){
$('#service_photos').fileupload({
   	url: 'php/index.php',
   	dataType: 'json',
   	// Enable image resizing, except for Android and Opera,
    // which actually support image resizing, but fail to
   	// send Blob objects via XHR requests:
    disableImageResize: false,
    imageMaxWidth: 250,
    imageMaxHeight: 250,
   	imageCrop: false, // Force cropped images
	change: function (e, data) {
	var l = data.files.length;
		if(l < 5)
		{
		
		$('#add_photos').html('Выбрано фотографий: '+l);
		$('#photo_div').html('');
		servicePhotos = [];
        $.each(data.files, function (index, file) {
            $('#photo_div').append('<div class="span3" style="margin-top:20px;"><div class="alert alert-success" rel="tooltip"  title="При выборе способа выделение банером, будет отоброжаться это изображение"><center style="margin-left:20px"><strong>'+file.name+'</strong></center><div class="fileupload fileupload-new" data-provides="fileupload"><div class="fileupload-preview thumbnail" style="margin-left:15px; width: 150px; height: 150px;"><img src="php/thumb/'+file.name+'" /></div></div></div></div>');
			
			servicePhotos.push('php/files/'+file.name);
        });
		}
		else
		{
			alert('Загружайте не больше 4 фотографий');
		}
    }
});
});