$(function() {
	$('#images > div').each(function() {
		var $cfs = $(this);
		$cfs.carouFredSel({
			direction: 'up',
			circular: false,
			infinite: false,
			auto: false,
			scroll: {
				queue: 'last'
			},
			items: {
				visible: 1,
				width: 150,
				height: 110
			}
		});
		$cfs.hover(
			function() {
				$cfs.trigger('next');
			},
			function() {
				$cfs.trigger('prev');
			}
		);
	});
});