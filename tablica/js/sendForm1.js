        $(document).ready(function(){
			
			var data = new Array();
			
			$("#sendButton").click(function(){sendForm()})
			
			$(".forms a").not("#href").not("#sendButton").click(function(){
				
			    var form = $(".sendForm")
                var obj  = $(this)
                var next = $("."+obj.attr("next"))

				if (obj.attr("variables") != undefined){
                    if (obj.attr("attribute") != undefined || obj.attr("value") != undefined){
                        console.log(obj.attr("attribute"))
						$("dt#" + obj.attr("attribute")).remove()
						$("dd#" + obj.attr("attribute")).remove()
						$("input[type=hidden]#" + obj.next().attr("attribute")).remove()
						form.append("<dt id='"+ obj.attr("attribute") +"'>" + obj.attr("formName") + "</dt><dd id='"+ obj.attr("attribute")+"'>" + obj.text() + "</dd>")
						form.append("<input type='hidden' name='" + obj.attr("attribute") + "' id='" + obj.attr("attribute") + "' value='" + obj.text() + "'>");
                    }
				}
                else 
				{
                    if (obj.attr("attribute") != undefined || obj.attr("value") != undefined){
						$("dt#" + obj.attr("attribute")).remove()
						$("dd#" + obj.attr("attribute")).remove()
						$("input[type=hidden]#" + obj.next().attr("attribute")).remove()
						form.append("<dt id='"+ obj.attr("attribute") +"'>" + obj.attr("formName") + "</dt><dd id='"+ obj.attr("attribute")+"'>" + obj.text() + "</dd>")
						form.append("<input type='hidden' name='" + obj.attr("attribute") + "' id='" + obj.attr("attribute") + "' value='" + obj.text() + "'>");
                    }
					else{//now next button guys
						
						// REQUIRED CHECK INPUT, SELECT
						var req = obj.parents("div .forms").find('.required')//.css("border", "1px solid red");
						var jump_next = true;
						if(req.length){
							req.each(function(){
								if($(this).is("input"))//if input is empty
								{
									if(!$(this).val())//if empty red
									{
										$(this).css("box-shadow", "0px 1px 1px rgba(0, 0, 0, 0.075) inset, 0px 0px 5px 2px rgba(255, 0, 0, 0.8)");
										$(this).css("border-color", "rgba(255, 150, 100, 0.8)");
										jump_next = false; //disable jumping to next stage
									}
									else $(this).removeAttr("style");
								}
								else if($(this).is("select"))//if selector not selected
								{
									if(!$(this).attr("selected"))
										$(this).addClass("required1")
									
								}
								
								else if($(this).is("div"))// this is fucking dropdown button
								{
									if($(this).find("button > div.filter-option").text()=="...")//if not selected
									{
										$(this).css("box-shadow", "0px 0px 5px 2px rgba(255, 0, 0, 0.8)");
										jump_next = false; //disable jumping to next stage
									}
									else $(this).removeAttr("style");
								}
								
								//($(this).css("border", "1px solid red");
								//this.css("border", "1px solid red");
							})
						}
						if(!jump_next)return false;
					}
					
					obj.parents("div .forms").hide("slider")
					next.show("slider")
					
					//fucking dropdown select
					$("DIV.btn-group.bootstrap-select > BUTTON.btn.dropdown-toggle.btn-inverse > DIV.filter-option.pull-left").bind("DOMSubtreeModified",function(){
						var form = $(".sendForm");
						
						var label_text = $(this).parent().parent().prevAll("label").first().text();
						if(!label_text)label_text = $(this).parent().parent().prevAll("legend").first().text();
						
						var id = $(this).parent().parent().prevAll("select").attr("id");
						
						
						//THIS IS FOR COUNTRY -> CITY -> DISTRICT -> MORE ...
						if(id=='country'){//country was changed, we set cookie country to selected val
							alert($("#country").val());
							var expires = new Date();
							expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
							document.cookie = 'country=' + $("#country").val() + ';expires=' + expires.toUTCString();
						}
						
						//if prev element is input
						if($(this).parent().parent().prevAll("input").attr("class"))
						{
							
							var prev_input = $(this).parent().parent().prevAll("input");
							var prev_select = $(this).parent().parent().prevAll("select");
							
							var meter = parseFloat($('option:selected', prev_select).attr('meter'));
							
							$("dt#"+id).remove();
							$("dd#"+id).remove();
							$("input[type=hidden]#" + id).remove();
							
							form.append("<dt id='"+ id +"'>" + label_text + "</dt><dd id='"+ id +"'>" + prev_input.val() + " " + $(this).text() + "</dd>")
							form.append("<input type='hidden' name='" + id + "' value='" + prev_input.val() * meter + " " + $(this).text() + "'>");
						}
						//two selects
						else if($(this).parent().parent().parent(".btn-group").children('select').size()==2)
						{
							var label_text = $(this).parent().parent().parent().prevAll("label").text();
							if(!label_text)label_text = $(this).parent().parent().parent().prevAll("legend").text();
							var id = $(this).parent().parent().parent().children('select').eq(1).attr("id")
							
							if($(this).parent().parent().nextAll("select").attr("class"))//first changed
							{
								//alert('i am first bitch!');
								
								$("dt#"+id).remove();
								$("dd#"+id).remove();
								$("input[type=hidden]#" + id).remove();
								
								var second_select_val = $(this).parent().parent().parent().children('select').eq(1).val()
								
								form.append("<dt id='"+ id +"'>" + label_text + "</dt><dd id='"+ id +"'>" + $(this).text() + " - " + second_select_val + "</dd>")
								form.append("<input type='hidden' name='" + id + "' value='" + $(this).text() + " - " + second_select_val + "'>");
							}
							else//second
							{
								//alert('i am second man!');
								
								$("dt#"+id).remove();
								$("dd#"+id).remove();
								$("input[type=hidden]#" + id).remove();
								
								var first_select_val = $(this).parent().parent().parent().children('select').eq(0).val()
								
								form.append("<dt id='"+ id +"'>" + label_text + "</dt><dd id='"+ id +"'>" + first_select_val + " - " + $(this).text() + "</dd>")
								form.append("<input type='hidden' name='" + id + "' value='" + first_select_val + " - " + $(this).text() + "'>");
							}
						}
						else//just select
						{
							$("dt#" + id).remove()
							$("dd#" + id).remove()
							$("input[type=hidden]#" + id).remove()
							
							form.append("<dt id='"+ id +"'>" + label_text + "</dt><dd id='"+ id +"'>" + $(this).text() + "</dd>")
							form.append("<input type='hidden' name='" + id + "' value='" + $(this).text() + "'>");
						}
					});
					
					
                }
                return false
            }).css("cursor","pointer")
			
			$('.btn-inverse').click(function(){
				var form = $(".sendForm")
                var obj  = $(this)
				
                if (obj.attr("id") != undefined || obj.attr("value") != ""){
					$("dt#" + obj.attr("id")).remove()
					$("dd#" + obj.attr("id")).remove()
					$("input[type=hidden]#" + obj.next().attr("id")).remove()
					//alert(obj.parent().prev().text());
					form.append("<dt id='"+ obj.attr("id") +"'>" + obj.parent().prev().text() + "</dt><dd id='"+ obj.attr("id")+"'>" + obj.text() + "</dd>")
					form.append("<input type='hidden' name='" + obj.attr("id") + "' value='" + obj.text() + "'>");
				}
			
			})
			
			$("#email, #password").change(function(){
				if($(this).is("#email")){
					$("#password").val("")
					$("#confirm").hide(500);
					$("#password").show(500);
				}
				if($("#email").val() && $("#password").val())	//fields are filled and
				{
					var msg;
					
					//MAKING AJAX REQUEST TO SERVER
					$.ajax({
						url:"reg.php",
						type:"POST",
						async:false,
						data:{
							email:$("#email").val(),
							password:$("#password").val()
						},
						success:function(res){
							msg = res;
						},
						error: function(res){alert('Server connection lost');}
					});
					
					
					if(msg=='11') //email was registered and password is true 1 and 1
					{
						alert("user already registered");
					}
					else if(msg=='10')//email registered, but wrog password
					{
						$("#password").val('');
						$("#password").attr("placeholder", "Wrong password");
					}
					else//email was not registered 
					{
						$("#password").hide(500);
						$("#confirm").show(500);
						
						$("#confirm").attr("placeholder", "Type password again!");
					}
					
				}	
					 
				
			})
			$("#confirm").change(function(){
				if($("#password").val() == $("#confirm").val())
				{
					$.ajax({
						url:"reg.php",
						type:"POST",
						async:false,
						data:{
							email:$("#email").val(),
							password:$("#confirm").val()
						},
						success:function(res){
							alert(res);
						},
						error: function(res){alert('Server connection lost');}
					});
				}
			})
			
			
			function is_user(email, password)
			{
				var ret_val;
				$.ajax({
					url:"reg.php",
					type:"POST",
					data:{
						email:email,
						password:password
					},
					success:function(res){
						ret_val = res;
					},
					error: function(res){alert('Server connection lost');}
				});
				return ret_val;
			}
			
			
			//BUTTON CLICK
            $(".forms button").not('.btn-inverse').click(function(){
				
                var form = $(".sendForm")
                var obj  = $(this)
                var next = $("."+obj.attr("next"))
				
				if (obj.attr("variables") != undefined){
                        
                    //	alert(1 + obj.parents("div .forms").serialize())
				}
                if (obj.attr("attribute") != undefined || obj.attr("value") != undefined){
					$("dt#" + obj.attr("attribute")).remove()
					$("dd#" + obj.attr("attribute")).remove()
					$("input[type=hidden]#" + obj.attr("attribute")).remove()
					
					form.append("<dt id='"+ obj.attr("attribute") +"'>" + obj.attr("formName") + "</dt><dd id='"+ obj.attr("attribute")+"'>" + obj.text() + "</dd>")
					form.append("<input type='hidden' name='" + obj.attr("attribute") + "' value='" + obj.text() + "'>");
                }
                obj.parents("div .forms").hide("slider")
                next.show("slider")
                return false
            });
			
			// SELECT OPTION CHANGE EVENT
            /*$(".forms select").change(function(){
                alert("select");
				var form = $(".sendForm");
                var obj  = $(this);
				
                if (obj.attr("id") != undefined){
                    console.log("girya");
					
					//alert("dt#" + obj.attr("id"));
					$("dt#"+obj.attr("id")).remove();
					$("dd#"+obj.attr("id")).remove();
					$("input[type=hidden]#" + obj.attr("id")).remove()
					
					//$("dd#" + obj.attr("id")).remove();
					if(obj.prev().is('input')){
						if(parseInt(obj.prev().val())){
							var meter = parseFloat($('option:selected', obj).attr('meter'));
							//alert(meter);
							form.append("<dt id='"+ obj.attr("id") +"'>" + obj.prev().prev().text() + "</dt><dd id='"+ obj.attr("id") +"'>" + obj.prev().val() + " " + obj.val() + "</dd>")
							form.append("<input type='hidden' name='" +  obj.attr("id") + "' value='" + parseFloat(obj.prev().val())*meter + "'>");
							return false;
						}else return false;
					}
					
					
					form.append("<dt id='"+ obj.attr("id") +"'>" + obj.prev().text() + "</dt><dd id='"+ obj.attr("id") +"'>" + obj.val() + "</dd>")
					form.append("<input type='hidden' name='" + obj.attr("id") + "' value='" + obj.val() + "'>");
                }
                console.log($(this).attr("id"))
            })
            
			*/
			// INPUT VALUE CHANGE EVENT
			$("input").change(function(){
				var form = $(".sendForm")
				var obj  = $(this)
				if(obj.next().is('select'))
				{
					$("dt#" + obj.next().attr("id")).remove()
					$("dd#" + obj.next().attr("id")).remove()
					$("input[type=hidden]#" + obj.next().attr("id")).remove()
					
					form.append("<dt id='"+ obj.next().attr("id") +"'>" + obj.prev().text() + "</dt><dd id='"+ obj.next().attr("id") +"'>" + obj.val() + " " + obj.next().val() + "</dd>")
					form.append("<input type='hidden' name='" + obj.next().attr("id") + "' value='" + obj.val() + "'>");
					return false;
				}
				if (obj.attr("name") != undefined || obj.attr("value") != ""){
					$("dt#" + obj.attr("name")).remove()
					$("dd#" + obj.attr("name")).remove()
					$("input[type=hidden]#" + obj.next().attr("name")).remove()

					form.append("<dt id='"+ obj.attr("name") +"'>" + obj.prev().text() + "</dt><dd id='"+ obj.attr("name")+"'>" + obj.val() + "</dd>")
					form.append("<input type='hidden' name='" + obj.attr("name") + "' value='" + obj.val() + "'>");
				}
			})
			
		
			
			$(".forms .maps").click(function(){
                googleMaps()
            })
			
            $(".selectpicker").selectpicker()
        });

		function sendForm()
		{
			$.ajax({
				url:"test.php",
				type:"POST",
				datatype:'json',
				data:$('.sendForm').serialize(),
				async:false,
				success:function(res){
					alert(res);
				},
				error: function(res){alert('Some error accured');}
			}); 
		}

		function aliii(){
			alert("yest znacheniya!!!")
		}

        function googleMaps( $form ){
            $("#maps").modal('show');
            loadScript();
            $("#mapAdres").val( "Turkmenistan Ashgabat" );
			
            $("#mapSave").on("click", function(){
                
				var form = $(".sendForm");
				var label_text = $(".maps").prevAll("label:first").text();
				var id = 'map_coordinates';
				
				$("dt#"+id).remove();
				$("dd#"+id).remove();
				$("input[type=hidden]#" + id).remove();
				
				form.append("<dt id='"+ id +"'>" + label_text + "</dt><dd id='"+ id +"'>" + $("#mapCoordinates").text() + "</dd>")
				form.append("<input type='hidden' name='" + id + "' value='" + $("#mapCoordinates").text() + "'>");

				$("#maps").modal('hide');
				$(".maps").val( $("#mapCoordinates").text())
            });
        }

        function initialize() {
            var geocoder;
            var address = $("#mapAdres").val();
            geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    initMaps( results[0].geometry.location + '' );
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        function initMaps( geoAddress ){

            geoAddress = geoAddress.substring( 1 , geoAddress.length -1 );
            $("#mapCoordinates").text( geoAddress );

            var latlngStr = geoAddress.split(',',2);
            var lat = parseFloat(latlngStr[0]);
            var lng = parseFloat(latlngStr[1]);
            
            var latlng = new google.maps.LatLng( lat, lng );
            var mapOptions = {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            map = new google.maps.Map(document.getElementById('maps_modal'), mapOptions);

            var marker = new google.maps.Marker({
                position: latlng,
                title: 'Точка',
                map: map,
                draggable: true
            });

            google.maps.event.addListener(marker, 'drag', function() {
                $("#mapCoordinates").text(marker.getPosition());
            });
        }

        function loadScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' +
                'callback=initialize';
            document.body.appendChild(script);
        }
