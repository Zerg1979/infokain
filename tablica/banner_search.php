﻿<?php 
/*
YOUR USER-ID: infokain
YOUR API-KEY: wzmHLiZBaFjc1ZB0bkWyQQ1gzAGT3FMG 
*/

//header('Content-type: text/html; charset=utf-8');
//include ("search_data.php");

// echo "go";

function getJSONData() {
	$output = array("aaData" => array());
	
  for($i=1; $i<100; $i++)
		array_push($output['aaData'], array( 
      'country' => getCountry(), 
      'city' => getCity(),
      'currency' => getCurrency(),
      'adtype' => getAdType(), 
      'type' => getAutoType(),
      'style' => getStyle(),
      'marka' => getBrand(), 
      'model' => getModel(),
      'doors' => getDoors(),
      'privod' => getWheel(),
      'toplivo' => getToplivo(), 
      'price' => getPrice(),
      'date' => "03.09.2013",
      'ad_id' => $i,
      'image' => array(getImage(),getImage(),getImage(),getImage()),
      'banner_image' => getImage(),
      'colored' => "0",
      'map' => '<a onclick="showOnMap('+'37.0625,-95.677068'+')">На карте</a>'
   ));
	
	return $output;
}

function normJsonStr($str){
    $str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
    return $str;
//	return $str;
}

// izmenit' kodirovku
function changeEncoding($str){
	return iconv('cp1251', 'utf-8', $str);
}

echo changeEncoding(normJsonStr(json_encode(getJSONData())));

function getCountry(){ $array = array('Россия','Украина','Франция','Казахстан','Англия','Италия','Марокко');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getCity(){ $array = array('Миллан','Киев','Москва','Париж','Лондон','Москва','Берлин');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getCurrency(){ $array = array('Евро','Доллар','Рубль','Юань');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getAdType(){ $array = array('Продажа','Покупка','Аренда','Лизинг');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getAutoType(){ $array = array('Легковой','Грузовой','Строительный','Спецтехника');
  return $array[ mt_rand(0, count($array)-1) ]; }
 
function getStyle(){ $array = array('Седан','Хетчбек','Внедорожник','Кроссовер');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getBrand(){ $array = array('Audi','Mersedes','Mini Cooper','Rang Rover');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getModel(){ $array = array('R8','A110','CC190','Evogue');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getDoors(){ $array = array('2','3','4','5');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getWheel(){ $array = array('FWD','RWD','4X4','4X6');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getToplivo(){ $array = array('Бензин','Дизель','Газ','Электро');
  return $array[ mt_rand(0, count($array)-1) ]; }
  
function getPrice(){
  return mt_rand(10, 50) * 500; }
  
function getImage() {
  return 'image.php?color='.mt_rand(0, 360); }



/*
titles:{
  country:"Страна", // Обычное поле это просто строковый параметр
  city:"Город",
  doorCount:{       // Если нужно численное поле то параметр объект
    value:"Кол-во дверей",
    isNumber:1  // (поиск по диапазону) то указывается так 
  },
  mass:{            
    value:"Масса",
    meas:meas:[    // Единицы измерений указываем так
      {name:"км"}, // Нулевой элемент массива это то что будет в таблице по умолчанию
      {name:"миль",multiplier:0.76} // Для других единиц указываем коэффициент
    ]
  },
  mileage:{
    value:"Пробег",
    isNumber:1,       // В отличие от предыдущего поля здесь будет и возможность перевода единиц и поиск по диапазону
    meas:[
      {name:"км"},
      {name:"миль",multiplier:0.76}
    ]
}

*/

?>