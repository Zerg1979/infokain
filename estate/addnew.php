<?php include('../includes/langdetect.php'); ?>
<!DOCtype html> 
<html> 
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>INFOKAIN.COM</title>

    <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen" />   
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="../css/bootstrap-select.css" type="text/css" media="screen" />
  <!--  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->
	
</head>
<body>
	<?php include('../includes/info.php'); ?>

<center><h2><p><?php echo $l['estate']; ?></p></h2>
<div class="part_border_1 est_color"></div>
		<?php include('buttons2.php'); ?>
<p><div class="part_border_1 est_color"></div></p>
</center>
<button id="otpravka">OTPRAVIT FORMU</button>

<!-- ZDES NACHINAYETSYA BOLSHAYA FORMA -->
<form class="sendFormEstate">
<input type='hidden' name='razdel' value='1'>
<div class="container">
    <!-- ВЫБЕРИТЕ СПОСОБ -->
	<div class="forms MainEstate">
	    <center><h5><?php echo $l['select_a_method']; ?></h5></center>
		<ul class="thumbnails">
			<li class="span6">
				<a class="thumbnail slider forInput" attribute="action" value="sell" next="EstateChooseType">
				<center><h5><?php echo $l['sell']; ?></h5></center>
				</a>
			</li>
			<li class="span6">
				<a class="thumbnail slider forInput" attribute="action" value="rent" next="EstateChooseRentWay">
				<center><h5><?php echo $l['rent']; ?></h5></center>
				</a>
			</li>
		</ul>
	</div>
    <!-- ВЫБЕРИТЕ СПОСОБ end -->

    <!-- ВЫБЕРИТЕ КАТЕГОРИЮ НЕДВИЖИМОСТИ -->
	<div class="forms EstateChooseType" style="display:none">
		<div>
			<center><h5><?php echo $l['select_a_catego']; ?></h5></center>
		</div>
		<ul class="thumbnails">
			<li class="span4">
				<a class="thumbnail slider forInput enabledElements" attribute="category" value="room" rel="tooltip" title="<?php echo $l['room_dis']; ?>" next="ROOM">
				<center><h5><?php echo $l['living_room']; ?></h5></center>
				</a>
			</li>
			<li class="span4">
				<a class="thumbnail slider forInput enabledElements" attribute="category" value="apartments" rel="tooltip" title="<?php echo $l['apartments']; ?>" next="APARTMENT">
				<center><h5><?php echo $l['residential_apa']; ?></h5></center>
				</a>
			</li>
			<li class="span4" >
				<a class="thumbnail slider forInput enabledElements" attribute="category" value="villa" rel="tooltip" title="<?php echo $l['villa']; ?>" next="HOUSE">
				<center><h5><?php echo $l['residential_hou']; ?></h5></center>
				</a>
			</li>
		</ul>
		<ul class="thumbnails">
			<li class="span4" >
				<a class="thumbnail slider forInput enabledElements" attribute="category" value="plantation" rel="tooltip"title="<?php echo $l['plantation']; ?>" next="PLOT_OF_LAND">
				<center><h5><?php echo $l['plot_of_land']; ?></h5></center>
				</a>
			</li>
			<li class="span4" >
				<a class="thumbnail slider forInput enabledElements" attribute="category" value="OFFICE" rel="tooltip"title="<?php echo $l['office']; ?>" next="BUSINESS_PREMIS">
				<center><h5><?php echo $l['business_premis']; ?></h5></center>
				</a>
			</li>
			<li class="span4" >
				<a class="thumbnail slider forInput enabledElements" attribute="category" value="HOTEL" rel="tooltip"title="<?php echo $l['hotel']; ?>" next="BUILDING_FOR_BU">
				<center><h5><?php echo $l['building_for_bu']; ?></h5></center>
				</a>
			</li>
		</ul>
		<ul class="pager">
		  <li class="previous">
			<a class="slider" next="MainEstate">&larr; <?php echo $l['go_back']; ?></a>
		  </li>
		</ul>
	</div>
    <!-- ВЫБЕРИТЕ КАТЕГОРИЮ НЕДВИЖИМОСТИ END -->

    <!-- ЗА КАКОЙ ПЕРИОД ВЫ УКАЖИТЕ ЦЕНУ -->
	<div class="forms EstateChooseRentWay" style="display:none">
		<div>
			<center><h5><?php echo $l['over_what_perio']; ?><i  rel="tooltip" data-placement="botton" title="<?php echo $l['after_filling_o']; ?>" class="icon-question-sign"></i></h5></center>
		</div>
		<ul class="thumbnails">
			<li class="span4">
				<a class="thumbnail slider forInput" attribute="period" value="one_hour_stay" next="EstateChooseRentTime">
				<center><h5><?php echo $l['one_hour_stay']; ?></h5></center>
				</a>
			</li>
			<li class="span4">
				<a class="thumbnail slider forInput" attribute="period" value="per_night" next="EstateChooseRentTime">
				<center><h5><?php echo $l['per_night']; ?></h5></center>
				</a>
			</li>
			<li class="span4">
				<a class="thumbnail slider forInput" attribute="period" value="one_month_stay" next="EstateChooseRentTime">
				<center><h5><?php echo $l['one_month_stay']; ?></h5></center>
				</a>
			</li>
		</ul>
	</div>
    <!-- ЗА КАКОЙ ПЕРИОД ВЫ УКАЖИТЕ ЦЕНУ END -->

    <!-- ЗА КАКОЙ ПЕРИОД ВЫ ЖЕЛАЕТЕ ПОЛУЧИТЬ ПРЕДОПЛАТУ -->
	<div class="forms EstateChooseRentTime" style="display:none">
		<div>
			<center><h5><?php echo $l['the_period_for']; ?>  <i  rel="tooltip" data-placement="botton" title="<?php echo $l['the_number_of_t']; ?>" class="icon-question-sign"></i></h5></center>
		</div>
		<ul class="thumbnails">
			<li class="span4">
				<a class="thumbnail slider forInput" attribute="predopl" value="not_required" next="EstateChooseType">
    				<center><h5><?php echo $l['not_required']; ?></h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail" class="disabled">
				<center><h5><p class="muted"><?php echo $l['or']; ?></p></h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="1" next="EstateChooseType">
				<center><h5>1</h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="2" next="EstateChooseType">
				<center><h5>2</h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="3" next="EstateChooseType">
				<center><h5>3</h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="4" next="EstateChooseType">
				<center><h5>4</h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="5" next="EstateChooseType">
				<center><h5>5</h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="6" next="EstateChooseType">
				<center><h5>6</h5></center>
				</a>
			</li>
			<li class="span1">
				<a class="thumbnail slider forInput" attribute="predopl" value="12" next="EstateChooseType">
				<center><h5>12</h5></center>
				</a>
			</li>
		</ul>
	</div>
    <!-- ЗА КАКОЙ ПЕРИОД ВЫ ЖЕЛАЕТЕ ПОЛУЧИТЬ ПРЕДОПЛАТУ END -->

    <!-- КОМНАТА -->
	<div class="forms ROOM" style="display:none;">
		<div class="row">
				<center><h5><?php echo $l['room']; ?></h5></center>
		</div>
		<div class="row">
			<div class="span3">
			   <label><?php echo $l['type']; ?>* <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
					<select disabled class="selectpicker" data-style="btn-inverse" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
						<?php
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/room?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );
							foreach( $data as $it )
							{
								echo "<option>".$it."</option>";
							}
						?>
					</select>
					<?php include('estate_include/owner.php'); ?>
			</div>
			<div class="span2">
			    <label><?php echo $l['rooms']; ?>*</label>
                    <input disabled class="span2" type="number" name="attributes[rooms]" placeholder="1">
			    <label><?php echo $l['bedrooms']; ?></label>
					<input disabled class="span2" type="number" name="attributes[bedrooms]" placeholder="1">
			</div>
			<div class="span2">
				<label><?php echo $l['floor']; ?>*</label>
					<input disabled class="span2" type="number" name="attributes[floor]" placeholder="1">
				<label><?php echo $l['floors']; ?></label>
					<input disabled class="span2" type="number" name="attributes[floors]" placeholder="25">
			</div>
			<div class="span3">
				<?php include('estate_include/ceiling_height.php'); ?>
				<?php include('estate_include/total_area.php'); ?>
			</div>
			<div class="span2">
				<?php include('estate_include/type_of_constru.php'); ?>
				<?php include('estate_include/ownership.php'); ?>
			</div>
		</div>
		<ul class="pager">
		  <li class="previous">
			<a class="slider" next="EstateChooseType">&larr; <?php echo $l['go_back']; ?></a>		  </li>
		  <li class="next" >
			<a class="slider checkElements enabledElements" next="Dopinfo"><?php echo $l['attention_after']; ?> &rarr;</a>
		  </li>
		</ul>
	</div>
    <!-- КОМНАТА END -->
    <!-- Квартира -->
	<div class="forms APARTMENT" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['apartment']; ?></p></h5></center>
				</div>
			</div>
		</div>
		<p>
		<div class="row">
			<div class="span3">
			   <label><?php echo $l['type_of_apartme']; ?>* <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
					<select disabled class="selectpicker" data-style="btn-inverse" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
					<?php
								  
						include_once ("../lib/Catalog.php");
						$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/Apartments?lang='.$_COOKIE['lang'], null);
						$data = json_decode( $data, true );
						foreach( $data as $it )
						{
							echo "<option>".$it."</option>";
						}
					?>
					</select>
				<?php include('estate_include/owner.php'); ?>
			</div>
			<div class="span2">
			    <label><?php echo $l['rooms']; ?>*</label>
                    <input disabled class="span2" type="number" name="attributes[rooms]" placeholder="1">
			    <label><?php echo $l['bedrooms']; ?></label>
					<input disabled class="span2" type="number" name="attributes[bedrooms]" placeholder="1">
			</div>
			<div class="span2">
				<label><?php echo $l['floor']; ?>*</label>
					<input disabled class="span2" type="number" name="attributes[floor]" placeholder="1">
				<label><?php echo $l['floors']; ?></label>
					<input disabled class="span2" type="number" name="attributes[floors]" placeholder="25">
			</div>
			<div class="span3">
				<?php include('estate_include/ceiling_height.php'); ?>
				<?php include('estate_include/total_area.php'); ?>
			</div>
			<div class="span2">
				<?php include('estate_include/type_of_constru.php'); ?>
				<?php include('estate_include/ownership.php'); ?>
			</div>
		</div>
		</p>
		<ul class="pager">
		  <li class="previous">
			<a class="slider" next="EstateChooseType">&larr; <?php echo $l['go_back']; ?></a>		  </li>
		  <li class="next">
			<a class="slider checkElements enabledElements" next="Dopinfo"><?php echo $l['attention_after']; ?> &rarr;</a>
		  </li>
		</ul>
	</div>
    <!-- Квартира end -->
    <!-- Дом -->
	<div class="forms HOUSE" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['house']; ?> </p></h5></center>
				</div>
			</div>
		</div>
		<p>
			<div class="row">
				<div class="span3">
				   <label><?php echo $l['view_home']; ?>* <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
						<select disabled class="selectpicker" data-style="btn-inverse" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
							<?php
										  
								include_once ("../lib/Catalog.php");
								$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/Villa?lang='.$_COOKIE['lang'], null);
								$data = json_decode( $data, true );
								foreach( $data as $it )
								{
									echo "<option>".$it."</option>";
								}
								 
							?>
						</select>
					<?php include('estate_include/owner.php'); ?>
			</div>
			<div class="span2">
			    <label><?php echo $l['rooms']; ?>*</label>
                    <input disabled class="span2" type="number" name="attributes[rooms]" placeholder="1">
			    <label><?php echo $l['bedrooms']; ?></label>
					<input disabled class="span2" type="number" name="attributes[bedrooms]" placeholder="1">
			</div>
			<div class="span2">
				<label><?php echo $l['floors']; ?>*</label>
					<input disabled class="span2" type="number" name="attributes[floors]" placeholder="1">
			</div>
			<div class="span3">
				<?php include('estate_include/ceiling_height.php'); ?>
				<?php include('estate_include/total_area.php'); ?>
			</div>
			<div class="span2">
				<?php include('estate_include/type_of_constru.php'); ?>
				<?php include('estate_include/ownership.php'); ?>
			</div>
		</div>
		</p>
		<ul class="pager">
		  <li class="previous">
			<a class="slider" next="EstateChooseType">&larr; <?php echo $l['go_back']; ?></a>		  </li>
		  <li class="next">
			<a class="slider checkElements enabledElements" next="Dopinfo"><?php echo $l['attention_after']; ?> &rarr;</a>
		  </li>
		</ul>
	</div>
    <!-- Дом end -->
    <!-- Участок end -->
	<div class="forms PLOT_OF_LAND" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['plot_of_land']; ?></p></h5></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['describe_the_op']; ?></p></h5></center>
				</div>
			</div>
		</div>
		<p>
		</p>
		<div class="row">
			<div class="span3">
			   <label><?php echo $l['view_site']; ?>* <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
					<select disabled class="selectpicker" data-style="btn-inverse" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
					<?php
			  
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/Plantation?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );
							foreach( $data as $it )
							{
								echo "<option>".$it."</option>";
							}
					
					?>
				</select>
			</div>
			<div class="span3">
				<label><?php echo $l['owner']; ?> <i  rel="tooltip" title="<?php echo $l['what_are_you_th']; ?>" class="icon-question-sign"></i></label>
				<select disabled class="selectpicker span3" data-style="btn-inverse" data-container="body" name="attributes[owner]">
					<option></option>
					<option value="state"><?php echo $l['state']; ?></option>
					<option value="private person"><?php echo $l['private person']; ?></option>
				</select>
			</div>
			<div class="span2">
				<label><?php echo $l['ownership']; ?></label>
                   <select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" name="attributes[ownership]">
						<option></option>
						<option value="bought"><?php echo $l['bought']; ?></option>
						<option value="heritage"><?php echo $l['heritage']; ?></option>
						<option value="donation"><?php echo $l['donation']; ?></option>
                    </select>
			</div>
			<div class="span4">
				<?php include('estate_include/area.php'); ?>
			</div>
		</div>
		<p><div class="part_border_1 est_color"></div></p>
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['describe_the_structure']; ?> (<?php echo $l['only_if_there_i']; ?>)</p></h5></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<?php include('estate_include/type_of_constru_span3.php'); ?>
			</div>
			<div class="span3">
			   <label><?php echo $l['number_of_build']; ?>*</label>
					<input disabled class="span3" type="number" name="number_of_build" placeholder="1">
			</div>
			<div class="span2">
				<label><?php echo $l['floors']; ?></label>
					<input disabled class="span2" type="number" name="attributes[floors]" placeholder="25">
			</div>
			<div class="span4">
				<?php include('estate_include/total_area.php'); ?>
			</div>
		</div>
		<ul class="pager">
			<li class="previous">
				<a class="slider" next="EstateChooseType">&larr; <?php echo $l['go_back']; ?></a>			</li>
			<li class="next">
				<a class="slider checkElements enabledElements" next="Dopinfo"><?php echo $l['attention_after']; ?> &rarr;</a>
			</li>
		</ul>
	</div>
    <!-- Участок end -->
    <!-- Бизнес-->
	<div class="forms BUSINESS_PREMIS" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['business_premis']; ?></p></h5></center>
				</div>
			</div>
		</div>
		<p>
		<div class="row">
			<div class="span3">
			   <label><?php echo $l['type_of_accommo']; ?>* <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
					<select disabled class="selectpicker" data-style="btn-inverse" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
					<?php
			 
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/Office?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );

							foreach( $data as $it )
							{
							echo "<option>".$it."</option>";
							}
               	?>
				</select>
				<label><?php echo $l['type_of_busines']; ?> <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
				<select disabled class="selectpicker span3 dropup" data-style="btn-inverse" data-live-search="true"  data-container="body" id="attributes_type_of_busines" name="attributes[type_of_busines]">
					<?php
			   
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'category/goods?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );
							


							foreach( $data as $it )
							{
							echo "<option>".$it."</option>";
							}
                
?>
				</select>
			</div>
			<div class="span2">
				<label><?php echo $l['owner']; ?></label>
					<select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" name="attributes[owner]">
						<option></option>
						<option value="state"><?php echo $l['state']; ?></option>
						<option value="private person"><?php echo $l['private person']; ?></option>
					</select>
				<?php include('estate_include/type_of_constru.php'); ?>
			</div>
			<div class="span2">
				<?php include('../includes/All_add_new_include/mode_of_operation.php'); ?>
			    <label><?php echo $l['rooms']; ?>*</label>
                    <input disabled class="span2" type="number" name="attributes[rooms]" placeholder="1">
			</div>
			<div class="span2">
				<label><?php echo $l['floor']; ?>*</label>
					<input disabled class="span2" type="number" name="attributes[floor]" placeholder="1">
				<label><?php echo $l['floors']; ?></label>
					<input disabled class="span2" type="number" name="attributes[floors]" placeholder="25">
			</div>
			<div class="span3">
				<?php include('estate_include/ceiling_height.php'); ?>
				<?php include('estate_include/total_area.php'); ?>
			</div>
		</div>
		</p>
		<ul class="pager">
			<li class="previous">
				<a class="slider" next="EstateChooseType">&larr; <?php echo $l['go_back']; ?></a>			</li>
			<li class="next">
				<a class="slider checkElements enabledElements" next="Dopinfo"><?php echo $l['attention_after']; ?> &rarr;</a>
			</li>
		</ul>
	</div>
    <!-- Бизнес end -->
   <!-- Строение-->
	<div class="forms BUILDING_FOR_BU" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['building_for_bu']; ?></p></h5></center>
				</div>
			</div>
		</div>
		<p>
		<div class="row">
			<div class="span3">
			   <label><?php echo $l['type_of_structu']; ?>* <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
					<select disabled class="selectpicker" data-style="btn-inverse" data-live-search="true" data-container="body" data-size="auto" name="attributes[type]">
					<?php
			  
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/Hotel?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );
							


							foreach( $data as $it )
							{
							echo "<option>".$it."</option>";
							}
               	?>
				</select>
				<label><?php echo $l['type_of_busines']; ?> <i class="icon-exclamation-sign" rel="tooltip"  data-placement="right" title="<?php echo $l['add_value_new']; ?>"></i></label>
				<select disabled class="selectpicker span3 dropup" data-style="btn-inverse" data-live-search="true"  data-container="body" id="attributes_type_of_busines" name="attributes[type_of_busines]">
					<?php
			   
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'category/goods?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );
							


							foreach( $data as $it )
							{
							echo "<option>".$it."</option>";
							}
                	?>
				</select>
			</div>
			<div class="span3">
				<label><?php echo $l['owner']; ?></label>
				<select disabled class="selectpicker span3" data-style="btn-inverse" data-container="body" name="attributes[owner]">
					<option></option>
					<option value="state"><?php echo $l['state']; ?></option>
					<option value="private person"><?php echo $l['private person']; ?></option>
				</select>
			   <label><?php echo $l['number_of_premises']; ?>*</label>
				<input disabled class="span3 inform" type="number" formname="number_of_premises" name="number_of_premises" placeholder="1">
			</div>
			<div class="span3">
				<label><?php echo $l['type_of_constru']; ?></label>
				<select disabled class="selectpicker span3" data-style="btn-inverse" data-container="body" id="attributes_type_of_construct"  name="attributes[type_of_construct]">
						<option></option>
						<option value="modular"><?php echo $l['modular']; ?></option>
						<option value="monolithic"><?php echo $l['monolithic']; ?></option>
						<option value="frame"><?php echo $l['frame']; ?></option>
						<option value="brick"><?php echo $l['brick']; ?></option>
						<option value="panel"><?php echo $l['panel']; ?></option>
						<option value="metal construction"><?php echo $l['metal construction']; ?></option>
				</select>
				<label><?php echo $l['floors']; ?></label>
				<input disabled class="span3 inform" type="number" formname="floors" name="floors" placeholder="25">
			</div>
			<div class="span3">
				<label><?php echo $l['ceiling_height']; ?></label>
					  <input disabled class="span2 inform" formname="ceiling_height" name="ceiling_height" size="16" type="number">
							<select disabled class="selectpicker span1" data-style="btn-inverse" data-container="body"  id="attributes_ceiling_height_units" name="attributes[ceiling_height_units]">
								<option meter='1' data-subtext="<?php echo $l['meter']; ?>"><?php echo $l['m']; ?></option>
								<option meter='0.9144' data-subtext="<?php echo $l['yard']; ?>"><?php echo $l['yd']; ?></option>
								<option meter='0.3048' data-subtext="<?php echo $l['foot']; ?>"><?php echo $l['ft']; ?></option>
								<option meter='0.3333' data-subtext="<?php echo $l['syaku_japan']; ?>"><?php echo $l['syaku']; ?></option>
								<option meter='0.3333' data-subtext="(<?php echo $l['souk_thailand']; ?>) <?php echo $l['souk']; ?>"><?php echo $l['souk']; ?></option>
								<option meter='0.3333' data-subtext="(<?php echo $l['china_chi']; ?>) <?php echo $l['chi']; ?>"><?php echo $l['chi']; ?></option>
							</select>
				<label><?php echo $l['total_area']; ?></label>
					  <input disabled class="span2 inform" formname="total_area" name="total_area" size="16" type="number">
						<select disabled class="selectpicker span1" data-style="btn-inverse" data-container="body"  id="attributes_total_area" name="attributes[total_area]">
							<option meter='1' data-subtext="<?php echo $l['square_meter']; ?>"><p><?php echo $l['m']; ?><sup>2</sup></p></option>
							<option meter='0.836127' data-subtext="<?php echo $l['square_yard']; ?>"><p><?php echo $l['yd']; ?><sup>2</sup></p></option>
							<option meter='0.092903' data-subtext="<?php echo $l['square_foot']; ?>"><p><?php echo $l['ft']; ?><sup>2</sup></p></option>
						</select>
					<p></p>
			</div>
		</div>
		</p>
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['adjacent_territ']; ?> (<?php echo $l['only_if_there_i']; ?>)</p></h5></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span6">
			   <label><?php echo $l['view_site']; ?></label>
				<select disabled class="selectpicker span6" data-style="btn-inverse" data-live-search="true"  data-container="body" id="attributes_type_of_construct"  name="attributes[type_of_construct]">
					<?php
			   
							include_once ("../lib/Catalog.php");
							$data = Curl::makeHttpCall( 'http://cat.infokain.com', 80, 'HTTP_GET', 'type/Plantation?lang='.$_COOKIE['lang'], null);
							$data = json_decode( $data, true );
							


							foreach( $data as $it )
							{
							echo "<option>".$it."</option>";
							}
               	?>
				</select>
			</div>
			<div class="span6">
				<?php include('estate_include/total_area.php'); ?>
			</div>
		</div>
		<ul class="pager">
		  <li class="previous">
			<a class="slider" next="EstateChooseType">&larr; <?php echo $l['go_back']; ?></a>		  </li>
		  <li class="next">
			<a class="slider checkElements enabledElements" next="Dopinfo"><?php echo $l['attention_after']; ?> &rarr;</a>
		  </li>
		</ul>
	</div>
    <!-- edilmeik end-->
    <!-- ВЫБЕРИТЕ ГАЛОЧКАМИ ПОХОДЯЩИЕ ПАРАМЕТРЫ ВАШЕЙ НЕДВИЖИМОСТИ  seretmeli -->
	<div class="forms Dopinfo" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['select_suitable']; ?></p></h5></center>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span3">
				<legend><h5><?php echo $l['electricity']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple="multiple" id="attributes_electricity" name="attributes[electricity][]" data-container="body">
							<option value="2 phase"><?php echo $l['2 phase']; ?></option>
							<option value="3 phase"><?php echo $l['3 phase']; ?></option>
							<option value="diesel generator"><?php echo $l['diesel generator']; ?></option>
							<option value="own substation"><?php echo $l['own substation']; ?></option>
						</select>
				<legend><h5><?php echo $l['water_est']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_water" name="attributes[water][]" data-container="body">
							<option value="constantly"><?php echo $l['constantly']; ?></option>
							<option value="hot"><?php echo $l['hot']; ?></option>
							<option value="modal"><?php echo $l['modal']; ?></option>
							<option value="technical"><?php echo $l['technical']; ?></option>
						</select>
			</div>
			<div class="span3">
				<legend><h5><?php echo $l['gas']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_gas" name="attributes[gas][]" data-container="body">
							<option value="permanent"><?php echo $l['permanent']; ?></option>
							<option value="in cylinders"><?php echo $l['in cylinders']; ?></option>
							<option value="natural"><?php echo $l['natural']; ?></option>
							<option value="liquefied"><?php echo $l['liquefied']; ?></option>
						</select>
				<legend><h5><?php echo $l['elevator']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_elevator" name="attributes[elevator][]" data-container="body">
							<option value="passenger"><?php echo $l['passenger']; ?></option>
							<option value="cargo"><?php echo $l['cargo']; ?></option>
							<option value="external"><?php echo $l['external']; ?></option>
							<option value="escalator"><?php echo $l['escalator']; ?></option>
						</select>
			</div>
			<div class="span3">
				<legend><h5><?php echo $l['a_place_for_tra']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_a_place_for_tra" name="attributes[a_place_for_tra][]" data-container="body">
							<option value="parking on the street"><?php echo $l['parking on the street']; ?></option>
							<option value="underground parking"><?php echo $l['underground parking']; ?></option>
							<option value="garage"><?php echo $l['garage']; ?></option>
						</select>
				<legend><h5><?php echo $l['security']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_security" name="attributes[security][]" data-container="body">
							<option value="wachter"><?php echo $l['wachter']; ?></option>
							<option value="fenced area"><?php echo $l['fenced area']; ?></option>
							<option value="secured area"><?php echo $l['secured area']; ?></option>
							<option value="intercom"><?php echo $l['intercom']; ?></option>
						</select>
			</div>
			<div class="span3">
				<legend><h5><?php echo $l['locations']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_locations" name="attributes[locations][]" data-container="body">
							<option value="sleeping area"><?php echo $l['sleeping area']; ?></option>
							<option value="near shopping center"><?php echo $l['near shopping center']; ?></option>
							<option value="close park area"><?php echo $l['close park area']; ?></option>
							<option value="near school"><?php echo $l['near school']; ?></option>
							<option value="near a kindergarten"><?php echo $l['near a kindergarten']; ?></option>
						</select>
				<legend><h5><?php echo $l['more']; ?></h5></legend>
						<select disabled class="selectpicker span3" data-style="btn-inverse" multiple id="attributes_more" name="attributes[more][]" data-container="body">
							<option value="internet"><?php echo $l['internet']; ?></option>
							<option value="a view of the sea"><?php echo $l['a view of the sea']; ?></option>
							<option value="good neighbors"><?php echo $l['good neighbors']; ?></option>
						</select>
			</div>
		</div>
	<ul class="pager">
			<li class="next">
				<a class="slider checkElements enabledElements" next="MY_AGENCY_IS_NO_LOC"> <?php echo $l['further']; ?> &rarr;</a>
			</li>
		</ul>
	</div>
    <!-- ВЫБЕРИТЕ ГАЛОЧКАМИ ПОХОДЯЩИЕ ПАРАМЕТРЫ ВАШЕЙ НЕДВИЖИМОСТИ end seretmeli-->
    <!-- МЕСТОПОЛОЖЕНИЕ НЕДВИЖИМОСТИ -->
	<div class="forms MY_AGENCY_IS_NO_LOC" style="display:none;">
		<div class="container">
			<div class="span12">
					<center><h5><?php echo $l['property_locati']; ?></h5></center>
			</div>
			<div class="row">
				<div class="span3">
					<label><h6><?php echo $l['country']; ?>*</h6></label>
                       <select disabled class="selectpicker span3 dependCatalogsLoad" id="countries" parentcatalog="cities" data-style="btn-inverse"  data-live-search="true"  data-container="body" name="attributes[countries]">
							<?php
							include_once ("../lib/Catalog.php");
											$data = Curl::makeHttpCall('http://cat.infokain.com', 80, 'HTTP_GET', 'countries?lang='.$lang, null);
											$data = json_decode($data, true);
											foreach($data as $index=>$en_val)
											{
												echo "<option value='$index'>".$en_val."</option>";
											}
							?>
                       </select>
					<label><h6><?php echo $l['city']; ?>*</h6></label>
					<select disabled class="selectpicker span3 dependCatalogsLoad" id="cities" parentcatalog="district metro street" data-style="btn-inverse"  data-live-search="true"  data-container="body" name="attributes[city]">
					</select>
				</div>
				<div class="span3">
					<label><h6><p><?php echo $l['district']; ?>*</p></h6></label>
						<select disabled class="selectpicker span3 dependCatalogsLoad" id="district" data-style="btn-inverse"  data-live-search="true"  data-container="body" name="attributes[district]">
						</select>
					<label><h6><p><?php echo $l['metro']; ?></p></h6></label>
						<select disabled class="selectpicker span3 dependCatalogsLoad" id="metro" data-style="btn-inverse"  data-live-search="true"  data-container="body" name="attributes[metro]">
						</select>
				</div>
				<div class="span3">
					<label><h6><?php echo $l['street']; ?>*</h6></label>
						<select disabled class="selectpicker span3 dependCatalogsLoad" id="street" data-style="btn-inverse"  data-live-search="true"  data-container="body" name="attributes[street]">
						</select>
					<label><h6><?php echo $l['house_number']; ?></h6></label>
						<input disabled class="span3" type="number" name="attributes[house_number]" placeholder="64/2">
				</div>
				<div class="span3">
					<label><h6><?php echo $l['refine_on_the_m']; ?>*</h6></label>
						<input disabled type="text" class="span3 maps" name="attributes[maps]" placeholder="<?php echo $l['map']; ?>">
			<label><h6><?php echo $l['price']; ?></h6></label>
				<?php include('../includes/All_add_new_include/price.php'); ?>
				</div>
			</div>
			<ul class="pager">
				<li class="next">
					<a class="slider checkElements enabledElements" next="FOTO"><?php echo $l['add_photos']; ?> &rarr;</a>
				</li>
			</ul>
		</div>
	</div>
    <!-- МЕСТОПОЛОЖЕНИЕ НЕДВИЖИМОСТИ end -->
    <!-- ФОТОГРАФИИ -->
	<div class="forms FOTO" style="display:none;">
		<div class="row">
			<div class="span12">
				<div>
					<center><p><h5><?php echo $l['your_ad_is_succ']; ?></p></h5></center>
				</div>
			</div>
			<div class="row text-center">
				<div class="span3">
                    <div name="userpic" class="upload">
                        <div id="preview" class="upload__preview"></div>
                        <div class="upload__link">
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Browse pic"><br/>
                                <input disabled class="upload-link__inp" style="width:90px;" name="photo1" type="file" accept=".jpg,.jpeg,.gif" />
                                <input disabled name="attributes[photo1]" type="hidden" />
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Uploud photo"> 
                                <div class="progress progress-info progress-striped">
                                    <div class="bar uploud-progress" style="width: 0%"></div>
                                </div>
                        </div>
                    </div>
				</div>
				<div class="span3">
                    <div name="userpic" class="upload">
                        <div id="preview" class="upload__preview"></div>
                        <div class="upload__link">
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Browse pic"><br/>
                                <input disabled class="upload-link__inp" style="width:90px;" name="photo1" type="file" accept=".jpg,.jpeg,.gif" />
                                <input disabled name="attributes[photo2]" type="hidden" />
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Uploud photo"> 
                                <div class="progress progress-info progress-striped">
                                    <div class="bar uploud-progress" style="width: 0%"></div>
                                </div>
                        </div>
                    </div>
				</div>
				<div class="span3">
                    <div name="userpic" class="upload">
                        <div id="preview" class="upload__preview"></div>
                        <div class="upload__link">
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Browse pic"><br/>
                                <input disabled class="upload-link__inp" style="width:90px;" name="photo1" type="file" accept=".jpg,.jpeg,.gif" />
                                <input disabled name="attributes[photo2]" type="hidden" />
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Uploud photo"> 
                                <div class="progress progress-info progress-striped">
                                    <div class="bar uploud-progress" style="width: 0%"></div>
                                </div>
                        </div>
                    </div>
				</div>
				<div class="span3">
                    <div name="userpic" class="upload">
                        <div id="preview" class="upload__preview"></div>
                        <div class="upload__link">
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Browse pic"><br/>
                                <input disabled class="upload-link__inp" style="width:90px;" name="photo1" type="file" accept=".jpg,.jpeg,.gif" />
                                <input disabled name="attributes[photo2]" type="hidden" />
                                <input disabled class="upload-link__btn btn btn-small" type="button" value="Uploud photo"> 
                                <div class="progress progress-info progress-striped">
                                    <div class="bar uploud-progress" style="width: 0%"></div>
                                </div>
                        </div>
                    </div>
				</div>
			</div>
            <br />
		</div>
			<ul class="pager">
				<li class="next">
					<a class="slider checkElements enabledElements" next="WHO_POST__THE_A"><?php echo $l['everything_is_r']; ?> &rarr;</a>
				</li>
			</ul>
	</div>
    <!-- ФОТОГРАФИИ -->
    <!-- КТО ПОДАЕТ ОБЪЯВЛЕНИЕ -->
	<div class="forms WHO_POST__THE_A" style="display:none;">
	<div class="row">
		<div class="span12">
		    <center><h5><?php echo $l['who_post__the_a']; ?></h5></center>
		</div>
	</div>
		<ul class="thumbnails">
			<li class="span6">
				<a class="thumbnail slider enabledElements" attribute="who_post__the_a" value="private_person_big" next="PRIVATE_PERSON_BIG">
				<center><h5><?php echo $l['private_person_big']; ?></h5></center>
				</a>
			</li>
			<li class="span6">
				<a class="thumbnail slider enabledElements" attribute="who_post__the_a" value="real_estate_age" next="REAL_ESTATE_AGE">
				<center><h5><?php echo $l['real_estate_age']; ?> (<?php echo $l['realtor_dev']; ?>)</h5></center>
				</a>
			</li>
		</ul>
	</div>
    <!-- КТО ПОДАЕТ ОБЪЯВЛЕНИЕ end -->
    <!-- КОНТАКТЫ -->
	<div class="forms PRIVATE_PERSON_BIG" style="display:none;">
	<div class="row">
		<div class="span12">
				<center><h5><?php echo $l['contacts']; ?></h5></center>
		</div>
	</div>
	<div class="row">
		<div class="span6">
			<label><h6><?php echo $l['cell_phones']; ?>*</h6></label>
				<input disabled type="text" class="span6" name="attributes[cell_phones]" placeholder="+*(***)***-**-**, +*(***)***-**-**,">
		</div>
		<div class="span6">
			<label><h6><?php echo $l['additionally']; ?></h6></label>
				<input disabled type="text" class="span6" name="attributes[additionally]" placeholder="<?php echo $l['skype']; ?>">
		</div>
	</div>
	<div class="row">
		<div class="span3">
			<label><h6><?php echo $l['to_contact_you']; ?>*</h6></label>
				<input disabled type="text" class="span3" name="attributes[native_language]" placeholder="<?php echo $l['native_language']; ?>">
		</div>
		<div class="span3">
			<label><h6><?php echo $l['to_contact_you']; ?>* <i  rel="tooltip" title="<?php echo $l['in_the_case_of']; ?>" class="icon-question-sign"></i></h6></label>
				<input disabled type="text" class="span3" name="attributes[in_latin]" placeholder="<?php echo $l['in_latin']; ?>">
		</div>
		<div class="span6">
			<label><h6><?php echo $l['email']; ?>*</h6></label>
				<input disabled type="text" name="email" class="span6 required" placeholder="email" id='email'>
		</div>
	</div>
    <div class="alert alert-error" style="display:none;">
    </div>
	</br>
        <a class="btn btn-success btn-large btn-block sendForm slider" type="button" form="sendFormEstate" next="PROMO"><?php echo $l['everything_is_r']; ?></a>
	</div>
    <!-- КОНТАКТЫ end -->
    <!-- ВЫБРАТЬ АГЕНТСТВО -->
	<div class="forms REAL_ESTATE_AGE" style="display:none;">
	<ul class="thumbnails">
		<li class="span12">
			<div class="thumbnail" class="accordion-toggle" data-toggle="collapse" data-target="#EstateAgencySelect">
				<center><h5><?php echo $l['choose_an_agenc']; ?></h5></center>
			</div>
		</li>
	</ul>
	<ul id="EstateAgencySelect" class="collapse">
	<div id="sendAgencyData">

            <div class="thumbnail">
                <center><legend><h4><?php echo $l['Authorization']; ?></h4></legend></center>
                <center>
                    <input class="span2" type="text" placeholder="Email" name="username" id="agencyUser">
                    <input class="span2" type="text" placeholder="<?php echo $l['Password']; ?>" name="password" id="agencyPass">
                    <button onclick="loginAgencyCHeck(); return false" class="btn btn-primary" name="agencyLogin"><?php echo $l['Sign in']; ?></button>
                    <input type="checkbox" name="remember"> <?php echo $l['Remember']; ?>
                    <button class="btn btn-success "><?php echo $l['Forgot your password']; ?></button>
                </center>
            </div>

    <div class="alert alert-error agencyAlert" style="display:none;">
    </div>
	</div>
	</ul>
			<ul class="thumbnails">
				<li class="span12">
					<a class="thumbnail" href="http://localhost/infokain-web3/services/addnew.php" target="_blank">
					    <center><h5><?php echo $l['my_agency_is_no']; ?></h5></center>
					</a>
				</li>
			</ul>
	</div>
    <!-- ВЫБРАТЬ АГЕНТСТВО end -->
    <!-- PROMO -->
	<div class="forms PROMO" style="display:none;">
	<div class="container">
		<div class="row">
			<div class="span12">
				<legend><h4><?php echo $l['your_ad_has_the']; ?></h4></legend>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<blockquote>
					<p><h5>
					<?php echo $l['the_announcemen_world']; ?>
					</br>
					<?php echo $l['the_announcemen_38']; ?>, <em><?php echo $l['the_language_pa']; ?></em>
					</br>
					<?php echo $l['this_ad_is_opti']; ?>
					</h5></p>
				</blockquote>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<legend><h4><?php echo $l['want_more']; ?></h4></legend>
			</div>
		</div>
		<div class="row">
			<div class="span6">
				<blockquote>
					<p><h5>
					<?php echo $l['section_real']; ?>
					</br>
					<?php echo $l['show_my_ads']; ?>
					</br>
					<?php echo $l['click_on_the']; ?> <i class="icon-cog"></i> <?php echo $l['next_to_the_des']; ?>
					</br>
					<?php echo $l['select_one_o']; ?>
					</br>
					</h5></p>
				</blockquote>
			</div>
			<div class="span6">
				<li class="span5">
					<a class="thumbnail"  href="#">
						<center><h5><p class="muted"><?php echo $l['highlight_this']; ?> (<?php echo $l['opens_a_modal_w']; ?>)</p></h5></center>
					</a>
				</li>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<legend><h4><?php echo $l['become_our_part']; ?></h4></legend>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<blockquote>
					<p><h5>
					<?php echo $l['affiliate_progr']; ?> 
					</br>
					<center><a class="btn btn-link" href="../user_room"><?php echo $l['go_to_affiliate']; ?></a></center>
					</h5></p>
				</blockquote>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<legend><h4><?php echo $l['the_book_of_com']; ?></h4></legend>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<blockquote>
					<p><h5>
					<?php echo $l['point_to_shortc']; ?>  
					</br>
					</h5></p>
				</blockquote>
				<center><textarea rows="3"></textarea></center>
				
			</div>
		</div>
		<a class="btn btn-block btn-inverse" type="button" href="index.php"><?php echo $l['send_or_simply']; ?></a>
	</div>
</div>
<!-- PROMO end-->
<!-- GOOGLE MAPS -->
    <div class="modal hide" id="maps" tabindex="-1" role="dialog" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Определение месторасположения на карте</h3>
        </div>

        <div class="modal-body row" >
            <div class="span8" id="maps_modal" style="height:400px; border:1px solid #ccc;"></div>
            <div class="span3" id="maps_info" style="height:400px;">
                <dl>
                <dt>Координаты маркера:</dt>
                <dd id="mapCoordinates">...</dd>
                <dt><input id="mapAdres" type="hidden" value=""/></dt>
                <dd><h3>Перетащите маркер если не точно</h3></dd>
                </dl>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
            <button class="btn btn-primary" id="mapSave" >Сохранить</button>
        </div>
    </div>
<!-- GOOGLE MAPS -->
</form>
</div>

</body>
</html>
	<script src="../js/jquery-1.9.1.js"></script>
	<script src="../js/bootstrap-select.js"></script>
	<script src="../js/bootstrap.js"></script>
    <script src="../js/tooltip.js"></script>
    <script src="../js/sendForm.js"></script>
    <script src="../js/dependCatalog.js"></script>
    <script src="../uploderFiles/uploud.js"></script>
    <script src="../uploderFiles/FileAPI.min.js"></script>
    <script src="../uploderFiles/FileAPI.exif.js"></script>
    <script>

function loginAgencyCHeck(){
    console.log('ishleddcfvgbjhnji');
    $.ajax({
        url: "reg.php", 
            data: 'login=' + $('#agencyLogin').val() + '&pass=' + $('#agencyPass').val(), 
            method: "POST",
            success: function (data) {
                $('.agencyAlert').show();
                $('.agencyAlert').html(data);
            }
                                                });
                                                return false;
                                            }

                                            </script>
