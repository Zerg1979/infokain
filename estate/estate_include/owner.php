<label><?php echo $l['owner']; ?> <i  rel="tooltip" data-placement="right" title="<?php echo $l['what_are_you_th']; ?>" class="icon-question-sign"></i></label>
	<select disabled class="selectpicker span3" data-style="btn-inverse" data-container="body" name="attributes[owner]">
		<option></option>
		<option value="developer"><?php echo $l['developer']; ?></option>
		<option value="first"><?php echo $l['first']; ?></option>
		<option value="the second"><?php echo $l['the second']; ?></option>
		<option value="the third"><?php echo $l['the third']; ?></option>
		<option value="the fourth"><?php echo $l['the fourth']; ?></option>
		<option value="fifth or more"><?php echo $l['fifth or more']; ?></option>
		<option value="i do not know"><?php echo $l['i do not know']; ?></option>
	</select>
