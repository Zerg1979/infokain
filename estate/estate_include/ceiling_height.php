<label><?php echo $l['ceiling_height']; ?>*</label>
	  <input disabled class="span1" type="number" name="attributes[ceiling_height]" size="16" >
			<select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" name="attributes[ceiling_height_units]">
				<option></option>
				<option data-subtext="<?php echo $l['meter']; ?>"><?php echo $l['m']; ?></option>
				<option data-subtext="<?php echo $l['yard']; ?>"><?php echo $l['yd']; ?></option>
				<option data-subtext="<?php echo $l['foot']; ?>"><?php echo $l['ft']; ?></option>
				<option data-subtext="<?php echo $l['syaku_japan']; ?>"><?php echo $l['syaku']; ?></option>
				<option data-subtext="<?php echo $l['souk_thailand']; ?>"><?php echo $l['souk']; ?></option>
				<option data-subtext="<?php echo $l['china_chi']; ?>"><?php echo $l['chi']; ?></option>
			</select>
