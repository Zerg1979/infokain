<label><?php echo $l['type_of_constru']; ?></label>
	<select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" name="attributes[type_of_construct]">
		<option></option>
		<option value="modular"><?php echo $l['modular']; ?></option>
		<option value="monolithic"><?php echo $l['monolithic']; ?></option>
		<option value="frame"><?php echo $l['frame']; ?></option>
		<option value="brick"><?php echo $l['brick']; ?></option>
		<option value="panel"><?php echo $l['panel']; ?></option>
		<option value="metal construction"><?php echo $l['metal construction']; ?></option>
	</select>
