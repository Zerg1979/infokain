<label><?php echo $l['area']; ?></label>
	  <input disabled class="span2 inform" formname="area" name="area" size="16" type="number">
		<select disabled class="selectpicker span2" data-style="btn-inverse" data-container="body" id="attributes_area" name="attributes[area]">
			<option meter='100' data-subtext="<?php echo $l['weaving']; ?>"><?php echo $l['weaving']; ?></option>
			<option meter='1' data-subtext="<?php echo $l['ha']; ?>"><?php echo $l['ga']; ?></option>
			<option meter='0.003861' data-subtext="<?php echo $l['square_mile']; ?>"><?php echo $l['mile']; ?><sup>2</sup></option>
			<option meter='2.471' data-subtext="<?php echo $l['acre']; ?>"><?php echo $l['a']; ?></option>
		</select>
