<?php
	
	include '../user_room/ajax.php';
	
	/*
	 * getUserInfo($id) id yada email
	 * 
	 * addTempUser($email); returns id
	*/
	$data = $_POST;
	
	if($data['email']=='')
	{
		die('Give me your email or please, please fuck you!!!');
	}
	
	$email = $data['email'];
	
	if(!isset($_SESSION['id']))
	{
		$status = 0;
		$user_info = getUserInfo($email);
		
		if(!$user_info)
		{
			$user_id = addTempUser($email);
			$user_info = getUserInfo($email);
		}
		
		$user_id = $user_info['id'];
	}
	else
	{
		$status = 1;
		$user_id = $_SESSION['id'];
		$user_info = getUserInfo($user_id);
	}
	//echo '<pre>';
	//print_r($data);
	$post_param = array(
			  "status" => $status,
			  "razdel" => $data['razdel'],
			  "action"=>$data['action'],
			  "validity"=>"30",
			  "price"=>$data['price'],
			  "currency"=>$_COOKIE['currency'],
			  "owner"=>$user_id,
			  "owner_name"=>$data['attributes']['in_latin'],
			  "owner_phone"=>$user_info['phone'],
			  "owner_im"=>$data['attributes']['additionally'],
			  "lang"=>$_COOKIE['lang'],
			  "city"=>$_COOKIE['city'],
			  "colored"=>"0",
			  "country"=>$_COOKIE['country'],
			  "attributes"=>$data['attributes']);
				  
	$data_string = json_encode($post_param);                                                                 
 
	$ch = curl_init("http://api.infokain.com/users/$user_id/ads");                                                                      
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	
	$result = curl_exec($ch);
	
	$response['message'] = 'Your add is successfully added';
	$response['code'] = 1;
	
	if(!$status)
	{
		//send verification email here
		$result = json_decode($result, true);
		$ad_id = $result['id'];
		
		include_once '../includes/verification.php';
		require '../PHPMailer/PHPMailerAutoload.php';
		
		$ver = new Verification();
		$code = $ver->generate_code($ad_id);
			
		$link = "http://www.infokain.com/activate_ad.php?id=$ad_id&code=$code";
		
		$html = file_get_contents('ad_activation.html');
		$html = str_replace('{{link}}', $link, $html);
		
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->setFrom('no-reply@infokain.com');
		$mail->addAddress($email);
		$mail->Subject = 'Активация объявления';
		$mail->msgHTML($html, dirname(__FILE__));
		$mail->AltBody = 'Your activation link is '.$link;
		
		if(!$mail->send())
		{
			$response['message'] = $mail->ErrorInfo;
			$response['code'] = 0;
		}
		else
		{
			$response['message'] = 'Check your email for activation of ad';
			$response['code'] = 1;
		}
		
		json_encode($response);
	}
?>
