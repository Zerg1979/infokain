<?php
    include_once ("../lib/Catalog.php");
    
    $lang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'ru';

    // ОБЩИЕ
    // --------------------------------------------------------------------------
    // Получить страны
    if (isset($_GET["method"]) && $_GET["method"] == "countries") {
        $url = 'http://cat.infokain.com/countries';
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
    // Получить города
    } elseif (isset($_GET["method"]) && $_GET["method"] == "cities") {
        $country = $_GET["country"];
        $url = 'http://cat.infokain.com/cities/'.urlencode($country);
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
    // Получить языки
    } elseif (isset($_GET["method"]) && $_GET["method"] == "languages") {
        $url = 'http://cat.infokain.com/language';
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;

    // РАЗДЕЛ "УСЛУГИ"
    // --------------------------------------------------------------------------
    // Получить категории услуг
    } elseif(isset($_GET["method"]) && $_GET["method"] == "category"){
        $url = 'http://cat.infokain.com/category/servise';
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
    // Получить услуги
    } elseif (isset($_GET["method"]) && $_GET["method"] == "service") {
        $category = $_GET["category"];
        $url = 'http://cat.infokain.com/service/'.urlencode($category);
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
   
    // РАЗДЕЛ "МАГАЗИНЫ"
    // --------------------------------------------------------------------------
    // Получить категории товаров
    } elseif (isset($_GET["method"]) && $_GET["method"] == "goods_category") {
        $url = 'http://cat.infokain.com/category/goods';
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
    // Получить виды товаров
    } elseif (isset($_GET["method"]) && $_GET["method"] == "kinds") {
        $category = $_GET["category"];
        $url = 'http://cat.infokain.com/kind/'.urlencode($category);
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
    // Получить марки товаров
    } elseif (isset($_GET["method"]) && $_GET["method"] == "marks") {
        $kind = $_GET["kind"];
        $url = 'http://cat.infokain.com/mark/'.urlencode($kind);
        $data = Curl::makeHttpCall( $url , 80, 'HTTP_GET', '?lang='.$lang, null);
        echo($data);
        die;
    // --------------------------------------------------------------------------
    // По умолчанию
    } else {
        echo("");
    }
?>
